package gusl.auth;

public enum AuthType {
    MSAL,
    GOOGLE_SSO,
    USERNAME_PASSWORD,
    TIME_BASED_ONE_TIME_PASSWORD,
    API_KEY
}
