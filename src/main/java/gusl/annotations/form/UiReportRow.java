package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiReportRow {

    String css() default "";

    float height() default -1.0f;

    MediaType mediaType() default MediaType.Desktop;

    String valueOuterCss() default "";

    String valueInnerCss() default "";


}
