package gusl.annotations.form.breadcrumb;

import gusl.core.tostring.ToString;
import gusl.query.QueryParams;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BreadCrumbDO {

    private String label;

    private String route;

    private QueryParams queryParams;

    public static BreadCrumbDO of(String label, String route) {
        return BreadCrumbDO.builder().label(label).route(route).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
