package gusl.annotations.form.breadcrumb;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BreadCrumbsDO {

    @Singular
    List<BreadCrumbDO> crumbs;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
