/* Copyright lottomart */
package gusl.annotations.form;

public interface FieldType {

    String avatar = "avatar";
    String booster = "booster";
    String card_panel = "diamond_panel";
    String chart = "chart";
    String changes = "changes";
    String checkbox = "checkbox";
    String cohort_logo = "cohort_logo";

    String circle = "circle";

    String codepen = "codepen";
    String color = "color";
    String comment = "comment"; // comment badge and panel
    String condition = "condition";
    String deeplink = "deeplink";
    String bundle = "bundle";
    String count = "count";
    String date_range = "date_range";
    String date_time = "date_time";
    String date = "date";
    String decimal = "decimal";
    String diamond_panel = "diamond_panel";
    String draw_options = "draw_options";
    String duration = "duration";
    String emoji = "emoji";
    String future_draws = "future_draws";
    String file_upload = "file_upload";
    String header = "header";
    String html = "html";
    String html_block = "html_block";
    String id = "id";
    String image_uploader = "image_uploader";
    String iframe = "iframe";
    String image = "image";
    String link = "link";
    String logo = "logo";
    String lookup = "lookup";
    String lotto_line = "lotto_line";
    String money = "money";
    String new_money = "new_money";
    String map = "map";
    String multi_option = "multi_option";
    String multiple = "multiple";
    String nested_table = "nested_table";
    String number = "number";
    String offer = "offer";
    String option = "option";
    String option_panel = "option_panel";
    String option_list = "option_list";
    String open_in_player = "open_in_player";
    String payment = "payment";
    String password = "password";
    String permissions = "permissions";
    String player_status = "player_status";
    String prize_table = "prize_table";
    String properties = "properties";
    String quick_picks = "quick_picks";
    String radio = "radio";
    String rate = "rate";
    String report = "report";
    String rewards = "rewards";
    String spending_limit = "spending_limit";
    String sub_panel = "sub_panel";
    String hero_slides = "hero_slides";
    String hero_slide = "hero_slide";
    String rule = "rule";

    String rule_condition = "rule_condition";

    String rule_action = "rule_action";

    String symbol = "symbol";
    String shape = "shape";
    String text = "text";
    String theme = "theme";
    String theme_definition = "theme_definition";
    String video = "text";
    String textarea = "textarea";
    String time = "time";
    String todo = "todo";
    String toggle = "toggle";
    String weekdays = "weekdays";
    String rearranging_list = "rearranging_list";
    String code = "code";
    String adyen_ref = "adyen_ref";
    String graph = "graph";
    String slide = "slide";
    String slide_deck = "slide_deck";
    String metric = "metric";

    String tree_content = "tree_content";
    String tree_children = "tree_children";
    String tree_parent = "tree_parent";
    String tree = "tree";
    String chips = "chips";
    String multi_lookup = "multi_lookup";

    String percent = "percent";
    String badge = "badge";
    String position = "position";
    String cron = "cron";

    String card = "card";

    String text_comment = "text_comment"; // editable comment

    String favourite = "favourite";

}
