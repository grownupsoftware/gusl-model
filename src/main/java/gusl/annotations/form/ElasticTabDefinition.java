package gusl.annotations.form;

public @interface ElasticTabDefinition {
    QueryCondition[] conditions() default {};
    Class<?> esoClass() default Object.class;
    Class<?> convertEsoTo() default Object.class;
    //index name without 'lm-'prefix
    String indexName() default "";
    String aggsUrl() default "";

    String elasticQueryGroup() default "";
}
