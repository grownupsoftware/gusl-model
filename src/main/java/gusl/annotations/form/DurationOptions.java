package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DurationOptions {

    boolean showLetters() default true;

    boolean showYears() default true;

    boolean showMonths() default true;

    boolean showWeeks() default true;

    boolean showDays() default true;

    boolean showHours() default true;

    boolean showMinutes() default true;

    boolean showSeconds() default true;

    boolean showPreview() default true;

    String previewFormat() default "";

    boolean showButtons() default false;
}
