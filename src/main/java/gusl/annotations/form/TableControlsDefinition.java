package gusl.annotations.form;

public @interface TableControlsDefinition {

    MediaType pagination() default MediaType.Mobile;

    MediaType resize() default MediaType.Mobile;

    MediaType filters() default MediaType.Mobile;

    MediaType search() default MediaType.Mobile;

    MediaType refresh() default MediaType.Mobile;

    MediaType columnSettings() default MediaType.Mobile;

}
