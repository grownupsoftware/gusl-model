package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiFieldType {

    String label() default "";

    String placeholder() default "";

    String labelCss() default "";

    String entryCss() default "";

    boolean noLabel() default false;

    boolean displayInTable() default false;

    boolean filterInTable() default false;

    boolean addIgnore() default false;

    boolean editReadOnly() default false;

    String dateFormat() default "";

    boolean editHide() default false;

    String fieldType();

}
