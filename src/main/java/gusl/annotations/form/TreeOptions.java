package gusl.annotations.form;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface TreeOptions {
    TreeType[] types() default {};
    int depth() default 1;

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    public static @interface TreeType {
        String type();
        Class<?> clazz();
        String updateUrl() default "";
        String addUrl() default "";
        String reorderUrl() default "";
        String [] childrenTypes() default {};
        String deleteUrl() default "";
    }
}
