package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UiReportTable {

    String title() default "";

    String css() default "";

    MediaType mediaType() default MediaType.Laptop;

    boolean withHeaderRow() default true;

    boolean withColumnLabels() default false;

    String blastDeltaCommand() default "";

    String keyField() default "";

    float width() default -1f;

    float[] columnWidths() default {};

    String valueOuterCss() default "";

    String valueInnerCss() default "";


}
