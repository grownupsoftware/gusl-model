package gusl.annotations.form;

public @interface CohortMediaRestrictions {

    String cohortId() default "";

    MediaType mediaType() default MediaType.Desktop;

}
