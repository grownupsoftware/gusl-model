package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface TableRowDefinition {

    String ELASTIC_TYPE = "ELASTIC";
    String MAINTAIN_TYPE = "MAINTAIN";
    String TICKER = "TICKER";
    String CASSANDRA_TYPE = "CASSANDRA";
    String BLAST_COLLECTION = "BLAST_COLLECTION";

    String DATA_TABLE = "DATA_TABLE";

    String ALPHA_LIST = "ALPHA_LIST";

    String FLEX_LIST = "FLEX_LIST";

    String ACCORDION_LIST = "ACCORDION_LIST";

    String ENTITY_GROUP = "ENTITY_GROUP";

    String id() default "";

    String title();

    UiTabDefinition[] tabs();

    String[] actions() default {};

    Class filterClass() default Object.class;

    boolean canAdd() default false;

    boolean tabsWithCounter() default false;

    String countUrl() default "";

    Class<?>[] tickerCommandEvents() default {};

    RestActionConfig[] overallRestActions() default {};

    //FOR TIME BEING ELASTIC SUPPORTS JUST ONE TAB!!!
    String type() default TableRowDefinition.MAINTAIN_TYPE;

    String tickerCode() default "";

    String aggsUrl() default "";

    boolean expandable() default false;

    TickerDefinition[] ticker() default @TickerDefinition();

    String headerUrl() default "";

    boolean disableTableControls() default false;

    boolean disableColumnResize() default false;

    int refreshRate() default -1;

}
