package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiEntityGroupHeader {
    RestActionConfig[] actions() default {};

    String jsBackground() default "'black'";

    // '{id}' in url will be changed for id in url on ui side
    String url();

    //refresh rate in seconds
    int refreshRate() default 0;

    String jsFontColor() default "'white'";

    String jsTitle() default "";

    String jsImage() default "";

    String jsLinkUrl() default "";

    UiPanel[] panels() default {};

    boolean withRefreshButton() default false;


}
