package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface RestActionConfig {

    String url() default "";

    String templateUrl() default "";

    String method() default "POST";

    BaseActionConfig base() default @BaseActionConfig();

    RestActionConfigChild[] children() default {};

    String parentMenuLabel() default "";

    boolean multipartForm() default false;

    String permission() default "";

    String modalCode() default "";

    String routerLink() default "";

    int displayOrder() default 10;

    String icon() default "";

    boolean iconOnly() default false;

    MediaType mediaType() default MediaType.Laptop;

    String buttonCss() default "";

}
