package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface UiFilter {
    boolean required() default false;

    String[] options() default "";

    String fieldName() default "";

    String fromName() default "";

    String[] applyForModes() default {};

    String elasticSearchOperand() default QueryOperandNames.EQUALS;

    UiDateFormat dateFormat() default UiDateFormat.NONE;

    boolean hasTime() default true;
    boolean hasFuture() default true;
    boolean hasPast() default true;

    String distinctUrl() default "";


}
