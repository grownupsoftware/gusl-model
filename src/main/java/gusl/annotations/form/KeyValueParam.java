package gusl.annotations.form;

public @interface KeyValueParam {
    String key();
    String [] value();
}
