/* Copyright lottomart */
package gusl.annotations.form;

/**
 * @author grant
 */
public enum LookupType {
    standard_id("standard_id"),
    standard_code("standard_code"),
    none("none"),
    country("country"),
    bonus("bonus"),
    bonus_targeting("bonus_targeting"),
    locale("locale"),
    insurer("insurer"),
    image("image"),
    condition("condition"),
    offer("offer"),
    diamond_tier("diamond-tier"),
    payment_type("payment-type"),
    draws("draws"),
    lotto("lotto"),
    hero_slide("hero_slide"),
    hero_banner("hero_banner"),
    currency("currency"),
    promotion("promotion"),
    sub_account("sub_accountSimplePlayersOnlineCacheImpl"),
    deeplink("deeplink"),
    lottoland_lottery("lottoland_lottery"),
    game_instance("game_instance"),
    casino_category("casino_category"),
    free_spin("free_spin"),
    free_spin_player("free_spin_player"),
    node_details("node_details"),
    draw_instance("draw_instance"),
    zendesk_article("zendesk_article"),
    player_card("player_card"),
    dynamic_option("dynamic_option"),
    game_studio("game_studio"),
    game_tag("game_tag"),
    network_name("network_name"),
    bundle("bundle"),
    slide("slide"),
    slide_deck("slide_deck"),
    rule_book("rule_book"),
    simple_game_category("simple_game_category"),
    number_wrapper("number_wrapper"),
    wagering_profile("wagering_profile"),
    player_tag("player_tag");

    private final String name;

    LookupType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
