package gusl.annotations.form.page;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class FilterActionControlDO {

    private List<FilterActionDO> filterActions;
    private boolean includeFavQueries;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
