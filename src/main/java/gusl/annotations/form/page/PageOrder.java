package gusl.annotations.form.page;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class PageOrder {
    private String field;
    private OrderDirection direction;
    private String tieBreaker;
}
