package gusl.annotations.form.page;

import gusl.annotations.form.MediaType;
import gusl.query.QueryParams;
import lombok.*;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@ToString
public class FilterActionDO {

    private QueryParams queryParams;

    private String route;

    private String label;

    private String icon;

    private MediaType mediaType;

    private Integer displayOrder;

}
