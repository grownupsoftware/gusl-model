package gusl.annotations.form.page;

import com.google.common.base.CaseFormat;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.errors.SystemErrors;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static gusl.core.utils.LambdaGUSLErrorExceptionHelper.rethrowGUSLErrorFunction;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class PredicateCache<T> {

    private final Map<Predicate<T>, Predicate<T>> theMapOfPredicates = new ConcurrentHashMap<>(3);

    public List<Predicate<T>> getPredicates(final PageRequest request, Class<T> dataClass) {
        if (isNull(request)
                || isNull(request.getQueryConditions())
                || isNull(request.getQueryConditions().getConditions())
                || request.getQueryConditions().getConditions().isEmpty()
        ) {
            return new ArrayList<>(0);
        }
        return getPredicates(request.getQueryConditions(), dataClass);
    }

    public List<Predicate<T>> getPredicates(final QueryConditions queryConditions, Class<T> dataClass) {
        if (isNull(queryConditions) || isNull(queryConditions.getConditions()) || queryConditions.getConditions().isEmpty()) {
            return new ArrayList<>(0);
        }
        return safeStream(queryConditions.getConditions())
                .filter(condition -> nonNull(condition.getFieldName()))
                .filter(condition -> nonNull(condition.getValue()))
                .filter(condition -> condition.getOperand() == QueryOperand.EQUALS
                        || condition.getOperand() == QueryOperand.NOT_EQUALS
                        || condition.getOperand() == QueryOperand.QUERY_STRING
                )
                .map(rethrowGUSLErrorFunction(condition -> createPredicate(condition, dataClass))).collect(Collectors.toList());
    }

    private static String normaliseName(String name) {
        if (name.contains("-")) {
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, name);
        } else if (name.contains("_")) {
            return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
        }
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, name);
    }

    private static String getMethodName(String name) {
        return "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private Predicate<T> createPredicate(QueryCondition queryCondition, Class<T> dataClass) throws GUSLErrorException {
        if (theMapOfPredicates.containsKey(queryCondition)) {
            return theMapOfPredicates.get(queryCondition);
        }

        String fieldName = normaliseName(queryCondition.getFieldName());
        Method getterMethod;
        try {
            getterMethod = dataClass.getMethod(getMethodName(fieldName));
        } catch (NoSuchMethodException ex) {
            throw new GUSLErrorException(SystemErrors.FAILED_TO_GET_GETTER_METHOD.getError(dataClass.getCanonicalName(), fieldName));
        }

        Predicate<T> predicate = createPredicate(queryCondition, getterMethod);
        theMapOfPredicates.put(predicate, predicate);

        return predicate;
    }

    public Predicate<T> createPredicate(QueryCondition queryCondition, Method getterMethod) {

        return p -> {
            try {
                switch (queryCondition.getOperand()) {
                    case EQUALS:
                        if (getterMethod.getReturnType().isEnum()) {
                            Enum enumValue = (Enum) getterMethod.invoke(p);
                            logger.debug("Enum Equals {} {} = {}", ((String) queryCondition.getValue()),
                                    enumValue.name(),
                                    enumValue.name().trim().equalsIgnoreCase((String) queryCondition.getValue()));
                            return enumValue.name().trim().equalsIgnoreCase((String) queryCondition.getValue());
                        } else {
                            switch (getterMethod.getReturnType().getSimpleName()) {
                                case "String":
                                    String strValue = (String) getterMethod.invoke(p);
                                    return !isNull(strValue) && ((String) queryCondition.getValue()).trim().equalsIgnoreCase(strValue);
                                case "Long":
                                    Long longValue = (Long) getterMethod.invoke(p);
                                    return ((Long) queryCondition.getValue()).longValue() == longValue;
                                case "Boolean":
                                    logger.info("---- {} {} {} {} {}",
                                            getterMethod.invoke(p),
                                            queryCondition.getValue(),
                                            Boolean.TRUE.equals(getterMethod.invoke(p)),
                                            Boolean.TRUE.equals(Boolean.valueOf((String) queryCondition.getValue())),
                                            (Boolean.TRUE.equals(getterMethod.invoke(p)) && Boolean.TRUE.equals(queryCondition.getValue())));
                                    return Boolean.TRUE.equals(getterMethod.invoke(p)) && Boolean.TRUE.equals(Boolean.valueOf((String) queryCondition.getValue()));
                                default:
                                    throw new GUSLErrorException(SystemErrors.NOT_SUPPORTED.getError("Return type of [" + getterMethod.getReturnType().getSimpleName() + "] not supported"));
                            }
                        }
                    case NOT_EQUALS:
                        if (getterMethod.getReturnType().isEnum()) {
                            Enum enumValue = (Enum) getterMethod.invoke(p);
                            if (isNull(enumValue)) {
                                return true;
                            }
                            return !enumValue.name().equals((String) queryCondition.getValue());
                        } else {
                            switch (getterMethod.getReturnType().getSimpleName()) {
                                case "String":
                                    String strValue = (String) getterMethod.invoke(p);
                                    return isNull(strValue) || !((String) queryCondition.getValue()).trim().equalsIgnoreCase(strValue);
                                case "Long":
                                    Long longValue = (Long) getterMethod.invoke(p);
                                    return ((Long) queryCondition.getValue()).longValue() != longValue;
                                default:
                                    throw new GUSLErrorException(SystemErrors.NOT_SUPPORTED.getError("Return type of [" + getterMethod.getReturnType().getSimpleName() + "] not supported"));
                            }
                        }
                    case QUERY_STRING:
                        if (getterMethod.getReturnType().isEnum()) {
                            Enum enumValue = (Enum) getterMethod.invoke(p);
                            logger.debug("Enum contains {} {} = {}", ((String) queryCondition.getValue()),
                                    enumValue.name(),
                                    enumValue.name().toLowerCase().contains(((String) queryCondition.getValue()).trim().toLowerCase()));
                            return enumValue.name().toLowerCase().contains(((String) queryCondition.getValue()).trim().toLowerCase());
                        } else {
                            switch (getterMethod.getReturnType().getSimpleName()) {
                                case "String":
                                    String strValue = (String) getterMethod.invoke(p);
                                    logger.debug("String contains {} {} = {}", ((String) queryCondition.getValue()),
                                            strValue,
                                            isNull(strValue) ? "null" : strValue.toLowerCase().contains(((String) queryCondition.getValue()).trim().toLowerCase()));
                                    return isNull(strValue) ? false : strValue.toLowerCase().contains(((String) queryCondition.getValue()).trim().toLowerCase());
                                case "Long":
                                    Long longValue = (Long) getterMethod.invoke(p);
                                    return isNull(longValue) ? false : ((String) queryCondition.getValue()).trim().toLowerCase().contains(longValue.toString().trim().toLowerCase());
                                default:
                                    throw new GUSLErrorException(SystemErrors.NOT_SUPPORTED.getError("Return type of [" + getterMethod.getReturnType().getSimpleName() + "] not supported"));
                            }
                        }
                    case DATE_RANGE:
                    default:
                        throw new GUSLErrorException(SystemErrors.NOT_SUPPORTED.getError("Query operand [" + queryCondition.getOperand().getName() + "] not supported"));
                }
            } catch (GUSLErrorException | IllegalAccessException | InvocationTargetException e) {
                logger.warn("Failed to apply predicated", e);
                return false;
            }
        };
    }

}
