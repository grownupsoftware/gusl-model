package gusl.annotations.form.page;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import gusl.query.QueryParams;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DownloadRequest {

    private static final int MAX_LIMIT = 10000;

    private QueryParams queryParams;
    private List<String> fields;

    @JsonIgnore
    public QueryParams getResetQueryParams() {
        queryParams.setSkip(0);
        queryParams.setLimit(MAX_LIMIT);
        return queryParams;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
