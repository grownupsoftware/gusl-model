package gusl.annotations.form.page;

import com.fasterxml.jackson.annotation.JsonFormat;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class DateRange {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date from;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date to;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
