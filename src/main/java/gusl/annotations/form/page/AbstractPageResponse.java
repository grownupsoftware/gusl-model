package gusl.annotations.form.page;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@ToString
@SuperBuilder
public class AbstractPageResponse {
    private int offset;
    private int perPage;
    private String pageStates[];
    private long total;
    private Map<String, Object> queryParams;
    // private PageOrder order;
    private String orderParamTieBreaker;
    private OrderDirection orderDirection;
    private String orderParam;

    private QueryConditions queryConditions;
    private String convertTo;
    private String indexClass;
    private String after;
    private String before;
    private Boolean last;

}
