package gusl.annotations.form.page;

import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class QueryCondition {
    private String fieldName;

    private QueryOperand operand;

    private Object value;

    private DateRange dateRange;

    private Map<String, Object> complexValue;

    private String fromName;

}
