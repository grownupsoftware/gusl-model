package gusl.annotations.form.page;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PageResponseBuilder<T> implements PageResponse<T> {
    private QueryConditions queryConditions;

    private int offset;

    private int perPage;

    private String orderParam;

    private OrderDirection orderDirection;

    private String convertTo;

    private String indexClass;

    private String orderParamTieBreaker;

    private String after;

    private String before;

    private Boolean last;

    private String pageStates[];

    private long total;

    @ToStringCount
    private List<T> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
