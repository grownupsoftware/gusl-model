package gusl.annotations.form.page;

import gusl.annotations.form.MediaType;
import gusl.query.QueryOrder;
import lombok.*;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@ToString
public class OrderByActionDO {

    private String field;

    private QueryOrder order;

    private String ascIcon;

    private String descIcon;

    private MediaType mediaType;

}
