package gusl.annotations.form.page;

import java.util.List;

public interface PageResponse<T> {

    String getOrderParam();

    OrderDirection getOrderDirection();

    String getConvertTo();

    String getIndexClass();

    String getOrderParamTieBreaker();

    String getAfter();

    String getBefore();

    Boolean getLast();

    QueryConditions getQueryConditions();

    int getOffset();

    int getPerPage();

    long getTotal();

    List<T> getContent();

    String[] getPageStates();

}
