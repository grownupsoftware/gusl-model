package gusl.annotations.form.page;

import gusl.annotations.form.QueryOperandNames;

/**
 *
 * @author grant
 */
public enum QueryOperand {
    EQUALS(QueryOperandNames.EQUALS),
    QUERY_STRING(QueryOperandNames.QUERY_STRING),
    DATE_RANGE(QueryOperandNames.DATE_RANGE),
    NOT_EQUALS(QueryOperandNames.NOT_EQUALS);

    private final String name;

    QueryOperand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
