package gusl.annotations.form.page;

import gusl.core.utils.ClassUtils;
import gusl.core.utils.FieldComparator;
import gusl.core.utils.Utils;
import gusl.query.OrderBy;
import gusl.query.QueryOrder;
import gusl.query.QueryParams;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author dhudson
 * @since 22/08/2022
 */
public class PagedResponseMetadata<T> {

    private final Class<T> DOClass;
    private final Map<String, Field> mapFields;
    private Map<String, String> theBODOMap = new HashMap<>();

    public PagedResponseMetadata(Class<T> clazz) {
        DOClass = clazz;
        Set<Field> javaFields = ClassUtils.getFieldsFor(DOClass, true);
        mapFields = new HashMap<>(javaFields.size());
        javaFields.forEach(field -> mapFields.put(field.getName(), field));
    }

    public void putBoToDoFieldMap(String boField, String doField) {
        theBODOMap.put(boField, doField);
    }

    public PagedDAOResponse<T> generatePagedResponse(List<T> data, QueryParams queryParams) {
        if (data == null) {
            return PagedDAOResponse.<T>builder().content(null).total(0).queryParams(queryParams).build();
        }
        int total = data.size();
        return PagedDAOResponse.<T>builder().content(process(data, queryParams)).queryParams(queryParams).total(total).build();
    }

    public List<T> process(List<T> data, QueryParams queryParams) {
        return processSkipLimit(processOrder(data, queryParams), queryParams);
    }

    private List<T> processOrder(List<T> current, QueryParams queryParams) {
        if (current == null) {
            return null;

        }
        if (!Utils.hasElements(queryParams.getOrderBys())) {
            return current;
        }

        // Single sort as that's all the UI does
        OrderBy orderBy = queryParams.getOrderBys().iterator().next();

        String fieldName = orderBy.getField();
        if (theBODOMap.containsKey(fieldName)) {
            fieldName = theBODOMap.get(fieldName);
        }

        Collections.sort(current, new FieldComparator<>(mapFields.get(fieldName)));
        if (orderBy.getOrder() == QueryOrder.DESC) {
            Collections.reverse(current);
        }
        return current;
    }

    private List<T> processSkipLimit(List<T> data, QueryParams queryParams) {
        if (queryParams.getSkip() == 0 && queryParams.getLimit() == -1) {
            return data;
        }

        // Expecting skip = page, limit number of rows.
        int size = data.size();
        int start = queryParams.getSkip();
        int end = start + queryParams.getLimit();

        if (end > size) {
            end = size;
        }

        List<T> newList = new ArrayList<>(end);
        for (int i = start; i < end; i++) {
            newList.add(data.get(i));
        }

        return newList;
    }
}
