package gusl.annotations.form.page;

import java.util.List;

public interface Page<T>{

    int getOffset();

    int getPerPage();

    long getTotal();

    List<T> getContent();

    String getOrder();


}
