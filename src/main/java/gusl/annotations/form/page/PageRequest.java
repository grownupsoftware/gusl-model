package gusl.annotations.form.page;

import gusl.core.annotations.DocField;
import lombok.*;

import javax.validation.constraints.DecimalMin;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class PageRequest {

    private QueryConditions queryConditions;

    @DocField(description = "The page offset e.g. page 5")
    @DecimalMin(value = "0", message = "{validation.query.offset.min}")
    private int offset;

    @DecimalMin(value = "1", message = "{validation.query.per-page.min}")
    @DocField(description = "Number of items to display")
    private int perPage;

    @DocField(description = "The name of the field that the data is ordered by")
    private String orderParam;

    @DocField(description = "The order direction e.g. ASC, DESC")
    private OrderDirection orderDirection;

    @DocField(description = "Class to convert to")
    private String convertTo;

    @DocField(description = "used by elastic queries")
    private String indexClass;

    private String orderParamTieBreaker;

    private String after;

    private String before;

    private Boolean last;

    private String [] pageStates;

}
