package gusl.annotations.form.page;

import gusl.annotations.form.breadcrumb.BreadCrumbsDO;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import gusl.query.QueryParams;
import lombok.*;

import java.util.List;

/**
 * @author dhudson
 * @since 15/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PagedDAOResponse<T> {

    @ToStringCount
    private List<T> content;
    private QueryParams queryParams;
    private int total;

    private BreadCrumbsDO breadCrumb;

    List<OrderByActionDO> orderByActions;

    FilterActionControlDO filterAction;

    // Used by accordion list
    private String groupBy;

    private String orderBy;

    private String noRowsMessage;

    private String noRowsAutoRoute;

    private List<Integer> rowsPerPage;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
