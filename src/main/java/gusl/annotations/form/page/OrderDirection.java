package gusl.annotations.form.page;

public enum OrderDirection {
    ASC, DESC
}
