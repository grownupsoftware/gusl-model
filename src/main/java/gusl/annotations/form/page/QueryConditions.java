package gusl.annotations.form.page;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class QueryConditions {
    private String index;

    @Singular
    private List<QueryCondition> conditions;

}
