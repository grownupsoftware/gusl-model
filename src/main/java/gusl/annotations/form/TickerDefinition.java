package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface TickerDefinition {
    boolean hideCohort() default false;

    boolean hideLock() default false;

    boolean hideFilter() default false;

    boolean showBorder() default false;

    boolean showAdd() default false;

    boolean focusLoop() default false;

    boolean autoNext() default false;

    boolean hideRefresh() default false;

    boolean hideHeader() default false;

    boolean hidePagination() default false;

    boolean showPopout() default false;

    int popoutWidth() default 375;

    int popoutHeight() default 871;
}
