package gusl.annotations.form;

public @interface BlastCollectionTableDefinition {

    String collectionName() default "";
    String title() default "";
    Class<?> rowClass() default Object.class;

    boolean expandable() default false;
    Class<?> expandedClass() default Object.class;
    String getUrl() default "";

}
