package gusl.annotations.form;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CohortMediaType {
    private String cohortId;

    private MediaType mediaType;

    public static CohortMediaType of(String cohortId, MediaType mediaType) {
        return CohortMediaType.builder()
                .cohortId(cohortId)
                .mediaType(mediaType)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
