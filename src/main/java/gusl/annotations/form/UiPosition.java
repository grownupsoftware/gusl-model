package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiPosition {
    int fxFlex() default -1;

    String header() default "";

    String updateUrl() default "";

    boolean nested() default false;

    boolean fullWidth() default false;

    String jsEditCondition() default "";

    RestActionConfig[] actions() default {};

    boolean noLabel() default false;

    boolean ignoreIfNotPresent() default false;

    boolean multipartForm() default false;

    String width() default "";

    String height() default "";

    String styleCss() default "";

    String deleteUrl() default "";

}
