package gusl.annotations.form;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LookupConfig {

    private String name;
    private String extractLabel;
    private List<String> tableFields;
    private List<String> fieldTypes;
    private String sortBy;
    private boolean pageable;
    private String externalUrl;
    private String uniqueId;
    private String retValue;
    private boolean inline;
    private Class<?> lookupClass;
    @Override
    public String toString() {
        return ToString.toString(this);
    }
}

