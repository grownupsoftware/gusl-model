package gusl.annotations.form;

public enum FilterType {
    EQ, GT, LT, GET, LET
}
