package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiField {

    String DEFAULT_ALIGN = "default";
    String LEFT_ALIGN = "left";
    String CENTER_ALIGN = "center";
    String RIGHT_ALIGN = "right";

    String GRID_LAYOUT = "grid";
    String LIST_LAYOUT = "list";

    String XML_CODE_MODE = "xml";
    String CSS_CODE_MODE = "css";

    String SUM_AGGREGATION = "SUM";
    String AVG_AGGREGATION = "AVG";

    boolean ignoreIfNotPresent() default false;

    UiRef ref() default @UiRef();

    String align() default LEFT_ALIGN;

    boolean sortable() default true;

    String sortableField() default "";

    String type();

    String label() default "";

    String placeholder() default "";

    String labelCss() default "";

    String colCss() default "";

    String entryCss() default "";

    String tableCss() default "";

    String category() default "";

    boolean noLabel() default false;

    boolean displayInTable() default true;

    boolean displayWhenSelected() default false;

    boolean filterInTable() default false;

    boolean advancedFilter() default false;

    boolean addIgnore() default false;

    String editReadOnly() default "";

    boolean editHide() default false;

    boolean deleteHide() default false;

    boolean alignRight() default false;

    // for money
    String moneyFormat() default "fullValue"; // jackpotValue ticketPrice fullValue

    boolean fixedCurrency() default false;

    boolean editNoCurrency() default false;

    // for number
    String suffix() default "";

    boolean localeFormatted() default false;

    // for date (momentjs formatting)
    UiDateFormat dateFormat() default UiDateFormat.NONE;

    // for look up
    String idFieldName() default "id";

    LookupType lookup() default LookupType.none;

    String lookupCode() default "";

    String lookupSelectUrl() default "";

    String lookupInsertUrl() default "";

    String lookupSearchUrl() default "";

    String lookupCollection() default "";

    boolean lookupOverwritesForm() default false;

    // for multioption

    Class<? extends Enum> optionClass() default Enum.class;

    String propertyName() default "";

    boolean formFullWidth() default false;

    // for multiple
    String child() default "";

    String canAdd() default "false";

    // date
    String timezone() default "";

    ElasticTable standalone() default @ElasticTable();

    //applicable only to tickers command form
    String jsValue() default "";

    //applicable only to tickers command form
    boolean jsRecalculate() default false;

    boolean viewHide() default false;

    String expandedColor() default "";

    String lookupModalLayout() default LIST_LAYOUT;

    boolean canReorder() default false;

    boolean canDelete() default false;

    String reorderWeightField() default "displayOrder";

    String codeMode() default "";

    String theme() default "material";

    boolean copyButton() default false;

    String externalLinkJs() default "";

    boolean onlyFilter() default false;

    //works just for tickers
    String[] aggregators() default {};

    MetricOptions metricOptions() default @MetricOptions();

    TreeOptions treeOptions() default @TreeOptions();

    boolean addReadOnly() default false;

    String newTableElementUrl() default "";

    //data means this class
    String jsExpandablePanel() default "false";

    boolean strippedRows() default false;

    boolean hideIfEmpty() default false;

    String properties() default "";

    boolean editIfTicked() default false;

    boolean clipboard() default false;

    RestActionConfig[] actions() default {};

    DurationOptions durationOptions() default @DurationOptions();

    String panelOptionsUrl() default "";

    String lookupWatcherFieldName() default "";

    String lookupWatcherFieldObjectId() default "";

    String onBefore() default "";

    String onAfter() default "";

    String popupUrl() default "";

    String permission() default "";

    MediaType mediaType() default MediaType.Laptop;

    int displayOrder() default -1;

    boolean searchable() default false;

    boolean searchableSelected() default false;

    boolean noAutoSearch() default false;

    boolean searchbarOpen() default false;

    int fxFlex() default -1;

    String hint() default "";

    String updateFieldUrl() default "";

    String deleteFieldUrl() default "";

    String reorderUrl() default "";

    String[] allowedCohorts() default {};

    CohortMediaRestrictions[] mediaRestrictions() default {};

    String blastDeltaCommand() default "";

    String keyField() default "";

    String jsEditCondition() default "";

}
