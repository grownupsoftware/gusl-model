package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface BaseActionConfig {
    String buttonLabel() default "Action";

    String popUpTitle() default "Confirmation required";

    String popUpDescription() default "Are you sure you want to proceed?";

    String successMessage() default "Success";

    String jsEditCondition() default "";

    String minModalWidth() default "";

    String minModalHeight() default "";

    Class<?> form() default Object.class;

    String jsWarningCondition() default "";

    String warningMessage() default "Be very careful with that action. It shouldn't be used...";

    String confirmationMessage() default "";

    String confirmationFields() default "";

    boolean downloadAction() default false;

    boolean dynamicResponse() default false;

    String confirmationUrl() default "";

    boolean displaySummary() default false;

    Class<?> confirmationForm() default Object.class;

    String[] executionPermissions() default {};

    String templateUrl() default "";

    String modalCode() default "";

    String icon() default "";

    boolean iconOnly() default false;

    boolean inLine() default false;

    String saveButton() default "Save";

    String cancelButton() default "Cancel";

    String saveIcon() default "fas fa-check";

    String cancelIcon() default "fas fa-ban";

    boolean showCancel() default true;

    ActionType actionType() default ActionType.NOT_DEFINED;

    String selectUrl() default "";

    String selectMethod() default "GET";

    int displayOrder() default 10;

    String cancelColorTheme() default "defaultColor";

    String saveColorTheme() default "save";

    String tooltip() default "";

    String route() default "";

    String command() default "";

    int fxFlex() default -1;

    String altSaveButton() default "";

    String altSaveColorTheme() default "new";

    String altSaveIcon() default "fas fa-check";


    String properties() default "";
}
