package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UiMenuItem {
    String label();

    String code();

    String selectOne() default "";

    String formTitle() default "";

    String help() default "";

    String idTitleTagJs() default "";

    String permission() default "";

    String externalUrl() default "";

    boolean footer() default false;

    boolean overlay() default false;

}
