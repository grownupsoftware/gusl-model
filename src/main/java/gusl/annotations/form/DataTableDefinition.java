package gusl.annotations.form;

public @interface DataTableDefinition {
    String selectUrl() default "";

    Class<?> dtoClass() default Object.class;

    String blastDeltaCommand() default "";
}
