package gusl.annotations.form;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ComplexTableRowDetails {
    FieldValue [] conditions() default {};
    String getDetailsUrl();
    Class configClass() default Object.class;
    boolean expandable() default false;
    boolean grouped() default false;
    boolean table() default false;
    String code();
    boolean detailsButton() default false;
}
