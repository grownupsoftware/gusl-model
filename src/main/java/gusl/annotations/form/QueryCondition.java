package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE_PARAMETER})
public @interface QueryCondition {

    String ENTITY_ID_PLACEHOLDER = "{entityId}";
    String EQUALS_OPERAND = "EQUALS";
    String QUERY_STRING_OPERAND = "QUERY_STRING";
    String NOT_EQUALS = "NOT_EQUALS";

    String fieldName();
    String operand();
    String value() default "";
    String jsValue() default "";
}
