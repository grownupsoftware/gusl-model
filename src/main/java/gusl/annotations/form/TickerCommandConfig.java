package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TickerCommandConfig {
    String command();

    BaseActionConfig base();

    boolean requireConfirmation() default true;

    String fontAwesomeIcon() default "";

    boolean quickAccess() default false;

    boolean actionsBar() default true;

}
