package gusl.annotations.form;

public @interface CassandraTabDefinition {
    QueryCondition[] conditions() default {};

    String selectUrl() default "";

    Class<?> dtoClass() default Object.class;

    Class<?> convertTo() default Object.class;

    String blastDeltaCommand() default "";

    boolean highlightDelta() default true;

    boolean disableTableControls() default false;

    boolean disableColumnResize() default false;

    /**
     * refresh rate in seconds
     * @return
     */
    int refreshRate () default -1;


}
