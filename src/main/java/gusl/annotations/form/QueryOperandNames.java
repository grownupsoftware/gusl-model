package gusl.annotations.form;

public class QueryOperandNames {
    public static final String EQUALS = "EQUALS";
    public static final String QUERY_STRING = "QUERY_STRING";
    public static final String DATE_RANGE = "DATE_RANGE";
    public static final String NOT_EQUALS = "NOT_EQUALS";
    public static final String IN = "IN";
}
