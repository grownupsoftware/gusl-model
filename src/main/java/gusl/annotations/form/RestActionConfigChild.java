package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface RestActionConfigChild {
    String url() default "";

    String templateUrl() default "";

    String method() default "POST";

    BaseActionConfig base() default @BaseActionConfig();

    String parentMenuLabel() default "";

    String permission() default "";

    boolean multipartForm() default false;

    String modalCode() default "";

    String routerLink() default "";

    int displayOrder() default 10;

    String icon() default "";

    boolean iconOnly() default false;

    MediaType mediaType() default MediaType.Laptop;

    String buttonCss() default "";

}
