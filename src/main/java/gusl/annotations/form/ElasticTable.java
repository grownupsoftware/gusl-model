package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ElasticTable {

    Class<?> convertTo() default Object.class;

    Class<?> indexClass() default Object.class;

    QueryCondition[] conditions() default {};

    String title() default "";

    String index() default "";

    boolean withTimestamp() default false;
}
