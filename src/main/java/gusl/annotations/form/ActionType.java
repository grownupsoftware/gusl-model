package gusl.annotations.form;

public enum ActionType {
    VIEW_ONLY,
    ENTRY,
    VIEW_EDIT,
    EDIT,
    DELETE,
    VIEW_WITH_ACTION,
    DOWNLOAD_NO_PROMPT,
    DOWNLOAD_WITH_PROMPT,
    ACTION_ONLY,
    ROUTE,
    ROUTE_IN_CARD_PANEL,

    NOT_DEFINED,
    PANEL_FORM_BUTTON,

    ROUTE_IN_OVERLAY,


}
