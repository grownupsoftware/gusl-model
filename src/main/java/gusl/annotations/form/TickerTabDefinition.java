package gusl.annotations.form;

public @interface TickerTabDefinition {
    FieldValue [] conditions() default {};
    boolean expandable() default false;
    boolean actionable() default true;
}
