package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiSubMenu {
    SubMenuType type();

    String label();

    String code();

    String editUrl() default "";

    String getUrl() default "";

    Class<?> esoClass() default Object.class;

    Class<?> convertEsoTo() default Object.class;

    //index name without 'lm-'prefix
    String indexName() default "";

    QueryCondition[] conditions() default {};

    //structure = '[field-name],[direction]' where direction === ASC or DESC
    String defaultSortString() default "";

    String sortTieBreaker() default "id";

    boolean expandable() default false;

    String aggsUrl() default "";

    String elasticQueryGroup() default "";

    RestActionConfig[] actions() default {};

    String elasticCustomGetUrl() default "";

    String permission() default "";

    String collectionName() default "";

    RestActionConfig[] rowActions() default {};

    RestActionConfig[] groupActions() default {};

    int refreshRate() default -1;

    boolean noBanner() default false;

    MediaType mediaType() default MediaType.Laptop;

    UiOrderBy[] orderByActions() default {};

    boolean noLabel() default false;

}
