/* Copyright lottomart */
package gusl.annotations.form.pagetypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author grant
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UiEntityMaintain {

    String title() default "";
    boolean canAdd() default false;
    boolean canEdit() default false;
    boolean canView() default false;
    boolean canDelete() default false;


}
