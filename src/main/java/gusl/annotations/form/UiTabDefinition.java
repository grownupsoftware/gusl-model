package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UiTabDefinition {
    MaintainTabDefinition maintain() default @MaintainTabDefinition();

    ElasticTabDefinition elastic() default @ElasticTabDefinition();

    TickerTabDefinition ticker() default @TickerTabDefinition();

    TableControlsDefinition tableControls() default @TableControlsDefinition();

    CassandraTabDefinition cassandra() default @CassandraTabDefinition();

    DataTableDefinition dataTable() default @DataTableDefinition();

    BlastCollectionFormDefinition blastForm() default @BlastCollectionFormDefinition();

    BlastCollectionTableDefinition blastTable() default @BlastCollectionTableDefinition();

    String label();

    String code();

    String defaultSortString() default "";

    String sortTieBreaker() default "id";

    RestActionConfig[] actions() default {};

    RestActionConfig[] groupActions() default {};

    String permission() default "";

    int refreshRate() default -1;

    UiOrderBy[] orderByActions() default {};

    boolean noLabel() default false;

}

