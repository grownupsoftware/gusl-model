package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiCreateEntity {

    String title();

    String submitUrl();

    String templateUrl() default "";

    int cols() default 2;

    boolean multipartForm() default false;

    String permission() default "";

}
