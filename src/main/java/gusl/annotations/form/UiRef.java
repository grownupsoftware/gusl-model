package gusl.annotations.form;

public @interface UiRef {
    String entity() default "";
    String id() default "";
    String tab() default "";
    String nestedId() default "";
}
