package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiPanel {

    String panelCss() default "";

    boolean noTitle() default false;

    String title() default "";

    String titleCss() default "";

    String selectUrl() default "";

    // collection.currency/{id}
    String blastCollection() default "";

    String blastTopic() default "";

    RestActionConfig[] actions() default {};

    int fxFlex() default 0;

    String jsEditCondition() default "";

    Class<?>[] dtoClass() default {};

    int refreshRate() default -1;

}
