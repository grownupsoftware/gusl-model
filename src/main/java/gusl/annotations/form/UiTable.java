package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiTable {
    String getUrl() default "";

    Class<?> rowClass() default Object.class;

    QueryCondition[] conditions() default {};

    String defaultSortString() default "";

    String sortTieBreaker() default "id";

    boolean expandable() default false;

    RestActionConfig[] actions() default {};

}
