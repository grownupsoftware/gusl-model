package gusl.annotations.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface UiReportCol {

    ReportColumnType type();

    String label() default "";

    String labelCss() default "";

    String valueOuterCss() default "";

    String valueInnerCss() default "";

    UiDateFormat dateFormat() default UiDateFormat.NONE;

    MediaType mediaType() default MediaType.Laptop;

    String properties() default "";

    RestActionConfig action() default @RestActionConfig;

    boolean input() default false;

    String inputIdentifier() default "";

    boolean withHeader() default false; // used by nested table

    boolean hideNoValue() default false; // used by nested table

    String condition() default "";

}
