package gusl.annotations.form;

public @interface BlastCollectionFormDefinition {
    String collectionName() default "";
    String title() default "";
    Class<?> formClass() default Object.class;
}
