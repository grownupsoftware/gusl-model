package gusl.annotations.form;

import gusl.query.QueryOrder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface UiOrderBy {

    String field();

    QueryOrder order() default QueryOrder.ASC;

    String ascIcon() default "fa-solid fa-arrow-up-a-z";

    String descIcon() default "fa-solid fa-arrow-up-z-a";

    MediaType mediaType() default MediaType.Laptop;
}
