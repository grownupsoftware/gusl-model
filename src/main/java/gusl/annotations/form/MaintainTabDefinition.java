package gusl.annotations.form;

public @interface MaintainTabDefinition {
    String path() default "";
    KeyValueParam[] params() default {};
    boolean paged() default true;
}
