/* Copyright lottomart */
package gusl.annotations.form;

/**
 *
 * @author grantwallace
 * 
 * Note: The data formats defined below must follow moment.js formatting conventions.
 */
public enum UiDateFormat {
    NONE(null),
    OTC("MM/DD/YYYY"),
    SHORT_DATE("DD-MM-YY"),
    LONG_DATE("DD-MM-YYYY"),
    DAY_MONTH("DD-MM-YYYY"),
    DAY_MTH("DD/MM"),
    DAY_FULL_MONTH("DD-MMM"),
    DATE_WITH_YEAR_AND_TIME("DD-MM-YY HH:mm"),
    DATE_WITH_FULL_YEAR_AND_TIME("DD-MM-YYYY HH:mm"),
    DATE_AND_TIME("DD-MMM-YY HH:mm"),

    DAY_MONTH_YEAR("DD-MMM-YYYY"),

    DATE_AT_TIME("DD-MMM-YYYY @ HH:mm"),

    DAY_MONTH_AND_TIME("DD-MMM HH:mm"),
    LONG_DAY_MONTH_AND_TIME("ddd DD MMM HH:mm"),
    MILLISECOND("DD-MM-YYYY HH:mm:ss.SSS"),
    QUERY_DATE("YYYY-MM-DDTHH:mm:ss.SSS[Z]"),
    FULL_DATE_AND_TIME("DD-MM-YYYY HH:mm:ss"),
    DRAW_DATE("ddd DD MMM HH:mm"),
    TIME("HH:mm"),
    FULL_TIME("HH:mm:ss"),

    // NEW
    // date-fns
    // https://date-fns.org/v2.28.0/docs/format
    NEW_DAY_MTH("dd/MM"),
    NEW_FULL_DATE_AND_TIME("dd-MM-yyyy HH:mm:ss"),
    NEW_DATE_AND_TIME("dd-MMM-YY HH:mm"),
    NEW_DAY_MONTH_AND_TIME("dd-MMM HH:mm"),
    NEW_DRAW_DATE("ddd DD MMM HH:mm"),
    LONG_DAY("eeee DD MMM HH:mm");


    private final String format;

    UiDateFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

}
