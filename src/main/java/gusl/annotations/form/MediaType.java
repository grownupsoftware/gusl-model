package gusl.annotations.form;

public enum MediaType {
    // will cascade up
    Mobile,
    Tablet,
    Laptop,
    Desktop,
    ExtraLarge,
    // no casacde
    MobileOnly,
    MobileAndTabletOnly,

    TabletOnly,
    LaptopOnly,
    DesktopOnly,
    ExtraLargeOnly,
    NotMobileOrTablet,
    Never

}
