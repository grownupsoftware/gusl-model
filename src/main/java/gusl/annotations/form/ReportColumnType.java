package gusl.annotations.form;

public enum ReportColumnType {
    chart,
    decimal,
    image,
    money,
    movement,
    position,
    number,
    report,
    text,
    blank,
    circle,
    percentage,
    date_time,
    breadcrumb,

    total,
    iframe,
    external_link,
    nested_table,
    odds,
    button
}
