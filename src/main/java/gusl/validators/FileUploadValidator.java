package gusl.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

/**
 * Validator for the FileUpload annotation.The validators is primarily used by
 * the UI to identify that a file should be uploaded. Checks that the list is
 * not empty and file names are not null or empty
 *
 * @author dhudson - 10-Dec-2014
 * @since 1.0
 */
public class FileUploadValidator implements ConstraintValidator<FileUpload, List<String>> {

    @Override
    public void initialize(FileUpload email) {
    }

    @Override
    public boolean isValid(List<String> listFilenames, ConstraintValidatorContext cvc) {

        if (listFilenames == null || listFilenames.isEmpty()) {
            return false;
        }

        for (String filename : listFilenames) {
            if (filename == null || filename.isEmpty()) {
                return false;
            }
        }
        return true;

    }

}
