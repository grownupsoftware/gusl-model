package gusl.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Address Validator.
 *
 * @author dhudson
 */
public class AddressValidator implements ConstraintValidator<Address, String> {

    @Override
    public void initialize(Address constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return ValidatorsRegex.ADDRESS_LINE_PATTERN.matcher(value).matches();
    }

}
