package gusl.validators;

import gusl.core.utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;

/**
 * Date validators. Validates that date must be specified in yyyy-MM-dd format.
 *
 * @author ra created on 09.09.2014.
 */
public class DateValidator implements ConstraintValidator<DateValidation, String> {

    @Override
    public void initialize(DateValidation dateValidation) {

    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {

        // Its optional right
        if (date != null) {
            try {
                Utils.parseDate(date);
            } catch (ParseException ignore) {
                return false;
            }
        }

        return true;
    }
}
