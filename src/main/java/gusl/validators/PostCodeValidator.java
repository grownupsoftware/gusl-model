package gusl.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * PostCode Validator.
 *
 * @author dhudson
 */
public class PostCodeValidator implements ConstraintValidator<PostCode, String> {

    @Override
    public void initialize(PostCode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return ValidatorsRegex.POSTCODE_PATTERN.matcher(value).matches();
    }

}
