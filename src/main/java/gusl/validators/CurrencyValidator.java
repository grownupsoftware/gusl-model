package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dhudson - 27-Jan-2015
 * @since 
 */
public class CurrencyValidator implements ConstraintValidator<CurrencyCode, String> {

    public static final Pattern currencyPattern = Pattern.compile("^([A-Z]{3})$");

    @Override
    public void initialize(CurrencyCode a) {
    }

    @Override
    public boolean isValid(String code, ConstraintValidatorContext cvc) {
        if (StringUtils.isBlank(code)) {
            return false;
        }

        Matcher matcher = currencyPattern.matcher(code);
        return matcher.matches();
    }

}
