package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Amount Validator
 *
 * @author dhudson - 22-Jan-2015
 * @since 1.0
 */
public class AmountValidator implements ConstraintValidator<Amount, String> {

    private static final Pattern numberPattern = Pattern.compile("^[0-9]*\\.[0-9]{2}$");

    @Override
    public void initialize(Amount a) {
    }

    @Override
    public boolean isValid(String amount, ConstraintValidatorContext cvc) {
        if (StringUtils.isBlank(amount)) {
            return false;
        }

        Matcher matcher = numberPattern.matcher(amount);
        return matcher.matches();
    }

}
