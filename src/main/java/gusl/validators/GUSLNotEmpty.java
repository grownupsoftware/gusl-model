package gusl.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * @author grant
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = GUSLNotEmptyValidator.class)
@Documented
public @interface GUSLNotEmpty {

    String message() default "{gusl.validation.not.empty}";

    /**
     * @return The groups the constraint belongs to.
     */
    Class<?>[] groups() default {};

    
    /**
     * @return The payload associated to the constraint
     */
    Class<? extends Payload>[] payload() default {};
}
