package gusl.validators;

import gusl.core.utils.DateFormats;
import gusl.core.utils.StringUtils;
import lombok.CustomLog;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

/**
 * Check to see if the Date Of Birth is valid.
 * <p>
 * This uses a Date Format of yyyy/MM/dd.
 * <p>
 * To see if the person is 18+.
 *
 * @author dhudson
 * @since 1.0
 */
@CustomLog
public class DoBValidator implements ConstraintValidator<DoB, String> {

    private static final int MUST_BE_THIS_AGE = 18;
    private static final int MUST_BE_UNDER_THIS_AGE = 150;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Override
    public void initialize(DoB annotation) {
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext cvc) {

        if (StringUtils.isBlank(date)) {
            return false;
        }

        try {
            SimpleDateFormat dateFormatter = DateFormats.getUTCFormatFor("dd/MM/yyyy");
            dateFormatter.parse(date);

            return isOver18(LocalDate.parse(date, DATE_FORMATTER));
        } catch (DateTimeParseException | ParseException ex) {
            logger.info("Parse Error ...{}", date);
            return false;
        }
    }

    public static boolean isOver18(LocalDate dob) {
        int age = (int) ChronoUnit.YEARS.between(dob, LocalDate.now());
        return (age >= MUST_BE_THIS_AGE && age < MUST_BE_UNDER_THIS_AGE);
    }
}
