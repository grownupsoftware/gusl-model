package gusl.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = GUSLMinLengthValidator.class)
@Documented
public @interface GUSLMinLength {
    long min() default 0;

    String message() default "{gusl.validation.min.length}";

    /**
     * @return The groups the constraint belongs to.
     */
    Class<?>[] groups() default {};


    /**
     * @return The payload associated to the constraint
     */
    Class<? extends Payload>[] payload() default {};
}
