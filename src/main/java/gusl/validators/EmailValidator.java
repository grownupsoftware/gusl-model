package gusl.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator for the Email annotation.
 *
 * @author dhudson - 10-Dec-2014
 * @since 1.0
 */
public class EmailValidator implements ConstraintValidator<Email, String> {

    @Override
    public void initialize(Email email) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext cvc) {
        return ValidatorsRegex.EMAIL_PATTERN.matcher(value).matches();
    }

}
