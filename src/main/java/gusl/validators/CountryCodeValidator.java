package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Country Code Validator.
 *
 * @author dhudson - 28-Jan-2015
 * @since 1.0
 */
public class CountryCodeValidator implements ConstraintValidator<CountryCode, String> {

    public static final Pattern countryPattern = Pattern.compile("^([A-Z]{2})$");

    @Override
    public void initialize(CountryCode a) {
    }

    @Override
    public boolean isValid(String code, ConstraintValidatorContext cvc) {
        if (StringUtils.isBlank(code)) {
            return false;
        }

        Matcher matcher = countryPattern.matcher(code);
        return matcher.matches();
    }

}
