/* Copyright lottomart */
package gusl.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author grant
 */
public class MobileNumberValidator implements ConstraintValidator<MobileNumber, String> {

    @Override
    public void initialize(MobileNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return ValidatorsRegex.MOBILE_TELEPHONE_NUMBER_PATTERN.matcher(value).matches();
    }

}
