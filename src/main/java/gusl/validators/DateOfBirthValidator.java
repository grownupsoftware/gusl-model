package gusl.validators;

import gusl.core.utils.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

/**
 * Check to see if the Date Of Birth is valid.
 *
 * This uses a Date Format of dd-MM-yy.
 *
 * To see if the person is 18+.
 *
 * @author dhudson - 19-Aug-2014
 * @since 1.0
 */
public class DateOfBirthValidator implements ConstraintValidator<DateOfBirth, Date> {

    @Override
    public void initialize(DateOfBirth annotation) {
    }

    @Override
    public boolean isValid(Date date, ConstraintValidatorContext cvc) {

        if (date == null) {
            return false;
        }

        return DoBValidator.isOver18(Utils.asLocalDate(date));
    }

}
