package gusl.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = GUSLMaxLengthValidator.class)
@Documented
public @interface GUSLMaxLength {
    long max () default 0;

    String message() default "{gusl.validation.max.length}";

    /**
     * @return The groups the constraint belongs to.
     */
    Class<?>[] groups() default {};


    /**
     * @return The payload associated to the constraint
     */
    Class<? extends Payload>[] payload() default {};
}
