package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

import static java.util.Objects.isNull;

public class GUSLMinLengthValidator  implements ConstraintValidator<GUSLMinLength, Object> {

    GUSLMinLength theValidationAnnotation;

    @Override
    public void initialize(GUSLMinLength validationAnnotation) {
        theValidationAnnotation = validationAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext cvc) {
        if (isNull(value)) {
            return false;
        }


        if (value instanceof String) {
            if (StringUtils.isBlank((String) value)) {
                return false;
            } else {
                return ((String) value).trim().length() >= theValidationAnnotation.min();
            }
        }
        if (value instanceof Collection) {
            return ((Collection<?>) value).size() >= theValidationAnnotation.min();
        }

        return value.toString().length()  >= theValidationAnnotation.min();
    }
}
