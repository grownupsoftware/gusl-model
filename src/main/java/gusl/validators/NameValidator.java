package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author dhudson
 */
public class NameValidator implements ConstraintValidator<Name, String> {

    @Override
    public void initialize(Name name) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext cvc) {

        if (StringUtils.isBlank(value)) {
            return false;
        }

        if (value.length() == 1 || value.length() > 64) {
            return false;
        }

        return ValidatorsRegex.SURNAME_PATTERN.matcher(value).matches();
    }

}
