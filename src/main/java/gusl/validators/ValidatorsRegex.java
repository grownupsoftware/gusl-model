package gusl.validators;

import java.util.regex.Pattern;

/**
 * Collection of well known Regex patterns for validation.
 *
 * @author dhudson
 */
public interface ValidatorsRegex {

    public static final String DOMAIN_REGEX = "^([a-zA-Z0-9]+([-.][a-zA-Z0-9]+)*){2,255}$";
    public static final Pattern DOMAIN_PATTERN = Pattern.compile(DOMAIN_REGEX);

    public static final String EMAIL_REGEX = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    //"^((\\w+([-+.]\\w+)*@[a-zA-Z0-9]+([-.][a-zA-Z0-9]+)*)*){3,320}$";
    public static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);


    public static final String FORENAME_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ]{1,256}$";
    public static final Pattern FORENAME_PATTERN = Pattern.compile(FORENAME_REGEX);

    public static final String SURNAME_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ]{1,256}$";
    public static final Pattern SURNAME_PATTERN = Pattern.compile(SURNAME_REGEX);

    public static final String ELECTRIC_NUMBER_REGEX = "^([0-9]{0,2})([0-9]{0,4})([0-9]{0,4})([0-9]{0,3})$";
    public static final Pattern ELECTRIC_NUMBER_PATTERN = Pattern.compile(ELECTRIC_NUMBER_REGEX);

    public static final String TELEPHONE_NUMBER_REGEX = "^[0-9\\s\\(\\)+x-]{0,256}$";
    public static final Pattern TELEPHONE_NUMBER_PATTERN = Pattern.compile(TELEPHONE_NUMBER_REGEX);

    public static final String MOBILE_TELEPHONE_NUMBER_REGEX = "^[+0-9]{0,256}$";
    public static final Pattern MOBILE_TELEPHONE_NUMBER_PATTERN = Pattern.compile(MOBILE_TELEPHONE_NUMBER_REGEX);

    public static final String CARD_NUMBER_REGEX = "^[\\s0-9]{0,19}$";
    public static final Pattern CARD_NUMBER_PATTERN = Pattern.compile(CARD_NUMBER_REGEX);

    public static final String CARD_ISSUE_NUMBER_REGEX = "^[a-zA-Z0-9-.]{0,2}$";
    public static final Pattern CARD_ISSUE_NUMBER_PATTERN = Pattern.compile(CARD_ISSUE_NUMBER_REGEX);

    public static final String CARD_VERIFICATION_CODE_REGEX = "^[\\s0-9]{0,4}$";
    public static final Pattern CARD_VERIFICATION_CODE_PATTERN = Pattern.compile(CARD_VERIFICATION_CODE_REGEX);

    public static final String CARD_HOLDER_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{0,256}$";
    public static final Pattern CARD_HOLDER_PATTERN = Pattern.compile(CARD_HOLDER_REGEX);

    public static final String CARD_PIN_REGEX = "^[0-9\\ ]{4,4}$";
    public static final Pattern CARD_PIN_PATTERN = Pattern.compile(CARD_PIN_REGEX);

    public static final String INTERNATIONAL_PASSPORT_NUMBER_REGEX = "^([a-zA-Z0-9<]{0,9})([0-9]{0,1})([a-zA-Z<]{0,3})([0-9]{0,7})([a-zA-Z<]{0,1})([0-9]{0,7})([a-zA-Z0-9<]{0,14})([0-9<]{0,1})([0-9]{0,1})$";
    public static final Pattern INTERNATIONAL_PASSPORT_NUMBER_PATTERN = Pattern.compile(INTERNATIONAL_PASSPORT_NUMBER_REGEX);

    public static final String PASSPORT_NUMBER_REGEX = "^([a-zA-Z0-9<]{0,10})([a-zA-Z<]{0,3})([0-9]{0,7})([a-zA-Z<]{0,1})([0-9]{0,7})([0-9]{0,2})$";
    public static final Pattern PASSPORT_NUMBER_PATTERN = Pattern.compile(PASSPORT_NUMBER_REGEX);

    public static final String SHORT_PASSPORT_NUMBER_REGEX = "^[a-zA-Z0-9<]{0,15}$";
    public static final Pattern SHORT_PASSPORT_NUMBER_PATTERN = Pattern.compile(SHORT_PASSPORT_NUMBER_REGEX);

    public static final String ADDRESS_LINE_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{1,256}$";
    public static final Pattern ADDRESS_LINE_PATTERN = Pattern.compile(ADDRESS_LINE_REGEX);

    public static final String IPADDERSS_REGEX = "^((UNKNOWN)|(\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b)){0,15}$";
    public static final Pattern IPADDRESS_PATTERN = Pattern.compile(IPADDERSS_REGEX);

    public static final String COUNTRY_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{1,256}$";
    public static final Pattern COUNTRY_PATTERN = Pattern.compile(COUNTRY_REGEX);

    public static final String STATE_REGEX = "^[\\sa-zA-Z0-9&/`´'-.,;\"!_ÀÁÂÃÄÅÆàáâãäåąæßÇçćčÐðÈÉÊËèéêëěğĞÌÍÎÏİıìíîïÒÓÔÕÖØøòóôõöŁłÑñńŞşšśÙÚÛÜùúûüÝŸýÿŻžżþÞ@]{1,256}$";
    public static final Pattern STATE_PATTERN = Pattern.compile(STATE_REGEX);

    public static final String POSTCODE_REGEX = "^[\\sa-zA-Z0-9-.]{2,8}$";
    public static final Pattern POSTCODE_PATTERN = Pattern.compile(POSTCODE_REGEX);

    public static final String THREED_SECURE_RESPONSE_REGEX = "^[a-zA-Z0-9/\\+-;:/-/\\\"=]{0,1024}$";
    public static final Pattern THREED_SECURE_RESPONSE_PATTERN = Pattern.compile(THREED_SECURE_RESPONSE_REGEX);

    public static final String BANK_ACCOUNT_NUMBER_REGEX = "^[0-9]{0,10}$";
    public static final Pattern BANK_ACCOUNT_NUMBER_PATTERN = Pattern.compile(BANK_ACCOUNT_NUMBER_REGEX);

    public static final String SORT_CODE_REGEX = "^[0-9]{0,6}$";
    public static final Pattern SORT_CODE_PATTERN = Pattern.compile(SORT_CODE_REGEX);

    public static final String NATIONAL_INSURANCE_REGEX = "^([a-zA-Z0-9]{0,2})([a-zA-Z0-9]{0,2})([a-zA-Z0-9]{0,2})([a-zA-Z0-9]{0,2})([a-zA-Z0-9\\s]{0,1})$";
    public static final Pattern NATIONAL_INSURANCE_PATTERN = Pattern.compile(NATIONAL_INSURANCE_REGEX);

    public static final String PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!$%@#£€*?&.]{6,}$";
    public static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);
}
