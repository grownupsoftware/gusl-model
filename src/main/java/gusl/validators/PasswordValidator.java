package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Password Validator.
 *
 * Password needs to be 6 or more characters and contain a digit
 *
 * @author dhudson - 19-Aug-2014
 * @since 1.0
 */
public class PasswordValidator implements ConstraintValidator<Password, String> {

    private static final int PASSWORD_MIN_LENGTH = 6;
    private static final int PASSWORD_MAX_LENGTH = 64;

    @Override
    public void initialize(Password annotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext cvc) {
        if (StringUtils.isBlank(password)) {
            return false;
        }

        final int l = password.length();

        if (l < PASSWORD_MIN_LENGTH || l >= PASSWORD_MAX_LENGTH) {
            return false;
        }

        // We need at least one letter and one character
        boolean hasCharacter = false;
        boolean hasDigit = false;

        for (int i = 0; i < l; i++) {
            int c = password.codePointAt(i);
            if (Character.isWhitespace(c)) {
                return false;
            }
            if (Character.isDigit(c)) {
                hasDigit = true;
            }
            if (Character.isLetter(c)) {
                hasCharacter = true;
            }
        }

        return hasCharacter && hasDigit;
    }

}
