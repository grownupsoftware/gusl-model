package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

import static java.util.Objects.isNull;

public class GUSLMaxLengthValidator implements ConstraintValidator<GUSLMaxLength, Object> {

    GUSLMaxLength theValidationAnnotation;

    @Override
    public void initialize(GUSLMaxLength validationAnnotation) {
        theValidationAnnotation = validationAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext cvc) {
        if (isNull(value)) {
            return false;
        }

        if (value instanceof String) {
            if (StringUtils.isBlank((String) value)) {
                return false;
            } else {
                return ((String) value).trim().length() <= theValidationAnnotation.max();
            }
        }
        if (value instanceof Collection) {
            return ((Collection<?>) value).size() <= theValidationAnnotation.max();
        }

        return value.toString().length() <= theValidationAnnotation.max();
    }
}
