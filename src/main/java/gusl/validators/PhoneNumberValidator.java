package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Driver for the @PhoneNumber validators annotation.
 * 
 * Checks to see if the supplied number is null, empty and matches the phone number regex pattern.
 * 
 * Industry-standard notation specified by ITU-T E.123. This notation requires that international phone numbers include
 * a leading plus sign (known as the international prefix symbol), and allows only spaces to separate groups of digits.
 * 
 * Thanks to the international phone numbering plan (ITU-T E.164), phone numbers cannot contain more than 15 digits. 
 * The shortest international phone numbers in use contain seven digits.
 * 
 * @author dhudson - created 29 May 2014
 * @since 1.0
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber,String> {

    /*
     * ^      Assert position at the beginning of the string.
     * \+     Match a literal "+" character.
     * (?:    Group but don't capture...
     * [0-9]  Match a digit.
     *\x20    Match a space character...
     * ?      Between zero and one time.
     * )      End the noncapturing group.
     * {6,14} Repeat the preceding group between 6 and 14 times.
     * [0-9]  Match a digit.
     * $      Assert position at the end of the string.
     */
    private static final Pattern PHONE_PATTERN = Pattern.compile("^\\+(?:[0-9]??){6,14}[0-9]$");
    
    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value,ConstraintValidatorContext context) {

        if(StringUtils.isBlank(value)) {
            return false;
        }
        
        Matcher matcher = PHONE_PATTERN.matcher(value);
        return matcher.matches();
    }
}
