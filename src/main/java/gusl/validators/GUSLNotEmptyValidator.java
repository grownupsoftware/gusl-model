package gusl.validators;

import gusl.core.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

/**
 *
 * @author grant
 */
public class GUSLNotEmptyValidator implements ConstraintValidator<GUSLNotEmpty, Object> {

    GUSLNotEmpty theMbNotEmpty;

    @Override
    public void initialize(GUSLNotEmpty mbNotEmpty) {
        theMbNotEmpty = mbNotEmpty;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext cvc) {
        if (value == null) {
            return false;
        }
        
        if (value instanceof String) {
            return !StringUtils.isBlank((String) value);
        }
        
        if (value instanceof Collection) {
            return !((Collection<?>) value).isEmpty();
        }
        
        return true;
    }
}
