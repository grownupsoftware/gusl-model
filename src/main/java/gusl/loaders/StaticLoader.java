package gusl.loaders;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.*;
import gusl.errors.ModelErrors;
import lombok.CustomLog;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://stackoverflow.com/questions/51158173/parsing-csv-into-a-pojo-list-with-jackson-csv
 *
 * @author dhudson
 * @since 06/05/2021
 */
@CustomLog
public class StaticLoader {

    private static final String EMPTY_STRING = "";

    private StaticLoader() {
    }

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private static final String TIMESTAMP = "${timestamp}";
    private static Pattern INCLUDE = Pattern.compile("\\$\\{include\\:(.+?)\\}");
    private static final String COMMENT_PREFIX = "---";

    private static final CsvMapper theMapper = createCsvMapper();

    public static CsvMapper getCSVMapper() {
        return theMapper;
    }

    private static CsvMapper createCsvMapper() {
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.TRIM_SPACES);
        mapper.enable(CsvParser.Feature.ALLOW_TRAILING_COMMA);
        mapper.enable(CsvParser.Feature.INSERT_NULLS_FOR_MISSING_COLUMNS);
        mapper.enable(CsvParser.Feature.SKIP_EMPTY_LINES);
        mapper.enable(JsonGenerator.Feature.IGNORE_UNKNOWN);
        mapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT));
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new Jdk8Module());
        mapper.addHandler(new DeserializationProblemHandler() {
            @Override
            public Object handleMissingInstantiator(DeserializationContext ctxt, Class<?> instClass, ValueInstantiator valueInsta, JsonParser p, String msg) throws IOException {
                String possibleJSON = p.getValueAsString();
                logger.debug(".........................  handleMissingInstantiator instClass {}, {}", instClass, possibleJSON);
                if (StringUtils.isBlank(possibleJSON)) {
                    return null;
                }
                return ObjectMapperFactory.getDefaultObjectMapper().readValue(possibleJSON, instClass);
            }
        });
        return mapper;
    }

    public static <T> List<T> loadCSV(String resource, Class<T> doClass) throws GUSLErrorException {
        return loadCSV(getResourceAsFile(resource), doClass);
    }

    public static <T> List<T> loadCSV(File resource, Class<T> doClass) throws GUSLErrorException {
        try {
            CsvSchema schema = CsvSchema.emptySchema().withHeader().withQuoteChar('|').withEscapeChar('\\');
            MappingIterator<T> it = getCSVMapper().readerFor(doClass).with(schema).readValues(loadContent(resource));
            return it.readAll();
        } catch (IOException ex) {
            logger.warn("Unable to load {}", resource, ex);
            throw ModelErrors.STATIC_DATA_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
    }

    public static <T> List<T> loadJSON(String resource, Class<T> doClass) throws GUSLErrorException {
        return loadJSON(getResourceAsFile(resource), doClass);
    }

    public static <T> List<T> loadJSON(File resource, Class<T> doClass) throws GUSLErrorException {
        try {
            ObjectMapper mapper = ObjectMapperFactory.getDefaultObjectMapper();
            CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, doClass);
            return ObjectMapperFactory.getDefaultObjectMapper().readValue(loadContent(resource), listType);
        } catch (Throwable ex) {
            logger.warn("Unable to load {}", resource, ex);
            throw ModelErrors.STATIC_DATA_ERROR.generateException(ex, ex.getLocalizedMessage());
        }
    }

    public static <T> List<T> loadResource(String resource, Class<T> doClass) throws GUSLErrorException {
        return loadFile(getResourceAsFile(resource), doClass);
    }

    public static <T> List<T> loadFile(File file, Class<T> doClass) throws GUSLErrorException {
        String name = file.getName();
        if (name.endsWith(".csv")) {
            return loadCSV(file, doClass);
        }
        if (name.endsWith(".json")) {
            return loadJSON(file, doClass);
        }
        throw ModelErrors.UNKNOWN_RESOURCE_TYPE.generateException(name);
    }

    public static String loadContent(String resource) throws GUSLErrorException {
        return loadContent(getResourceAsFile(resource));
    }

    public static String loadContent(File file) throws GUSLErrorException {
        try {
            String stuff = IOUtils.readFileAsString(file);
            stuff = stuff.replace(TIMESTAMP, LocalDateTime.now().format(DATE_TIME_FORMATTER));

            return processIncludes(stuff);

        } catch (IOException ex) {
            logger.error("Error loading content", ex);
            throw ModelErrors.RESOURCE_NOT_FOUND.generateException(ex, file.getName());
        }
    }


    public static <T> String dumpCSV(Class<T> type, Collection<T> stuff, List<String> headers, List<String> fields, Map<Class<?>, Function<Object, String>> parserMap) throws GUSLErrorException {

        StringBuilder builder = new StringBuilder(1024);
        try {
            generateHeader(builder, headers);

            Map<String, Field> fieldMap = ClassUtils.getFieldForAsMap(type, true);

            for (T entity : Utils.safeCollection(stuff)) {
                for (String field : fields) {
                    Field javaField = fieldMap.get(field);
                    if (javaField != null) {
                        Object data = javaField.get(entity);
                        if (data != null) {
                            if (parserMap.containsKey(javaField.getType())) {
                                Function<Object, String> classStringFunction = parserMap.get(javaField.getType());
                                builder.append(classStringFunction.apply(data));
                            } else {
                                builder.append(data);
                            }
                        }
                        builder.append(",");
                    }
                }
                builder.setLength(builder.length() - 1);
                builder.append(Platform.LINE_SEPARATOR);
            }
        } catch (Throwable t) {
            logger.warn("Unable to dump contents ", t);
            throw ModelErrors.STATIC_DATA_ERROR.generateException(t.getCause(), t.getLocalizedMessage());
        }
        return builder.toString();
    }

    private static void generateHeader(StringBuilder builder, List<String> headers) {
        headers.forEach(field -> builder.append(field).append(","));
        builder.setLength(builder.length() - 1);
        builder.append("\n");
    }

    private static File getResourceAsFile(String resource) throws GUSLErrorException {
        try {
            return IOUtils.getResourceAsFile(resource, StaticLoader.class.getClassLoader());
        } catch (IOException ex) {
            logger.error("Error loading content", ex);
            throw ModelErrors.RESOURCE_NOT_FOUND.generateException(ex, resource);
        }
    }

    private static String processIncludes(String content) {

        String parsed = content;
        Matcher matcher = INCLUDE.matcher(content);
        List<MatchResult> results = new ArrayList<>(2);
        while (matcher.find()) {
            results.add(matcher.toMatchResult());
        }

        if (!results.isEmpty()) {
            for (int i = results.size() - 1; i > -1; i--) {
                MatchResult matchResult = results.get(i);
                String key = matchResult.group(1);
                try {
                    String result = IOUtils.readFileAsString(IOUtils.getResourceAsFile(key, StaticLoader.class.getClassLoader()));
                    result = result.replace("\"", "\\\"");
                    result = result.replace("\n", "\\n");
                    result = result.replace("\t", "  ");
                    String prefix = parsed.substring(0, matchResult.start());
                    String suffix = parsed.substring(matchResult.end());
                    parsed = prefix + result + suffix;
                } catch (IOException ex) {
                    logger.warn("Unable to find {} for to include.", key);
                }
            }
        }
        return parsed;
    }
}
