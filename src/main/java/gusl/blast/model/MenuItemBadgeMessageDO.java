package gusl.blast.model;

import gusl.core.tostring.ToString;
import gusl.model.Identifiable;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MenuItemBadgeMessageDO implements Identifiable<String> {

    public static final String MENU_ITEM_BLAST_DELTA_COMMAND = "menuitem.badge";

    private Integer value;
    private String color;
    private String backgroundColor;
    private String menuCode;
    private String tooltip;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getId() {
        return menuCode;
    }

    @Override
    public void setId(String id) {
        menuCode = id;
    }
}
