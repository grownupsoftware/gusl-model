package gusl.blast.model;

import gusl.model.Identifiable;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SingleId implements Identifiable<String> {
    private String id;

    public static SingleId of(String id) {
        return SingleId.builder().id(id).build();
    }
}
