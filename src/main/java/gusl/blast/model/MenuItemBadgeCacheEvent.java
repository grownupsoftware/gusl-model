package gusl.blast.model;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class MenuItemBadgeCacheEvent extends AbstractCacheActionEvent<MenuItemBadgeMessageDO> {

    public MenuItemBadgeCacheEvent() {
    }

    public MenuItemBadgeCacheEvent(CacheAction action, MenuItemBadgeMessageDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
