package gusl.query;

import gusl.core.tostring.ToString;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class InQuery {

    private String field;
    @EqualsAndHashCode.Exclude
    private Object[] values;

    public static InQuery of(String field, Object... values) {
        return new InQuery(field, values);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
