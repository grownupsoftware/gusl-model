package gusl.query;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class OrderBy {

    private String field;
    @EqualsAndHashCode.Exclude
    private QueryOrder order;

    /**
     * when a multiple order "ORDER BY description ASC , datePlaced DESC , ticketId ASC "
     * positionOrder will defined the order of the order-bys.
     * 1 heighest i.e. asc
     */
    @EqualsAndHashCode.Exclude
    private Integer positionOrder;

    public OrderBy(String field) {
        this.field = field;
        order = QueryOrder.ASC;
    }

    public OrderBy(String field, QueryOrder order) {
        this.field = field;
        this.order = order;
    }

    public static OrderBy of(String field) {
        return OrderBy.builder().field(field).order(QueryOrder.ASC).build();
    }

    public static OrderBy of(String field, QueryOrder order) {
        return OrderBy.builder().field(field).order(order).build();
    }

    public static OrderBy of(String field, QueryOrder order, int positionOrder) {
        return OrderBy.builder().field(field).order(order).positionOrder(positionOrder).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
