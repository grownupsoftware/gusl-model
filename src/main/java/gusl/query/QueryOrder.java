package gusl.query;

/**
 * @author dhudson
 * @since 19/04/2021
 */
public enum QueryOrder {
    ASC,
    DESC;
}
