package gusl.query;

/**
 * @author dhudson
 * @since 04/08/2023
 */
public enum RangeQueryType {
    DATE,
    NUMBER
}
