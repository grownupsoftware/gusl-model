package gusl.query;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author dhudson
 * @since 07/08/2023
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DateRangeQuery.class, name = "DATE"),
        @JsonSubTypes.Type(value = NumberRangeQuery.class, name = "NUMBER"),
})
public interface RangeQuery {

    boolean isDateRange();

    boolean isExclusive();

    RangeQueryType getType();

    String getField();

    void setField(String field);

    String getFormatHint();

    boolean isBetween();

    RangeQuery setFrom(Object value) throws GUSLErrorException;

    RangeQuery setTo(Object value) throws GUSLErrorException;

    RangeQuery setLessThan(Object value) throws GUSLErrorException;

    RangeQuery setGreaterThan(Object value) throws GUSLErrorException;

    Object getSingleBindValue();

    Object getFrom();

    Object getTo();

    Object getLt();

    Object getGt();
}
