package gusl.query;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author dhudson
 * @since 30/04/2021
 */
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class UpdateParams {

    @Singular
    private Set<String> updateFields;

    public UpdateParams addUpdateField(String field) {
        if (updateFields == null) {
            updateFields = new LinkedHashSet<>(2);
        }

        updateFields.add(field);
        return this;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
