package gusl.query;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import gusl.core.utils.Utils;
import lombok.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author dhudson
 * @since 26/04/2021
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class QueryParams {

    // -1 is as many as the Database will allow, 10K for Elastic
    // 0 is a valid limit for elastic
    @Builder.Default
    private int limit = -1;
    private int skip;
    private boolean firstAndLast;

    @Singular
    private Set<OrderBy> orderBys;

    @Singular
    private Set<RangeQuery> rangeQueries;

    @Singular
    private Set<MatchQuery> musts;

    @Singular
    private Set<MatchQuery> mustNots;

    @Singular("should")
    private Set<MatchQuery> should;

    @Singular("distinct")
    private Set<String> distinct;

    @Singular
    private Set<InQuery> ins;

    public QueryParams addMust(String field, Object value) {
        return addMust(new MatchQuery(field, value));
    }

    public QueryParams addMust(MatchQuery query) {
        if (musts == null) {
            musts = new LinkedHashSet<>(2);
        }

        musts.add(query);
        return this;
    }

    public QueryParams addMustNot(String field, Object value) {
        return addMustNot(new MatchQuery(field, value));
    }

    public QueryParams addShould(String field, Object value) {
        return addShould(new MatchQuery(field, value));
    }

    public QueryParams addIns(InQuery query) {
        if (ins == null) {
            ins = new LinkedHashSet<>(2);
        }
        ins.add(query);
        return this;
    }

    public QueryParams addShould(MatchQuery query) {
        if (should == null) {
            should = new LinkedHashSet<>(2);
        }

        should.add(query);
        return this;
    }

    public QueryParams addMustNot(MatchQuery query) {
        if (mustNots == null) {
            mustNots = new LinkedHashSet<>(2);
        }

        mustNots.add(query);
        return this;
    }

    private void addQuery(RangeQuery rangeQuery) {
        if (rangeQueries == null) {
            rangeQueries = new LinkedHashSet<>(2);
        }
        rangeQueries.add(rangeQuery);
    }

    public DateRangeQuery addDateRangeQuery(String fieldName) {
        DateRangeQuery rangeQuery = new DateRangeQuery(fieldName);
        addQuery(rangeQuery);
        return rangeQuery;
    }

    public NumberRangeQuery addNumberRangeQuery(String fieldName) {
        NumberRangeQuery rangeQuery = new NumberRangeQuery(fieldName);
        addQuery(rangeQuery);
        return rangeQuery;
    }

    @Deprecated
    public DateRangeQuery addRangeQuery(String fieldName) {
        if (rangeQueries == null) {
            rangeQueries = new LinkedHashSet<>(2);
        }

        DateRangeQuery rangeQuery = new DateRangeQuery(fieldName);
        rangeQueries.add(rangeQuery);
        return rangeQuery;
    }

    public boolean hasDateRangeQuery() {
        if (!Utils.hasElements(rangeQueries)) {
            return false;
        }

        for (RangeQuery rangeQuery : rangeQueries) {
            if (rangeQuery.isDateRange()) {
                return true;
            }
        }

        return false;
    }

    public QueryParams addOrder(String field) {
        addOrder(new OrderBy(field));
        return this;
    }

    public QueryParams addOrder(String field, QueryOrder order) {
        addOrder(new OrderBy(field, order));
        return this;
    }

    public QueryParams addOrder(OrderBy order) {
        if (orderBys == null) {
            orderBys = new LinkedHashSet<>(3);
        }

        orderBys.add(order);
        return this;
    }

    public QueryParams addDistinct(String field) {
        if (distinct == null) {
            distinct = new LinkedHashSet<>(2);
        }
        distinct.add(field);
        return this;
    }

    @JsonIgnore
    public List<MatchQuery> getAllMatchQueries() {
        List<MatchQuery> queries = new ArrayList<>(3);
        if (musts != null) {
            queries.addAll(musts);
        }

        if (mustNots != null) {
            queries.addAll(mustNots);
        }

        if (should != null) {
            queries.addAll(should);
        }

        return queries;
    }

    public static QueryParams ofMust(String field, Object value) {
        QueryParams params = new QueryParams();
        return params.addMust(field, value);
    }

    public static QueryParams ofIns(InQuery query) {
        QueryParams params = new QueryParams();
        return params.addIns(query);
    }

    public void alias(String from, String to) {
        for (MatchQuery query : getAllMatchQueries()) {
            if (query.getField().equals(from)) {
                query.setField(to);
            }
        }

        for (OrderBy orderBy : Utils.safeCollection(orderBys)) {
            if (orderBy.getField().equals(from)) {
                orderBy.setField(to);
            }
        }

        for (RangeQuery rangeQuery : Utils.safeCollection(rangeQueries)) {
            if (rangeQuery.getField().equals(from)) {
                rangeQuery.setField(to);
            }
        }

        for (InQuery inQuery : Utils.safeCollection(ins)) {
            if (inQuery.getField().equals(from)) {
                inQuery.setField(to);
            }
        }

        if (Utils.hasElements(distinct)) {
            if (distinct.contains(from)) {
                distinct.remove(from);
                distinct.add(to);
            }
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
