package gusl.query;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import gusl.core.dates.DateMathParser;
import gusl.core.dates.GUSLDateException;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.tostring.ToString;
import gusl.core.utils.Utils;
import gusl.errors.ModelErrors;
import lombok.Getter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Getter
public class DateRangeQuery extends AbstractRangeQuery {

    public static final ZoneId ZONE_ID = ZoneId.of("UTC");

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime from;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime to;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime gt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime lt;

//    @JsonDeserialize(using = LocalDateDeserializer.class)
//    @JsonSerialize(using = LocalDateSerializer.class)
//    private LocalDate fromDate;
//
//    @JsonDeserialize(using = LocalDateDeserializer.class)
//    @JsonSerialize(using = LocalDateSerializer.class)
//    private LocalDate toDate;

    public DateRangeQuery() {
        super(RangeQueryType.DATE);
    }

    public DateRangeQuery(String field) {
        super(RangeQueryType.DATE, field);
    }

//    public boolean isLocalDateBetween() {
//        return !(toDate == null && fromDate == null);
//    }

    public static DateRangeQuery of(String field, LocalDateTime from, LocalDateTime to) throws GUSLErrorException {
        DateRangeQuery rangeQuery = new DateRangeQuery(field);
        rangeQuery.setFrom(from);
        rangeQuery.setTo(to);
        return rangeQuery;
    }

    public static DateRangeQuery of(String field, LocalDate from, LocalDate to) throws GUSLErrorException {
        DateRangeQuery rangeQuery = new DateRangeQuery(field);
        rangeQuery.setFrom(from);
        rangeQuery.setTo(to);
        return rangeQuery;
    }

    @Override
    public DateRangeQuery setFrom(Object date) throws GUSLErrorException {
        from = parseDate(date, ZONE_ID);
        return this;
    }

    @Override
    public DateRangeQuery setTo(Object date) throws GUSLErrorException {
        to = parseDate(date, ZONE_ID);
        return this;
    }

    @Override
    public DateRangeQuery setLessThan(Object date) throws GUSLErrorException {
        lt = parseDate(date, ZONE_ID);
        return this;
    }

    @Override
    public DateRangeQuery setGreaterThan(Object date) throws GUSLErrorException {
        gt = parseDate(date, ZONE_ID);
        return this;
    }

    public static LocalDateTime parseDate(Object value, ZoneId zoneId) throws GUSLErrorException {
        if (value == null) {
            return null;
        }
        if (value instanceof LocalDateTime) {
            return (LocalDateTime) value;
        }
        if (value instanceof Date) {
            return Utils.asLocalDateTime((Date) value, zoneId);
        }
        if (value instanceof Instant) {
            return LocalDateTime.ofInstant((Instant) value, zoneId);
        }
        if (value instanceof LocalDate) {
            return ((LocalDate) value).atStartOfDay();
        }
        if (value instanceof String) {
            try {
                return LocalDateTime.ofInstant(DateMathParser.parse((String) value), zoneId);
            } catch (GUSLDateException ex) {
                throw ModelErrors.RANGE_ERROR.generateException(value.toString());
            }
        }
        throw ModelErrors.RANGE_ERROR.generateException(value.toString());
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
