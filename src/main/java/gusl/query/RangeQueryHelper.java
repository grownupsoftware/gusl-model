package gusl.query;

import gusl.core.utils.Utils;

import java.util.Set;

/**
 * @author dhudson
 * @since 08/08/2023
 */
public class RangeQueryHelper {

    public static void processRangeQuery(StringBuilder builder, Set<RangeQuery> queries, boolean isFirst, boolean hasWhere) {
        if (hasWhere) {
            processRangeQuery(builder, queries, false);
        } else {
            processRangeQuery(builder, queries, isFirst);
        }
    }

    public static void processRangeQuery(StringBuilder builder, Set<RangeQuery> queries, boolean isFirst) {
        for (RangeQuery query : Utils.safeCollection(queries)) {
            if (query.isBetween()) {
                isFirst = getFirstPrefix(builder, isFirst);
                builder.append(query.getField());
                if (query.isExclusive()) {
                    builder.append(" >?");
                } else {
                    builder.append(" >=?");
                }
                builder.append(" AND ");
                builder.append(query.getField());
                if (query.isExclusive()) {
                    builder.append(" <? ");
                } else {
                    builder.append(" <=? ");
                }
                continue;
            }

            if (query.getLt() != null) {
                isFirst = getFirstPrefix(builder, isFirst);
                builder.append(query.getField());
                if (query.isExclusive()) {
                    builder.append(" <? ");
                } else {
                    builder.append(" <=? ");
                }
                continue;
            }

            if (query.getGt() != null) {
                isFirst = getFirstPrefix(builder, isFirst);
                builder.append(query.getField());
                if (query.isExclusive()) {
                    builder.append(" >? ");
                } else {
                    builder.append(" >=? ");
                }
            }
        }
    }

    private static boolean getFirstPrefix(StringBuilder builder, boolean isFirst) {
        if (isFirst) {
            builder.append(" WHERE ");
        } else {
            builder.append(" AND ");
        }
        return false;
    }
}
