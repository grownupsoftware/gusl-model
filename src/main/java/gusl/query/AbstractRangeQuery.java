package gusl.query;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 * @since 04/08/2023
 */
@Getter
@Setter
public abstract class AbstractRangeQuery implements RangeQuery {

    final private RangeQueryType type;
    private String field;
    private String formatHint;
    private boolean exclusive;

    public AbstractRangeQuery(RangeQueryType type) {
        this.type = type;
    }

    public AbstractRangeQuery(RangeQueryType type, String field) {
        this.type = type;
        this.field = field;
    }

    @Override
    public Object getSingleBindValue() {
        if (getLt() != null) {
            return getLt();
        }
        if (getGt() != null) {
            return getGt();
        }
        return null;
    }

    @JsonIgnore
    @Override
    public boolean isBetween() {
        return !(getTo() == null && getFrom() == null);
    }

    @JsonIgnore
    @Override
    public boolean isDateRange() {
        return type == RangeQueryType.DATE;
    }

    public String toString() {
        return ToString.toString(this);
    }
}
