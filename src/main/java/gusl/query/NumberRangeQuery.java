package gusl.query;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.tostring.ToString;
import gusl.errors.ModelErrors;
import lombok.Getter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * @author dhudson
 * @since 04/08/2023
 */
@Getter
public class NumberRangeQuery extends AbstractRangeQuery {

    private Number from;
    private Number to;
    private Number gt;
    private Number lt;

    public NumberRangeQuery() {
        super(RangeQueryType.NUMBER);
    }

    public NumberRangeQuery(String field) {
        super(RangeQueryType.NUMBER, field);
    }

    @Override
    public RangeQuery setFrom(Object value) throws GUSLErrorException {
        from = parseValue(value);
        return this;
    }

    @Override
    public RangeQuery setTo(Object value) throws GUSLErrorException {
        to = parseValue(value);
        return this;
    }

    @Override
    public RangeQuery setLessThan(Object value) throws GUSLErrorException {
        lt = parseValue(value);
        return this;
    }

    @Override
    public RangeQuery setGreaterThan(Object value) throws GUSLErrorException {
        gt = parseValue(value);
        return this;
    }

    private Number parseValue(Object value) throws GUSLErrorException {
        if (value == null) {
            return null;
        }

        Class<?> aClass = value.getClass();
        if (Number.class.isAssignableFrom(aClass)) {
            return (Number) value;
        }

        try {
            if (aClass == String.class) {
                return NumberFormat.getInstance().parse((String) value);
            }

        } catch (ParseException ex) {
            throw ModelErrors.RANGE_ERROR.generateException(value.toString());
        }

        if (aClass == BigDecimal.class) {
            return ((BigDecimal) value).doubleValue();
        }

        throw ModelErrors.RANGE_ERROR.generateException(value.toString());
    }

    public String toString() {
        return ToString.toString(this);
    }
}
