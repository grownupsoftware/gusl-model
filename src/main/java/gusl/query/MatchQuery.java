package gusl.query;

import gusl.core.tostring.ToString;
import lombok.*;

import static java.util.Objects.isNull;

/**
 * @author dhudson
 * @since 27/04/2021
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class MatchQuery {

    private String field;
    private Object value;
    @EqualsAndHashCode.Exclude
    private boolean fuzzy;

    @EqualsAndHashCode.Exclude
    private boolean caseInsensitive;

    public MatchQuery(String field, Object value) {
        this(field, value, false, false);
    }

    public static MatchQuery of(String field, Object value) {
        return new MatchQuery(field, value, false, false);
    }

    public static MatchQuery of(String field, Object value, boolean fuzzy) {
        return new MatchQuery(field, value, fuzzy, false);
    }

    public static MatchQuery ofLower(String field, String value) {
        return new MatchQuery(field, isNull(value) ? null : value.toLowerCase(), false, true);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
