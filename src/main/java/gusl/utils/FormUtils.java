package gusl.utils;

public class FormUtils {
//
//    protected final static GUSLLogger logger = GUSLLogManager.getLogger(FormUtils.class);
//
//    public static List<FormRowConfigDO> getRowsConfig(Class dtoClass) {
//
//        if (dtoClass.isAnnotationPresent(Grouped.class)) {
//            return getFieldsFor(dtoClass)
//                    .stream().map(f -> getRowConfig(f.getClass()))
//                    .filter(f -> !Objects.isNull(f))
//                    .collect(Collectors.toList());
//        } else {
//            return Collections.singletonList(getRowConfig(dtoClass));
//        }
//    }
//
//    public static List<Field> getFieldsFor(Class<?> clazz) {
//        List<Field> fields = new ArrayList<>();
//        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
//
//        Class<?> parentClass = clazz.getSuperclass();
//
//        while (parentClass != null) {
//            fields.addAll(0, Arrays.asList(parentClass.getDeclaredFields()));
//            parentClass = parentClass.getSuperclass();
//        }
//
//        return fields;
//    }
//
//    public static FormRowConfigDO getRowConfig(Class clazz) {
//        ColumnBuilder.Builder columnBuilder = new ColumnBuilder.Builder();
//
//        safeStream(getFieldsFor(clazz))
//                .map(FormUtils::createField)
//                .filter(Optional::isPresent)
//                .map(Optional::get)
//                .forEach(columnBuilder::field);
//
//        return new RowBuilder.Builder().column(columnBuilder.build()).build();
//    }
//
//    public static Optional<FieldConfigDO> createField(Field field) {
//        return Optional.ofNullable(field.getAnnotation(UiField.class))
//                .filter(def -> !def.onlyFilter())
//                .map(def -> createField(field, def));
//    }
//
//
//    @SuppressWarnings("unchecked")
//    private static FieldConfigDO createField(Field field, UiField ui) {
//        try {
//            FieldBuilder.Builder builder =  new FieldBuilder.Builder();
//
//            builder.type(ui.type());
//            builder.name(field.getName());
//
//            if (!ui.label().isEmpty()) {
//                builder.label(ui.label());
//            }
//
//            if (ui.viewHide()) {
//                builder.viewHide();
//            }
//
//            if (ui.editHide()) {
//                builder.editHide();
//            }
//
//            if (ui.deleteHide()) {
//                builder.deleteHide();
//            }
//
//            builder.editReadOnly(Strings.emptyToNull(ui.editReadOnly()));
//            builder.newTableElementUrl(Strings.emptyToNull(ui.newTableElementUrl()));
//            builder.expandablePanel(ui.jsExpandablePanel());
//            builder.strippedRows(ui.strippedRows());
//            builder.hideIfEmpty(ui.hideIfEmpty());
//            builder.mediaType(ui.mediaType());
//            builder.displayOrder(ui.displayOrder());
//
//            if (ui.addReadOnly()) {
//                builder.addReadOnly();
//            }
//
//            if (ui.displayInTable()) {
//                builder.displayInTable();
//            }
//
//            if (ui.displayWhenSelected()) {
//                builder.displayWhenSelected();
//            }
//
//            if (ui.filterInTable()) {
//                builder.filterInTable();
//            }
//            if (ui.addIgnore()) {
//                builder.addIgnore();
//            }
//
//            // builder.advancedFilter();
//            if (ui.noLabel()) {
//                builder.noLabel();
//            }
//
//            if (ui.formFullWidth()) {
//                builder.formFullWidth(Boolean.TRUE);
//            }
//
//            if (ui.type().equals(FieldType.draw_options)
//                    || ui.type().equals(FieldType.booster)
//                    || ui.type().equals(FieldType.report)
//                    || ui.type().equals(FieldType.prize_table)
//                    || ui.type().equals(FieldType.nested_table)) {
//                builder.formFullWidth(Boolean.TRUE);
//            }
//
//            if (!ui.propertyName().isEmpty()) {
//                builder.propertyName(ui.propertyName());
//            }
//
//            if (ui.type().equals(FieldType.option)) {
//                if (ui.lookupSelectUrl().isEmpty() && !field.getType().equals(String.class)) {
//                    builder.options((Class<? extends Enum<?>>) field.getType());
//                }
//            }
//
//            if (ui.type().equals("odds")) {
//                builder.optionsFromEnumFilter(field.getType());
////                if (ui.lookupSelectUrl().isEmpty() && !field.getType().equals(String.class)) {
////                    builder.options((Class<? extends Enum<?>>) field.getType());
////                }
//            }
//
//            // multi
//            if (ui.type().equals(FieldType.multi_option) && !ui.optionClass().equals(Enum.class)) {
//                builder.options((Class<? extends Enum<?>>) ui.optionClass());
//            }
//
//            if (ui.lookup() != LookupType.none) {
//                builder.lookup(ui.lookup());
//            }
//
//            if (StringUtils.isNotBlank(ui.lookupCode())) {
//                builder.lookupCode(ui.lookupCode());
//            }
//
//            Optional.ofNullable(Strings.emptyToNull(ui.lookupInsertUrl()))
//                    .ifPresent(d -> builder.setInsertUrl(d));
//
//            if (!ui.lookupSelectUrl().isEmpty()) {
//                builder.selectUrl(ui.lookupSelectUrl());
//                builder.lookupSelectUrl(ui.lookupSelectUrl());
//            }
//            if (!ui.lookupSearchUrl().isEmpty()) {
//                builder.lookupSearchUrl(ui.lookupSearchUrl());
//            }
//            if (!ui.lookupOverwritesForm()) {
//                builder.lookupOverwritesForm(ui.lookupOverwritesForm());
//            }
//
//            builder.lookupModalLayout(ui.lookupModalLayout());
//
//            if (!ui.moneyFormat().isEmpty()) {
//                builder.moneyFormat(ui.moneyFormat());
//            }
//
//            if (ui.dateFormat() != UiDateFormat.NONE) {
//                builder.dateFormat(ui.dateFormat().getFormat());
//                builder.child(ui.child());
//            }
//
//            if (!ui.category().isEmpty()) {
//                builder.category(ui.category());
//            }
//
//            FieldConfigDO fieldConfig = builder.build();
//
//            fieldConfig.setCanAdd(Strings.emptyToNull(ui.canAdd()));
//
//            fieldConfig.setPermission(Strings.emptyToNull(ui.permission()));
//
//            if (!ui.entryCss().isEmpty()) {
//                fieldConfig.setEntryCss(ui.entryCss());
//            }
//            if (!ui.labelCss().isEmpty()) {
//                fieldConfig.setLabelCss(ui.labelCss());
//            }
//            if (!ui.tableCss().isEmpty()) {
//                fieldConfig.setTableCss(ui.tableCss());
//            }
//
//            if (!ui.hint().isEmpty()) {
//                fieldConfig.setHint(ui.hint());
//            }
//
//            if (!ui.suffix().isEmpty()) {
//                fieldConfig.setSuffix(ui.suffix());
//            }
//
//            if (!ui.suffix().isEmpty()) {
//                fieldConfig.setSuffix(ui.suffix());
//            }
//
//            if (!ui.updateFieldUrl().isEmpty()) {
//                fieldConfig.setUpdateFieldUrl(ui.updateFieldUrl());
//            }
//
//            if (!ui.updateFieldUrl().isEmpty()) {
//                fieldConfig.setUpdateFieldUrl(ui.updateFieldUrl());
//            }
//
//            if (!ui.reorderUrl().isEmpty()) {
//                fieldConfig.setReorderUrl(ui.reorderUrl());
//            }
//
//            fieldConfig.setExpandedRowColor(org.apache.commons.lang3.StringUtils.stripToNull(ui.expandedColor()));
//
//            if (ui.type().equals(FieldType.number) || ui.type().equals(FieldType.money)) {
//                fieldConfig.setAlign(UiField.RIGHT_ALIGN);
//            } else if (ui.type().equals(FieldType.checkbox)) {
//                fieldConfig.setAlign(UiField.CENTER_ALIGN);
//            } else if (!UiField.DEFAULT_ALIGN.equals(ui.align())) {
//                fieldConfig.setAlign(ui.align());
//            }
//
//            if (field.getName().toLowerCase().endsWith("id")) {
//                fieldConfig.setAlign(UiField.LEFT_ALIGN);
//            }
//
//            if (!ui.idFieldName().isEmpty()) {
//                fieldConfig.setIdFieldName(ui.idFieldName());
//            }
//
//            if (ui.fixedCurrency()) {
//                fieldConfig.setFixedCurrency(ui.fixedCurrency());
//            }
//
//            if (ui.localeFormatted()) {
//                fieldConfig.setLocaleFormatted(ui.localeFormatted());
//            }
//
//            if (ui.lookupOverwritesForm()) {
//                fieldConfig.setLookupOverwritesForm(ui.lookupOverwritesForm());
//            }
//
//            fieldConfig.setCodeMode(Strings.emptyToNull(ui.codeMode()));
//            fieldConfig.setTheme(Strings.emptyToNull(ui.theme()));
//
//            if (FieldType.lookup.equals(ui.type()) || FieldType.option_list.equals(ui.type())) {
//                FormRowConfigDO rowConfig = getRowConfig(field.getType());
//                fieldConfig.setInnerFields(rowConfig.getColumns().get(0).getFields());
//            }
//            if (FieldType.nested_table.equals(ui.type()) || FieldType.tree.equals(ui.type())) {
//
//                ParameterizedType genericType = (ParameterizedType) field.getGenericType();
//                Class<?> clazz = (Class<?>) genericType.getActualTypeArguments()[0];
//                FormRowConfigDO rowConfig = getRowConfig(clazz);
//
//                //todo delete for elastic
//                fieldConfig.setInnerFields(rowConfig.getColumns().get(0).getFields());
//                fieldConfig.setCanReorder(ui.canReorder());
//                fieldConfig.setCanDelete(ui.canDelete());
//                fieldConfig.setReorderWeightField(Strings.emptyToNull(ui.reorderWeightField()));
//                fieldConfig.setTreeOptions(generateTreeOptions(ui.treeOptions()));
//            }
//
//            if (FieldType.metric.equals(ui.type())) {
//                fieldConfig.setMetricOptions(new MetricOptionsDTO(ui.metricOptions().name()));
//            }
//
//            if (ui.type().equals(FieldType.sub_panel)
//                    || ui.type().equals(FieldType.diamond_panel)
//                    || ui.type().equals(FieldType.card_panel)) {
//                FormRowConfigDO rowConfig = getRowConfig(field.getType());
//                fieldConfig.setInnerFields(rowConfig.getColumns().get(0).getFields());
//            }
//
//            if (!ui.standalone().indexClass().equals(Object.class)) {
//                fieldConfig.setStandaloneTableConfig(menuItem()
//                        .label(ui.standalone().title())
//                        .elastic(new ElasticQuery.Builder(ui.standalone().indexClass())
//                                .index(ui.standalone().index())
//                                .convertTo(ui.standalone().convertTo().equals(Object.class) ? null : ui.standalone().convertTo())
//                                .title(ui.standalone().title())
//                                .initialConditions(constructInitialConditionsForElasticTab(Arrays.asList(ui.standalone().conditions())))
//                                .withTimestamp(ui.standalone().withTimestamp())
//                                .build()
//                        ).build()
//                );
//            }
//
//            fieldConfig.setIgnoreIfNotPresent(ui.ignoreIfNotPresent());
//
//            if (StringUtils.isNotBlank(ui.ref().entity())) {
//                fieldConfig.setRef(UiRefDTO.builder()
//                        .entity(org.apache.commons.lang3.StringUtils.stripToNull(ui.ref().id()))
//                        .code(org.apache.commons.lang3.StringUtils.stripToNull(ui.ref().entity()))
//                        .nestedId(org.apache.commons.lang3.StringUtils.stripToNull(ui.ref().nestedId()))
//                        .tab(org.apache.commons.lang3.StringUtils.stripToNull(ui.ref().tab()))
//                        .build());
//            }
//            if (ui.sortable()) {
//                fieldConfig.setSortable(ui.sortable());
//            }
//            if (!ui.sortableField().isEmpty()) {
//                fieldConfig.setSortableField(StringUtils.isNotBlank(ui.sortableField()) ? ui.sortableField() : field.getName());
//            }
//            if (!ui.jsValue().isEmpty()) {
//                fieldConfig.setJsValue(ui.jsValue());
//            }
//            if (!ui.jsValue().isEmpty()) {
//                fieldConfig.setJsRecalculate(ui.jsRecalculate());
//            }
//
//            fieldConfig.setExternalLink(Strings.emptyToNull(ui.externalLinkJs()));
//            fieldConfig.setJustFilter(ui.onlyFilter());
//
//            fieldConfig.setAggs(Utils.safeStream(ui.aggregators())
//                    .map(AggFieldType::valueOf)
//                    .map(AggFieldConfigDO::new)
//                    .collect(Collectors.toList()));
//
//            fieldConfig.setLabel(parseCodeToLabel(fieldConfig));
//            LinkedList<FormValidator> formValidators = getValidators(field);
//            fieldConfig.setValidators(formValidators);
//
//            Optional<DocField> docFieldOptional = Optional.ofNullable(field.getAnnotation(DocField.class));
//            if (docFieldOptional.isPresent()) {
//                fieldConfig.setHelp(docFieldOptional.get().description());
//            }
//
//            List<ActionConfigDO> actionsDTOs = createActionsDTO(ui.actions());
//            fieldConfig.setActions(actionsDTOs);
//
//            if (FieldType.duration.equals(ui.type())) {
//                fieldConfig.setDurationOptions(DurationConfig.of(ui.durationOptions()));
//            }
//
//            if (ui.type().equals("otc")) {
//                FormRowConfigDO rowConfig = getRowConfig(field.getType());
//                fieldConfig.setInnerFields(rowConfig.getColumns().get(0).getFields());
//            }
//
//            fieldConfig.setProperties(ui.properties());
//            fieldConfig.setPanelOptionsUrl(ui.panelOptionsUrl());
//            fieldConfig.setLookupWatcherFieldName(ui.lookupWatcherFieldName());
//            fieldConfig.setLookupWatcherFieldObjectId(ui.lookupWatcherFieldObjectId());
//            fieldConfig.setLookupCollection(ui.lookupCollection());
//            fieldConfig.setOnBefore(ui.onBefore());
//            fieldConfig.setOnAfter(ui.onAfter());
//            fieldConfig.setClipboard(ui.clipboard());
//            fieldConfig.setPopupUrl(ui.popupUrl());
//
//            fieldConfig.setSearchable(ui.searchable());
//            fieldConfig.setSearchableSelected(ui.searchableSelected());
//            fieldConfig.setSearchbarOpen(ui.searchbarOpen());
//
//            Optional<UiFilter> filterDefOpt = Optional.ofNullable(field.getAnnotation(UiFilter.class));
//            if (filterDefOpt.isPresent()) {
//                fieldConfig.setFilterable(true);
//                fieldConfig.setHasTime(filterDefOpt.get().hasTime());
//                fieldConfig.setHasFuture(filterDefOpt.get().hasFuture());
//                fieldConfig.setHasPast(filterDefOpt.get().hasPast());
//                fieldConfig.setDistinctUrl(filterDefOpt.get().distinctUrl());
//            }
//
//            if (ui.fxFlex() > 0) {
//                fieldConfig.setFxFlex(ui.fxFlex());
//            }
//
//            return fieldConfig;
//        } catch (Throwable t) {
//            logger.error("Error: Field : {}  Ui Type: {} ", field.getName(), ui.type(), t);
//            return null;
//        }
//
//    }
//
//
//    public static LinkedList<FormValidator> getValidators(Field field) {
//        LinkedList<FormValidator> formValidators = new LinkedList<>();
//
//        if (field.isAnnotationPresent(NotNull.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.NOT_NULL));
//        }
//
//        if (field.isAnnotationPresent(GUSLNotEmpty.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.NOT_NULL));
//        }
//
//        if (field.isAnnotationPresent(GUSLMinLength.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.GUSL_MIN_LENGTH, field.getAnnotation(GUSLMinLength.class).min()));
//        }
//
//        if (field.isAnnotationPresent(GUSLMaxLength.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.GUSL_MAX_LENGTH, field.getAnnotation(GUSLMaxLength.class).max()));
//        }
//
//        if (field.isAnnotationPresent(Min.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.MIN, field.getAnnotation(Min.class).value()));
//        }
//
//        if (field.isAnnotationPresent(NotEmpty.class) || field.isAnnotationPresent(NotBlank.class)) {
//            formValidators.add(new FormValidator(FormValidatorType.MIN_LENGTH, 1L));
//            formValidators.add(new FormValidator(FormValidatorType.NOT_NULL));
//        }
//        return formValidators;
//    }
//
//    private static String parseCodeToLabel(FieldConfigDO fieldConfigDO) {
//        String label = fieldConfigDO.getLabel();
//        if (org.apache.commons.lang3.StringUtils.isNotBlank(label)) {
//            return label;
//        }
//
//        String name = fieldConfigDO.getName();
//        if (name.length() <= 1) {
//            return name;
//        }
//
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(name.charAt(0));
//        for (int i = 0; i < name.length() - 1; i++) {
//            char c1 = name.charAt(i);
//            char c2 = name.charAt(i + 1);
//
//            if (Character.isLowerCase(c1) && Character.isUpperCase(c2)) {
//                stringBuilder.append(" ");
//            }
//            stringBuilder.append(c2);
//        }
//
//        String s = stringBuilder.toString();
//        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
//    }
//
//    public static List<ActionConfigDO> createActionsDTO(RestActionConfig[] actions) {
//        return Arrays.stream(actions).map(FormUtils::createAdminActionConfigFromAnnotation).collect(Collectors.toList());
//    }
//
//
//    public static ActionConfigDO createAdminActionConfigFromAnnotation(RestActionConfig config) {
//
//        final ActionConfigDO.ActionConfigDOBuilder<?, ?> actionConfigBuilder = ActionConfigDO.builder()
//                .url(Strings.emptyToNull(config.url()))
//                .templateUrl(Strings.emptyToNull(config.templateUrl()))
//                .method(Strings.emptyToNull(config.method()))
//                .parentMenuLabel(Strings.emptyToNull(config.parentMenuLabel()))
//                .multipartForm(config.multipartForm())
//                .permission(Strings.emptyToNull(config.permission()))
//                .routerLink(Strings.emptyToNull(config.routerLink()))
//                .icon(Strings.emptyToNull(config.icon()))
//                .iconOnly(config.iconOnly())
//                .displayOrder(config.displayOrder())
//                .mediaType(config.mediaType())
//                .buttonCss(config.buttonCss())
//                .modalCode(Strings.emptyToNull(config.modalCode()));
//
//        List<RestActionConfigChild> children = Arrays.asList(config.children());
//
//        if (children.size() > 0) {
//            actionConfigBuilder.children(children.stream().map(FormUtils::createChildAdminActionConfigFromAnnotation).collect(Collectors.toList()));
//        } else {
//            updateFromBase(actionConfigBuilder, config.base());
//        }
//
//        return actionConfigBuilder.build();
//    }
//
//    public static ActionConfigDO createChildAdminActionConfigFromAnnotation(RestActionConfigChild config) {
//        final ActionConfigDO.ActionConfigDOBuilder<?, ?> actionConfigBuilder = ActionConfigDO.builder()
//                .method(Strings.emptyToNull(config.method()))
//                .modalCode(Strings.emptyToNull(config.modalCode()))
//                .multipartForm(config.multipartForm())
//                .parentMenuLabel(Strings.emptyToNull(config.parentMenuLabel()))
//                .permission(Strings.emptyToNull(config.permission()))
//                .routerLink(Strings.emptyToNull(config.routerLink()))
//                .templateUrl(Strings.emptyToNull(config.templateUrl()))
//                .icon(Strings.emptyToNull(config.icon()))
//                .iconOnly(config.iconOnly())
//                .displayOrder(config.displayOrder())
//                .mediaType(config.mediaType())
//                .buttonCss(config.buttonCss())
//                .url(Strings.emptyToNull(config.url()));
//
//        updateFromBase(actionConfigBuilder, config.base());
//        return actionConfigBuilder.build();
//
//    }
//
//    public static void updateFromBase(ActionConfigDO.ActionConfigDOBuilder<?, ?> configBuilder, final BaseActionConfig base) {
//        configBuilder
//                .actionType(base.actionType())
//                .buttonLabel(Strings.emptyToNull(base.buttonLabel()))
//                .buttonLabel(Strings.emptyToNull(base.buttonLabel()))
//                .cancelButton(Strings.emptyToNull(base.cancelButton()))
//                .cancelIcon(Strings.emptyToNull(base.cancelIcon()))
//                .cancelColorTheme(Strings.emptyToNull(base.cancelColorTheme()))
//                .confirmationFields(Strings.emptyToNull(base.confirmationFields()))
//                .confirmationMessage(Strings.emptyToNull(base.confirmationMessage()))
//                .confirmationUrl(Strings.emptyToNull(base.confirmationUrl()))
//                .displaySummary(base.displaySummary())
//                .downloadAction(base.downloadAction())
//                .dynamicResponse(base.dynamicResponse())
//                .icon(Strings.emptyToNull(base.icon()))
//                .iconOnly(base.iconOnly())
//                .displayOrder(base.displayOrder())
//                .inLine(base.inLine())
//                .jsEditCondition(Strings.emptyToNull(base.jsEditCondition()))
//                .jsWarningCondition(Strings.emptyToNull(base.jsWarningCondition()))
//                .minModalHeight(Strings.emptyToNull(base.minModalHeight()))
//                .minModalWidth(Strings.emptyToNull(base.minModalWidth()))
//                .modalCode(Strings.emptyToNull(base.modalCode()))
//                .popUpDescription(Strings.emptyToNull(base.popUpDescription()))
//                .popUpTitle(Strings.emptyToNull(base.popUpTitle()))
//                .saveButton(Strings.emptyToNull(base.saveButton()))
//                .saveIcon(Strings.emptyToNull(base.saveIcon()))
//                .saveColorTheme(Strings.emptyToNull(base.saveColorTheme()))
//                .selectMethod(Strings.emptyToNull(base.selectMethod()))
//                .selectUrl(Strings.emptyToNull(base.selectUrl()))
//                .showCancel(base.showCancel())
//                .successMessage(Strings.emptyToNull(base.successMessage()))
//                .templateUrl(Strings.emptyToNull(base.templateUrl()))
//                .warningMessage(Strings.emptyToNull(base.warningMessage()))
//                .tooltip(Strings.emptyToNull(base.tooltip()))
//                .route(base.route())
//                .command(base.command())
//                .fxFlex(base.fxFlex() > 0 ? base.fxFlex() : null)
//                .rows(isNull(base.form()) ? null : Collections.singletonList(FormRowConfigDO.builder().cols(1)
//                        .columns(getColsForSimpleFieldsDefClass(base.form()))
//                        .build()))
//                .confirmationFormRows(isNull(base.confirmationForm()) ? null : Collections.singletonList(FormRowConfigDO.builder().cols(1)
//                        .columns(getColsForSimpleFieldsDefClass(base.confirmationForm()))
//                        .build()));
//    }
//
//
//    public static List<FormColumnConfigDO> getColsForSimpleFieldsDefClass(Class clazz) {
//        FormColumnConfigDO currentColumn = new FormColumnConfigDO();
//        currentColumn.setActions(new ArrayList<>());
//        currentColumn.setFields(new ArrayList<>());
//
//        getFieldsFor(clazz).forEach(nestedField -> {
//            boolean nested = isNested(nestedField);
//            currentColumn.setNested(nested);
//            if (nested) {
//                currentColumn.getFields().addAll(getRowConfig(nestedField.getType()).getColumns().get(0).getFields());
//            } else {
//                createField(nestedField).ifPresent(config -> currentColumn.getFields().add(config));
//            }
//            currentColumn.setFxFlex(getFxFlex(nestedField));
//
//            getUpdateUrlEndpoint(nestedField).ifPresent(currentColumn::setUpdateUrl);
//            getDeleteUrlEndpoint(nestedField).ifPresent(currentColumn::setDeleteUrl);
//            getJsEditCondition(nestedField).ifPresent(currentColumn::setJsEditCondition);
//            getActions(nestedField).ifPresent(currentColumn::setActions);
//
//        });
//
//        return singletonList(currentColumn);
//    }
//
//
//    public static Optional<String> getUpdateUrlEndpoint(Field nestedField) {
//        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::updateUrl)
//                .filter(org.apache.commons.lang3.StringUtils::isNotBlank);
//    }
//
//    public static Optional<String> getDeleteUrlEndpoint(Field nestedField) {
//        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::deleteUrl)
//                .filter(org.apache.commons.lang3.StringUtils::isNotBlank);
//    }
//
//    public static Optional<String> getJsEditCondition(Field nestedField) {
//        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::jsEditCondition).filter(org.apache.commons.lang3.StringUtils::isNotBlank);
//    }
//
//    public static Integer getFxFlex(Field nestedField) {
//        Integer fxFlex = Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::fxFlex).orElse(1);
//        if (isNull(fxFlex) || fxFlex == 1 || fxFlex == -1) {
//            fxFlex = Optional.ofNullable(nestedField.getAnnotation(UiField.class)).map(UiField::fxFlex).orElse(1);
//        }
//        return fxFlex;
//    }
//
//    public static EndPointDO getUpdateUrlForMenu(UiSubMenu subMenu) {
//        return Optional.of(subMenu.editUrl())
//                .filter(s -> !org.apache.commons.lang3.StringUtils.isEmpty(s))
//                .map(s -> createEndpointDTO(subMenu.label(), EndPointType.PUT))
//                .orElse(null);
//    }
//
//    public static EndPointDO createEndpointDTO(String url, EndPointType method) {
//        EndPointDO endPointDO = new EndPointDO();
//        endPointDO.setMethod(method);
//        endPointDO.setUrl(url);
//        return endPointDO;
//    }
//
//    public static Optional<List<ActionConfigDO>> getActions(Field nestedField) {
//        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class))
//                .map(UiPosition::actions)
//                .filter(a -> a.length > 0)
//                .map(Arrays::stream)
//                .map(list -> list.map(FormUtils::createAdminActionConfigFromAnnotation).collect(Collectors.toList()));
//    }
//
//    public static boolean isNested(Field nestedField) {
//        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::nested)
//                .orElse(false);
//    }

}
