package gusl.elastic.model;

/**
 * Elastic Search Types
 *
 * @author grant
 */
public enum ESType {
    KEYWORD,
    DATE,
    GEO_POINT,
    DOUBLE,
    INTEGER,
    LONG,
    TEXT,
    FLOAT,
    // Create a text and raw keyword of the field
    MULTI,
    BOOLEAN,
    SCALED_FLOAT,
    OBJECT,
    NESTED,
    BINARY,
    IP
}
