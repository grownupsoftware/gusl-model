package gusl.elastic.model;

import gusl.core.security.ObfuscateMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author grant
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ElasticField {

    ESType type();

    ObfuscateMethod securityMethod() default ObfuscateMethod.NONE;

    boolean primaryKey() default false;

    boolean dateCreated() default false;

    boolean dateUpdated() default false;

    boolean populate() default false;

    boolean kibanaTimeField() default false;

    String name() default "";

    String defaultValue() default "";
}
