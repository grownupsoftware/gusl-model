package gusl.elastic.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author grant
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ElasticIndex {

    String index();

    int version();

    boolean rotate() default false;

    Class<?> type();

    String staticContent() default "";

    String nullValue() default "";
}
