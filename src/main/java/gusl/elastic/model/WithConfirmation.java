package gusl.elastic.model;

import java.time.ZonedDateTime;

public interface WithConfirmation {

    String getId();

    ZonedDateTime getDateCreated();

    ZonedDateTime getDateUpdated();

     void setDateUpdated(ZonedDateTime dateUpdated);

}
