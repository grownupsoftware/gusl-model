package gusl.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author dhudson
 */
public enum ModelErrors {

    KIBANA_QUERY_TYPE_NOT_SUPPORTED("GUSLMM04 Kibana query type {0} not supported", "kibana.query.type.not.supported"),
    STATIC_DATA_ERROR("GUSLMM15 static data loading issue, reason {0}", "static.data.error"),
    RESOURCE_NOT_FOUND("GUSLMM13 Resource {0} not found", "resource.not.found"),
    UNKNOWN_RESOURCE_TYPE("GUSLMM14 Unknown resource type {0}", "unknown.resource.type"),
    RANGE_ERROR("GUSLMM08 Can't parse {0}", "range.error");

    private final String message;
    private final String messageKey;

    ModelErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(Long id) {
        if (id != null) {
            return new ErrorDO(null, message, messageKey, String.valueOf(id));
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
