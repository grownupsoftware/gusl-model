package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class PieChartOptionDO {

    private int cutoutPercentage;

    private ChartLegendDO legend;

    public int getCutoutPercentage() {
        return cutoutPercentage;
    }

    public void setCutoutPercentage(int cutoutPercentage) {
        this.cutoutPercentage = cutoutPercentage;
    }

    public ChartLegendDO getLegend() {
        return legend;
    }

    public void setLegend(ChartLegendDO legend) {
        this.legend = legend;
    }

}
