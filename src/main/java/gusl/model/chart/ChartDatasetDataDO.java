package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartDatasetDataDO {

    private Long id;

    private Long datasetId;

    private Integer rank;

    private Double value;

}
