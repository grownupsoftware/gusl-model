package gusl.model.chart.map;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class MapDO {
    private Double lat;
    private Double lng;
    private Integer zoom;
    private Boolean zoomControl;
    private List<LocationMarkerDO> markers;
    private LocationCircleDO circle;

}
