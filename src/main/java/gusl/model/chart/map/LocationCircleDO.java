package gusl.model.chart.map;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class LocationCircleDO {
    private Double lat;
    private Double lng;
    private Integer radius;
    private String fillColor;
    private Boolean circleDraggable;
    private Boolean editable;

}
