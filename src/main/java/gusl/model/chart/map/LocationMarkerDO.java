package gusl.model.chart.map;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class LocationMarkerDO {
    private Double lat;
    private Double lng;
    private String label;
    private Boolean draggable;

}
