package gusl.model.chart.graphcollection;

import gusl.model.chart.ChartOptionsDO;
import gusl.core.tostring.ToString;

/**
 *
 * @author grantwallace
 */
public class ChartGO {

    private String key;

    private String title;

    private String type;

    private ChartOptionsDO options;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ChartOptionsDO getOptions() {
        return options;
    }

    public void setOptions(ChartOptionsDO options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
