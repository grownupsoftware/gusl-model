package gusl.model.chart;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class PieDataDO {

    private String[] labels;

    private List<ChartDatasetDO> datasets;

}
