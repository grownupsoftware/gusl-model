package gusl.model.chart;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartDataPointsDO<X, Y> {

    private String chartKey;
    private String key;

    private List<DataPointDO<X, Y>> data;

}
