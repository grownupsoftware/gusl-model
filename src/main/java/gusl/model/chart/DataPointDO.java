package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class DataPointDO<X, Y> {

    private X x;
    private Y y;

}
