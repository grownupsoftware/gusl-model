package gusl.model.chart;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class DatasetDO {

    private String label;

    private List<Long> data;


}
