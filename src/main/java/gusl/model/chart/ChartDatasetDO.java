package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString

public class ChartDatasetDO {

    private Long id;

    private Long chartId;

    private int rank;

    private String chartKey;
    private String key;
    private String label;
    private String[] backgroundColor;
    private String[] borderColor;
    private int[] borderWidth;
    private String[] hoverBackgroundColor;
    private String[] hoverBorderColor;
    private int[] hoverBorderWidth;

    private Integer pointRadius;

    private boolean fill;

    private Long[] data;
}


