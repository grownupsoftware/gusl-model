package gusl.model.chart;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartDataDO<T> {

    private String chartKey;
    private String key;

    private List<T> data;


}
