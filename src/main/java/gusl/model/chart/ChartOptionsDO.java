package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartOptionsDO {

    boolean animation;

    boolean scaleOverride;

    //** Required if scaleOverride is true **
    //Number - The number of steps in a hard coded scale
    int scaleSteps;

    //Number - The value jump in the hard coded scale
    int scaleStepWidth;

    //Number - The scale starting value
    int scaleStartValue;


}
