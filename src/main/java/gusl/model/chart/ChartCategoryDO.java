package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartCategoryDO {
    private Long id;
    private Long chartId;
    private String chartKey;
    private String categoryKey;
    private String label;
    private int rank;
}
