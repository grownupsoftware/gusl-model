package gusl.model.chart;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartLegendDO {

    private Boolean display;
    
    
//display: true
//fullWidth: true
//labels: {boxWidth: 40, padding: 10, generateLabels: ƒ}
//onClick: ƒ (e, legendItem)
//onHover: null
//position: "top"
//reverse: false
//weight: 1000


    public Boolean getDisplay() {
        return display;
    }

    public void setDisplay(Boolean display) {
        this.display = display;
    }

}
