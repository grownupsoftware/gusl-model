package gusl.model.chart;

import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ChartDO {

    private Long id;

    private String key;

    private String title;

    private String type;

    private List<String> labels;

    private List<ChartDatasetDO> datasets;

    private List<ChartCategoryDO> categories;

    // private ChartOptionsDO options;
    private Map<String, Object> options;

    private PieDataDO data;

    private Integer height;
    private Integer width;

    private String xAxisLabel;

    private String yAxisLabel;

    private PieChartOptionDO pieOptions;

}
