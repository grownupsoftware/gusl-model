package gusl.model.event;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.HashMap;

/**
 * @author dhudson
 * @since 16/06/2023
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CommandEvent extends AbstractEvent {
    private String id;
    private String command;
    private HashMap<String, Object> properties;

    public String toString() {
        return ToString.toString(this);
    }
}
