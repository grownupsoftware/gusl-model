package gusl.model.event;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class UniqueUUIDEvent {

    @ToStringMask
    private String uuid;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
