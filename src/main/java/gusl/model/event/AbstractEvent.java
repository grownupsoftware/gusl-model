package gusl.model.event;

/**
 * Base Event Class.
 *
 * Published Events can't be empty, as Jackson doesn't know how to handle them.
 *
 * @author dhudson
 */
public abstract class AbstractEvent {

    private long timestamp;

    public AbstractEvent() {
        timestamp = System.currentTimeMillis();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
