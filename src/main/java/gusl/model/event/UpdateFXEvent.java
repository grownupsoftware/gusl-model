package gusl.model.event;

import lombok.NoArgsConstructor;

/**
 * Fire this when we think that the FX rates needs updating.
 *
 * @author dhudson
 */
@NoArgsConstructor
public class UpdateFXEvent extends AbstractEvent {

}
