package gusl.model.event;

public class MetricEvent<T> {

    private String name;
    private Integer nodeId;
    private T value;

    public MetricEvent() {
    }

    public MetricEvent(String name, T size, Integer nodeId) {
        this.name = name;
        this.value = size;
        this.nodeId = nodeId;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public interface MetricEventName {
        String TEST_COUNTER_NAME = "TEST_COUNTER";
        String LOGGED_IN_PLAYERS_COUNTER_NAME = "LOGGED_IN_PLAYERS_COUNTER";
    }
}
