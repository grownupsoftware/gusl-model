package gusl.model.event;

import lombok.NoArgsConstructor;

/**
 * @author dhudson
 */
@NoArgsConstructor
public class PurgeCacheEvent extends AbstractEvent {

}
