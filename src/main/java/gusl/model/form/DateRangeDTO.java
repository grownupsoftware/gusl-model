package gusl.model.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.TemporalAmount;
import java.util.Date;
import java.util.LinkedHashMap;

@Builder
@Getter
@Setter
@ToString
public class DateRangeDTO {
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date from;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date to;

    public DateRangeDTO() {
    }

    public DateRangeDTO(Date selfExclusionPeriodUpdated, TemporalAmount duration) {
        this.from = selfExclusionPeriodUpdated;
        this.to = Date.from(selfExclusionPeriodUpdated.toInstant().plus(duration));
    }

    public DateRangeDTO(LinkedHashMap<Object, Object> map) throws ParseException {
        this.from = format.parse(map.get("from").toString());
        this.to = format.parse(map.get("to").toString());
    }

    public DateRangeDTO(Date from, Date to) {
        this.from = from;
        this.to = to;
    }

}
