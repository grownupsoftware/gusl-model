package gusl.model.form;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LookupRequestDO {

    private String searchValue;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
