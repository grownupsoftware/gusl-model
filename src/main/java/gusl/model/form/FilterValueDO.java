package gusl.model.form;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FilterValueDO {
    private String label;

    private String fieldName;
    private String value;
    private Integer count;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
