package gusl.model.form;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FilterValuesDO {

    @Singular
    private List<FilterValueDO> distinctValues;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
