package gusl.model.success;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SuccessWithNotificationResponseDO {

    private NotificationType type;
    private String message;
    private Integer autoCloseDuration;
    private Boolean noAutoClose;
    private NotificationPosition position;
    private Boolean noIcon;

    private boolean hideNotification;

    private boolean reload;

    public static SuccessWithNotificationResponseDO info(String message) {
        return info(message, false);
    }

    public static SuccessWithNotificationResponseDO info(String message, boolean hideNotification) {
        return SuccessWithNotificationResponseDO.builder()
                .type(NotificationType.INFO)
                .message(message)
                .autoCloseDuration(3000)
                .position(NotificationPosition.BOTTOM_RIGHT)
                .hideNotification(hideNotification)
                .build();
    }

    public static SuccessWithNotificationResponseDO success(String message) {
        return success(message, false, false);
    }

    public static SuccessWithNotificationResponseDO successWithReload(String message) {
        return success(message, false, true);
    }

    public static SuccessWithNotificationResponseDO successWithReload(String message, boolean hideNotification) {
        return success(message, hideNotification, true);
    }

    public static SuccessWithNotificationResponseDO success(String message, boolean hideNotification, boolean reload) {
        return SuccessWithNotificationResponseDO.builder()
                .type(NotificationType.SUCCESS)
                .message(message)
                .autoCloseDuration(3000)
                .position(NotificationPosition.BOTTOM_RIGHT)
                .reload(reload)
                .hideNotification(hideNotification)
                .build();
    }

    public static SuccessWithNotificationResponseDO warning(String message) {
        return warning(message, false);

    }

    public static SuccessWithNotificationResponseDO warning(String message, boolean hideNotification) {
        return SuccessWithNotificationResponseDO.builder()
                .type(NotificationType.WARNING)
                .message(message)
                .autoCloseDuration(3000)
                .position(NotificationPosition.BOTTOM_RIGHT)
                .hideNotification(hideNotification)
                .build();
    }

    public static SuccessWithNotificationResponseDO error(String message) {
        return error(message, false);
    }

    public static SuccessWithNotificationResponseDO error(String message, boolean hideNotification) {
        return SuccessWithNotificationResponseDO.builder()
                .type(NotificationType.ERROR)
                .message(message)
                .autoCloseDuration(3000)
                .position(NotificationPosition.BOTTOM_RIGHT)
                .hideNotification(hideNotification)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
