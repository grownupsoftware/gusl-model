/* Copyright lottomart */
package gusl.model.success;

import gusl.core.annotations.DocClass;

/**
 *
 * @author grant
 */
@DocClass(description = "Player client does not like an empty json response")
public class SuccessResponseDTO {

    private boolean success;

    public SuccessResponseDTO() {
        this.success = true;
    }

    public SuccessResponseDTO(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
