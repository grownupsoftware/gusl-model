package gusl.model.success;

public enum NotificationType {
    INFO,
    SUCCESS,
    WARNING,
    ERROR;

}
