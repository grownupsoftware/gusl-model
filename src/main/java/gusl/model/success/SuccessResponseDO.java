package gusl.model.success;

import gusl.core.tostring.ToString;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class SuccessResponseDO {

    private boolean success;

    public SuccessResponseDO() {
        this.success = true;
    }

    public SuccessResponseDO(boolean success) {
        this.success = success;
    }

    public static SuccessResponseDO success() {
        return new SuccessResponseDO(true);
    }

    public static SuccessResponseDO failed() {
        return new SuccessResponseDO(false);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
