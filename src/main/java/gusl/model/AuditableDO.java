package gusl.model;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 * @author dhudson on 01/08/2020
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
public class AuditableDO implements Auditable {

    private Date dateCreated;
    private Date dateUpdated;

    @Override
    public void touch() {
        setDateUpdated(new Date());
    }

    @Override
    public void created() {
        created(new Date());
    }

    @Override
    public void created(Date now) {
        setDateCreated(now);
        setDateUpdated(now);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
