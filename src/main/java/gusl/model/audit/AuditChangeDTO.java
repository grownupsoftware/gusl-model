/* Copyright lottomart */
package gusl.model.audit;

/**
 *
 * @author grant
 */
public class AuditChangeDTO {

    private String fieldName;

    private Object originalValue;

    private Object newValue;

    public AuditChangeDTO() {
    }

    public AuditChangeDTO(String fieldName, Object originalValue) {
        this.fieldName = fieldName;
        this.originalValue = originalValue;
    }

    public AuditChangeDTO(String fieldName, Object originalValue, Object newValue) {
        this.fieldName = fieldName;
        this.originalValue = originalValue;
        this.newValue = newValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getOriginalValue() {
        return originalValue == null ? "" : originalValue.toString();
    }

    public void setOriginalValue(Object originalValue) {
        this.originalValue = originalValue;
    }

    public Object getNewValue() {
        return newValue == null ? "" : newValue.toString();
    }

    public void setNewValue(Object newValue) {
        this.newValue = newValue;
    }

    @Override
    public String toString() {
        return "AuditChangeDO{" + "fieldName=" + fieldName + ", originalValue=" + originalValue + ", newValue=" + newValue + '}';
    }

}
