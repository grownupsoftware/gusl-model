package gusl.model.audit;

import java.util.Date;
import java.util.List;
import java.util.Map;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DocClass(description = "Data object for audit")
public class AuditDO {

    @DocField(description = "The date of the action")
    private Date auditDate;

    @DocField(description = "Unique Id")
    private Long id;

    @DocField(description = "The date and time of the action")
    private Date auditTime;

    @DocField(description = "The user whom performed the action")
    private String username;

    @DocField(description = "The audit action performed")
    private String action;

    @DocField(description = "The name of the table")
    private String tableName;

    //private AuditChangesDO changes;
    @DocField(description = "Record before changes")
    private Map<String, Object> before;

    @DocField(description = "Record after changes")
    private Map<String, Object> after;

    @DocField(description = "Diff after update")
    private List<AuditSingleDiffDO> diff;

    @DocField(description = "Diff after update")
    private List<String> referenceIds;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
