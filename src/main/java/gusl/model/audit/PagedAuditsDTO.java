/* Copyright lottomart */
package gusl.model.audit;

import java.util.List;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import gusl.model.dataservice.dtos.paged.AbstractPagedDTO;

/**
 *
 * @author grant
 */
public class PagedAuditsDTO extends AbstractPagedDTO {

    @ToStringCount
    private List<AuditDO> content;

    private Integer total;

    public List<AuditDO> getContent() {
        return content;
    }

    public void setContent(List<AuditDO> content) {
        this.content = content;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
