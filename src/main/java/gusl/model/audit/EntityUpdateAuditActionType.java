package gusl.model.audit;

public enum EntityUpdateAuditActionType {

    SUSPEND_ALL, OPEN_ALL;
}
