package gusl.model.audit;

import gusl.model.bigquery.BQField;
import gusl.model.bigquery.BQType;

public class AuditSingleDiffDO {

    @BQField(type = BQType.STRING)
    private String fieldName;

    private String diff;


    public AuditSingleDiffDO() {
    }

    public AuditSingleDiffDO(String fieldName, String diff) {
        this.fieldName = fieldName;
        this.diff = diff;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }
}
