package gusl.model.audit;

import java.util.Date;
import java.util.Map;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;

/**
 *
 * @author grant
 */
public class AuditDTO {

    @DocField(description = "The date of the action")
    private Date auditDate;

    @DocField(description = "Unique Id")
    private Long id;

    @DocField(description = "The date and time of the action")
    private Date auditTime;

    @DocField(description = "The user whom performed the action")
    private String username;

    @DocField(description = "The audit action performed")
    private String action;

    @DocField(description = "The name of the table")
    private String tableName;

//    @DocField(description = "Table changes")
//    private AuditChangesDTO changes;
    @DocField(description = "Record before changes")
    private Map<String, Object> before;

    @DocField(description = "Record after changes")
    private Map<String, Object> after;

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Map<String, Object> getBefore() {
        return before;
    }

    public void setBefore(Map<String, Object> before) {
        this.before = before;
    }

    public Map<String, Object> getAfter() {
        return after;
    }

    public void setAfter(Map<String, Object> after) {
        this.after = after;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
