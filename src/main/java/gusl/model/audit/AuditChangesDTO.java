/* Copyright lottomart */
package gusl.model.audit;

import java.util.ArrayList;
import java.util.List;
import gusl.core.tostring.ToString;

/**
 *
 * @author grant
 */
public class AuditChangesDTO {

    private List<AuditChangeDTO> changes = new ArrayList<>();

    public AuditChangesDTO() {
    }

    public AuditChangesDTO(List<AuditChangeDTO> changes) {
        this.changes = changes;
    }

    public List<AuditChangeDTO> getChanges() {
        return changes;
    }

    public void setChanges(List<AuditChangeDTO> changes) {
        this.changes = changes;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
