package gusl.model.audit;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;


public class EntityUpdateAuditDO {

    @DocField(description = "The Id of the audit entry")
    private Long id;

    @DocField(description = "The Id of the session which triggered the action")
    private Long sessionId;

    @DocField(description = "The Id of the operator who triggered the action")
    private Long accountId;

    @DocField(description = "The username of the operator who triggered the action")
    private String username;

    @DocField(description = "The date when action has been processed (used in partition key to limit partitions size)")
    @NotNull
    private Date date;

    @DocField(description = "The time when action has been processed")
    @NotNull
    private Date time;

    @DocField(description = "The action that performed the updates")
    @NotNull
    private EntityUpdateAuditActionType action;

    @DocField(description = "The Ids of the modified events")
    private Set<Long> eventIds;

    @DocField(description = "The Ids of the modified markets")
    private Set<Long> marketIds;

    @DocField(description = "The Ids of the modified runners")
    private Set<Long> runnerIds;

    public EntityUpdateAuditDO() {
    }

    public EntityUpdateAuditDO(Long id) {
        this.setId(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public EntityUpdateAuditActionType getAction() {
        return action;
    }

    public void setAction(EntityUpdateAuditActionType action) {
        this.action = action;
    }

    public Set<Long> getEventIds() {
        return eventIds;
    }

    public void setEventIds(Set<Long> eventIds) {
        this.eventIds = eventIds;
    }

    public Set<Long> getMarketIds() {
        return marketIds;
    }

    public void setMarketIds(Set<Long> marketIds) {
        this.marketIds = marketIds;
    }

    public Set<Long> getRunnerIds() {
        return runnerIds;
    }

    public void setRunnerIds(Set<Long> runnerIds) {
        this.runnerIds = runnerIds;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hashCode(id);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof EntityUpdateAuditDO) {
            EntityUpdateAuditDO other = (EntityUpdateAuditDO) obj;
            return Objects.equals(id, other.id);
        }
        return false;
    }

}
