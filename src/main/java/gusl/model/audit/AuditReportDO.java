package gusl.model.audit;

import java.util.Date;
import gusl.core.tostring.ToString;

/**
 *
 * @author grant
 */
public class AuditReportDO {

    private Date auditTime;
    private String username;
    private String action;
    private String info;

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
