package gusl.model.audit;

import gusl.core.annotations.DocField;

import java.util.Date;

public class AuditSimpleDO {

    @DocField(description = "The date of the action")
    private Date auditDate;

    @DocField(description = "Unique Id")
    private Long id;

    @DocField(description = "The user whom performed the action")
    private String username;

    @DocField(description = "The audit action performed")
    private String action;

    @DocField(description = "The name of the table")
    private String tableName;

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
