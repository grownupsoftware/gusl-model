package gusl.model.audit;

import gusl.core.annotations.DocField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 */
@NoArgsConstructor
@Getter
@Setter
public class AuditableRequest {

    @DocField(description = "The user whom performed the action")
    private String username;

    @DocField(description = "The audit action performed")
    private String action;

    @Override
    public String toString() {
        return "AuditableRequest{" + "username=" + username + ", action=" + action + '}';
    }

}
