package gusl.model.audit;

import java.util.List;

/**
 *
 * @author grant
 */
public class AuditResponseDO {

    private int offset;
    private int perPage;
    private int total;

    private List<AuditReportDO> records;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<AuditReportDO> getRecords() {
        return records;
    }

    public void setRecords(List<AuditReportDO> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "AuditResponseDO{" + "offset=" + offset + ", perPage=" + perPage + ", total=" + total + ", records=" + records + '}';
    }

}
