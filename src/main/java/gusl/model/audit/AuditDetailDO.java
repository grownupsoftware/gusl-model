/* Copyright lottomart */
package gusl.model.audit;

import java.util.Map;

/**
 *
 * @author grant
 */
public class AuditDetailDO {

    private Map<String, Object> changes;

    public AuditDetailDO() {
    }

    public AuditDetailDO(Map<String, Object> changes) {
        this.changes = changes;
    }

    public Map<String, Object> getChanges() {
        return changes;
    }

    public void setChanges(Map<String, Object> changes) {
        this.changes = changes;
    }

    @Override
    public String toString() {
        return "AuditDetailDO{" + "changes=" + changes + '}';
    }

}
