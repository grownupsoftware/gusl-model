package gusl.model.audit;

/**
 * @author grant
 */
public enum AuditOperationType {

    ALL,
    ADMIN,
    PLAYER;

}
