/* Copyright lottomart */
package gusl.model.audit;

import java.util.ArrayList;
import java.util.List;
import gusl.core.tostring.ToString;

/**
 *
 * @author grant
 */
public class AuditChangesDO {

    private List<AuditChangeDO> changes = new ArrayList<>();

    public AuditChangesDO() {
    }

    public AuditChangesDO(List<AuditChangeDO> changes) {
        this.changes = changes;
    }

    public List<AuditChangeDO> getChanges() {
        return changes;
    }

    public void setChanges(List<AuditChangeDO> changes) {
        this.changes = changes;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
