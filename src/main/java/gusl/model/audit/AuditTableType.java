package gusl.model.audit;

/**
 * @author grant
 */
public enum AuditTableType {

    ALL,
    LOGIN,
    LOADING,
    SETTLEMENT,
    BET_REPORTS,
    BET_MANAGER;

}
