package gusl.model;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Set;

import static gusl.core.utils.Utils.safeStream;
import static java.util.stream.Collectors.toSet;

public abstract class StateTransitionValidator<T> {

    protected final Multimap<T, T> transitionsMap = HashMultimap.create();

    public StateTransitionValidator() {
        populateTransitionsMap();
    }

    protected abstract void populateTransitionsMap();

    public boolean isValidTransition(T initialStatus, T requestedStatus) {
        return transitionsMap.containsEntry(initialStatus, requestedStatus);
    }

    public Set<T> getValidStatesForCommand(T command) {
        return safeStream(transitionsMap.entries())
                .filter(entry -> entry.getValue().equals(command))
                .map(entry -> entry.getKey())
                .collect(toSet());
    }
}
