/* Copyright lottomart */
package gusl.model;

/**
 * @param <ID> Unique ID type
 * @author grant
 */
public interface Identifiable<ID> {

    public ID getId();

    public void setId(ID id);

}
