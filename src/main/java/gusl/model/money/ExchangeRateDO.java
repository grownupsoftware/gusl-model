/* Copyright lottomart */
package gusl.model.money;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.model.currency.CurrencyDO;
import lombok.*;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ExchangeRateDO {

    @DocField(description = "The currency")
    private CurrencyDO currency;

    @DocField(description = "Rate to GBP")
    private BigDecimal rate;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
