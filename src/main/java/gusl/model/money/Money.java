package gusl.model.money;

import gusl.model.currency.Currency;
import lombok.CustomLog;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

import static java.math.BigDecimal.ZERO;

/**
 * Represents an amount in a currency.
 *
 * <p>
 * {@code Money} objects are immutable, so any change will result in a new Money
 * object.
 * </p>
 *
 * <p>
 * This class takes either an {@code int} or a {@link BigDecimal} for its
 * multiplication and division methods. It doesn't take {@code float} or
 * {@code double} for those methods, since those types don't interact well with
 * {@code BigDecimal}. Instead, the {@code BigDecimal} class must be used when
 * the factor or divisor is a non-integer.
 * </p>
 *
 * <p>
 * This version of Money is using BigDecimals, future releases should implement
 * a primitive version of this.</p>
 * <p>
 *
 * <p>
 * Money has a default scale of two.</p>
 *
 * <p>
 * FX calculations are done at the DEFAULT_SCALE of {@code FX_DEFAULT_SCALE} </p>
 *
 * <p>
 * Also happens to be a fantastic song by a popular beat combo called Pink
 * Floyd</p>
 *
 * @author dhudson
 */
@CustomLog
public class Money {

    public static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_EVEN;
    public static final int FX_DEFAULT_SCALE = 4;

    /**
     * Throw when performing operations that require the currency to be the same
     */
    public static final class MismatchedCurrencyException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        MismatchedCurrencyException(String message) {
            super(message);
        }
    }

    private final Currency theCurrency;
    private final BigDecimal theAmount;

    /**
     * Create a new Money Object, with a value of zero.
     * <p>
     * Scale is taken from the currency.
     *
     * @param currency
     */
    public Money(Currency currency) {
        this(currency, 0L);
    }

    /**
     * Create a new Money Object.
     * <p>
     * Scale is taken from the currency.
     *
     * @param currency
     * @param amount
     */
    public Money(Currency currency, String amount) {
        theCurrency = currency;
        theAmount = new BigDecimal(amount).setScale(currency.getDecimalPlaces(), DEFAULT_ROUNDING_MODE);
    }

    /**
     * Create a new Money Object.
     * <p>
     * This assumes that the scale is taken from the currency.
     *
     * @param currency
     * @param amount
     */
    public Money(Currency currency, Double amount) {
        this(currency, Double.toString(amount));
    }

    /**
     * Create a new money object.
     * <p>
     * This assumes that the amount is in pence (un scaled).
     *
     * @param currency
     * @param amount   in pence
     */
    public Money(Currency currency, int amount) {
        this(currency, (long) amount);
    }

    /**
     * Create a new money object.
     * <p>
     * This assumes that the amount is in pence (un scaled).
     *
     * @param currency
     * @param amount   in pence
     */
    public Money(Currency currency, long amount) {
        theCurrency = currency;
        if (amount == 0) {
            theAmount = new BigDecimal(0).setScale(currency.getDecimalPlaces(), DEFAULT_ROUNDING_MODE);
        } else {
            // BigInt constructor is unscaled amount
            theAmount = new BigDecimal(BigInteger.valueOf(amount), currency.getDecimalPlaces());
        }
    }

    /**
     * Create a new Money Object.
     * <p>
     * Will set the scale to the currency number of decimal places and set the
     * default rounding mode
     *
     * @param currency
     * @param amount
     */
    public Money(Currency currency, BigDecimal amount) {
        theCurrency = currency;
        theAmount = amount.setScale(currency.getDecimalPlaces(), DEFAULT_ROUNDING_MODE);
    }

    /**
     * Return the currency for this money.
     *
     * @return the immutable currency
     */
    public Currency getCurrency() {
        return theCurrency;
    }

    /**
     * Check to see if the currencies are the same.
     *
     * <p>
     * Currencies can only be the same if the exchange rate is the same</p>
     *
     * @param money to check
     * @return true if the currencies are the same
     */
    public boolean isSameCurrency(Money money) {
        return isSameCurrency(money.getCurrency());
    }

    /**
     * Check to see if the currencies are the same.
     *
     * <p>
     * Currencies can only be the same if the exchange rate is the same</p>
     *
     * @param currency to check
     * @return true if the currencies are the same
     */
    public boolean isSameCurrency(Currency currency) {
        return (theCurrency.getExchangeRate().compareTo(currency.getExchangeRate()) == 0);
    }

    /**
     * Return the amount formatted in the locale given.
     *
     * @param locale
     * @return formatted money amount
     */
    public String getLocaleFormatted(Locale locale) {
        return NumberFormat.getCurrencyInstance(locale).format(theAmount);
    }

    /**
     * Return the {@code BigDecimal}
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return theAmount;
    }

    /**
     * The amount scale.
     * <p>
     * Will be what the currency is set too.
     *
     * @return the scale
     */
    public int getScale() {
        return theAmount.scale();
    }

    /**
     * Check to see if the amount is positive.
     *
     * @return true if the amount is positive
     */
    public boolean isPositive() {
        return theAmount.compareTo(ZERO) > 0;
    }

    /**
     * Check to see if the amount if negative.
     *
     * @return true if the amount if negative
     */
    public boolean isNegative() {
        return theAmount.compareTo(ZERO) < 0;
    }

    /**
     * Check to see if the amount is zero
     *
     * @return true if the amount is zero
     */
    public boolean isZero() {
        return theAmount.compareTo(ZERO) == 0;
    }

    /**
     * Return a new Money amount of zero value in this currency
     *
     * @return a new money amount
     */
    public Money zero() {
        return new Money(theCurrency, 0);
    }

    /**
     * Return a new Money with the existing amount multiplied by the factor.
     *
     * @param factor to multiply by
     * @return a new Money as the result
     */
    public Money times(int factor) {
        BigDecimal tmp = new BigDecimal(factor);
        // The amount scale used, which is 2
        return new Money(theCurrency, theAmount.multiply(tmp));
    }

    /**
     * Return a new Money with the existing amount multiplied by the factor.
     *
     * @param factor to multiply by
     * @return a new Money as the result
     */
    public Money times(double factor) {
        BigDecimal newAmount = theAmount.multiply(AmountUtils.asBigDecimal(factor));
        return new Money(theCurrency, newAmount);
    }

    /**
     * Return a new Money with existing amount divided by the divisor.
     *
     * @param aDivisor to divide by
     * @return a new Money object which has been divided
     */
    public Money div(int aDivisor) {
        BigDecimal divisor = new BigDecimal(aDivisor);
        BigDecimal newAmount = theAmount.divide(divisor, DEFAULT_ROUNDING_MODE);
        return new Money(theCurrency, newAmount);
    }

    /**
     * Return a new Money with existing amount divided by the divisor.
     *
     * @param aDivisor to divide by
     * @return a new Money object which has been divided
     */
    public Money div(double aDivisor) {
        BigDecimal newAmount = theAmount.divide(AmountUtils.asBigDecimal(aDivisor), DEFAULT_ROUNDING_MODE);
        return new Money(theCurrency, newAmount);
    }

    /**
     * Returns the absolute value.
     *
     * @return a new Money absolute value
     */
    public Money abs() {
        if (isPositive()) {
            return new Money(theCurrency, theAmount);
        }

        return times(-1);
    }

    /**
     * Returns a new Money negated.
     *
     * @return amount * (-1)
     */
    public Money negate() {
        return times(-1);
    }

    /**
     * Add the given money to this money, doing any FX on the way.
     *
     * @param money to add
     * @return a new Money object
     */
    public Money addMoney(Money money) {
        if (money.isZero()) {
            return this;
        }
        // Convert to this currency
        BigDecimal amount = fx(money.getAmount(), money.getExchangeRate(), getExchangeRate());
        return new Money(theCurrency, theAmount.add(amount));
    }

    /**
     * Subtract the given from from this money, doing any FX on the way.
     *
     * @param money to subtract
     * @return a new Money object
     */
    public Money subMoney(Money money) {
        if (money.isZero()) {
            return this;
        }
        BigDecimal amount = fx(money.getAmount(), money.getExchangeRate(), getExchangeRate());
        return new Money(theCurrency, theAmount.subtract(amount));
    }

    /**
     * Check to see if the given money is greater than this.
     *
     * @param money to check
     * @return true the this is greater than the given amount
     * @throws MismatchedCurrencyException if currencies do not match
     */
    public boolean isGreaterThan(Money money) {
        checkCurrencies(money);
        return compareAmount(money) > 0;
    }

    /**
     * Check to see if the given money is greater or equal to this.
     *
     * @param money to check
     * @return true the this is greater than the given amount
     * @throws MismatchedCurrencyException if currencies do not match
     */
    public boolean isGreaterThanOrEqual(Money money) {
        checkCurrencies(money);
        return compareAmount(money) >= 0;
    }

    /**
     * Check to see if the given money is less than this.
     *
     * @param money to check
     * @return true the this is greater than the given amount
     * @throws MismatchedCurrencyException if currencies do not match
     */
    public boolean isLessThan(Money money) {
        checkCurrencies(money);
        return compareAmount(money) < 0;
    }

    /**
     * Check to see if the given money is less than or equal to this.
     *
     * @param money to check
     * @return true the this is greater than the given amount
     * @throws MismatchedCurrencyException if currencies do not match
     */
    public boolean isLessThanOrEqual(Money money) {
        checkCurrencies(money);
        return compareAmount(money) <= 0;
    }

    /**
     * Check to see if the given money equal to this.
     *
     * @param money to check
     * @return true the this is greater than the given amount
     * @throws MismatchedCurrencyException if currencies do not match
     */
    public boolean isEqualTo(Money money) {
        checkCurrencies(money);
        return compareAmount(money) == 0;
    }

    private void checkCurrencies(Money money) {
        if (!isSameCurrency(money)) {
            throw new MismatchedCurrencyException(getCurrencyCode()
                    + ": Exchange rate " + theCurrency.getExchangeRate()
                    + " does not match " + money.getCurrencyCode() + ": Exchange rate"
                    + money.getCurrency().getExchangeRate());
        }
    }

    /**
     * Compare amount.
     * <p>
     * This ignores scale, so that 0 is the same as 0.00
     *
     * @param money to compare
     * @return -1, 0, or 1 as this {@code BigDecimal} is numerically less than,
     * equal to, or greater than {@code money}
     */
    private int compareAmount(Money money) {
        return theAmount.compareTo(money.getAmount());
    }

    /**
     * Convert the amount in the given currency to this currency.
     *
     * @param money to convert
     * @return new converted money object
     */
    public Money convert(Money money) {
        BigDecimal result = fx(money.getAmount(), money.getExchangeRate(), getExchangeRate());
        return new Money(theCurrency, result);
    }

    /**
     * Convert the amount in this currency to the given currency.
     *
     * @param currency to convert to
     * @return a new Money with the currency of the given currency and the
     * converted amount
     */
    public Money convert(Currency currency) {
        BigDecimal result = fx(theAmount, theCurrency.getExchangeRate(), currency.getExchangeRate());
        return new Money(currency, result);
    }

    /**
     * Perform any FX that is required.
     *
     * @return the amount from the first currency to the second currency
     */
    private BigDecimal fx(BigDecimal amount, BigDecimal fromExchangeRate, BigDecimal toExchangeRate) {

        BigDecimal result;

        if (fromExchangeRate.compareTo(toExchangeRate) == 0) {
            return amount;
        } else if ((!AmountUtils.isBaseCurrency(toExchangeRate)) && (!AmountUtils.isBaseCurrency(fromExchangeRate))) {
            // Calculate the rate as neither rates and base currency
            BigDecimal rate = toExchangeRate.divide(fromExchangeRate, AmountUtils.DEFAULT_CONTEXT);
            result = amount.multiply(rate);
        } else if (AmountUtils.isBaseCurrency(toExchangeRate)) {
            result = AmountUtils.convertToBaseCurrency(amount, fromExchangeRate);
        } else {
            result = AmountUtils.convertFromBaseCurrency(amount, toExchangeRate);
        }

        return result.setScale(FX_DEFAULT_SCALE, DEFAULT_ROUNDING_MODE);
    }

    /**
     * Return the amount as pence.
     *
     * @return the amount as pence
     */
    public long getAmountAsPence() {
        return theAmount.movePointRight(theAmount.scale()).longValue();
    }

    /**
     * Return the amount as a double, with a scale of the currency.
     *
     * @return double amount
     */
    public double getAmountAsDouble() {
        return theAmount.doubleValue();
    }

    public double getAmountAsPoundsAndPence() {
        return getAmountAsDouble();
    }

    /**
     * Return the ISO 4217 code for the currency.
     * <p>
     * For example this will return USD.
     *
     * @return three digit currency code
     */
    public String getCurrencyCode() {
        return theCurrency.getCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(theCurrency);
        hash = 29 * hash + Objects.hashCode(theAmount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Money other = (Money) obj;
        if (!other.isSameCurrency(this)) {
            return false;
        }
        return theAmount.compareTo(other.theAmount) == 0;
    }

    public BigDecimal getExchangeRate() {
        return theCurrency.getExchangeRate();
    }

    public String format(MoneyFormatter formatter) {
        if (formatter == null) {
            return null;
        }

        return formatter.format(this);
    }

    @Override
    public String toString() {
        return "Money{" + theCurrency.getCode() + theAmount + '}';
    }

}
