package gusl.model.money;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MoneyDTO implements Comparable {

    @DocField(description = "The currency")
    private MoneyCurrencyDTO currency;

    @Min(0)
    @DocField(description = "Monetary value in smallest unit e.g. pence i.e. no decimal place")
    private Long value;

    public MoneyDTO(Long value) {
        this.value = value;
    }

    public MoneyDTO add(MoneyDTO money) {
        return new MoneyDTO(currency, value + money.getValue());
    }

    public MoneyDTO add(long amount) {
        return new MoneyDTO(currency, value + amount);
    }

    @Override
    public int compareTo(Object o) {
        if (o == null || value == null) {
            return 0;
        }

        if (o instanceof MoneyDTO) {
            // Its assuming that they are the same currency
            return value.compareTo(((MoneyDTO) o).getValue());
        }

        return 0;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
