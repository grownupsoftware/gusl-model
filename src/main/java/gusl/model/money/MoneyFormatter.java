package gusl.model.money;

import gusl.core.tostring.ToString;
import gusl.core.utils.StringUtils;
import gusl.model.currency.Currency;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author dhudson on 2019-08-22
 */
public class MoneyFormatter {

    private boolean currencyCodePrefix;
    private boolean currencySymbolPrefix;
    private CharSequence prefix;
    private int movePointLeft;
    private int movePointRight;
    private int scale;
    private CharSequence postFix;
    private boolean stripTrailingZeros;
    private CharSequence nullValue;
    // Don't html unescape the currency symbol
    private boolean noEscape;
    private boolean currencyCodePostfix;
    private boolean currencySymbolPostfix;
    private CharSequence groupingSeparator;

    public MoneyFormatter() {
    }

    public boolean isCurrencyCodePrefix() {
        return currencyCodePrefix;
    }

    public void setCurrencyCodePrefix(boolean currencyCodePrefix) {
        this.currencyCodePrefix = currencyCodePrefix;
    }

    public boolean isCurrencySymbolPrefix() {
        return currencySymbolPrefix;
    }

    public void setCurrencySymbolPrefix(boolean currencySymbolPrefix) {
        this.currencySymbolPrefix = currencySymbolPrefix;
    }

    public CharSequence getPrefix() {
        return prefix;
    }

    public void setPrefix(CharSequence prefix) {
        this.prefix = prefix;
    }

    public int getMovePointLeft() {
        return movePointLeft;
    }

    public void setMovePointLeft(int movePointLeft) {
        this.movePointLeft = movePointLeft;
    }

    public int getMovePointRight() {
        return movePointRight;
    }

    public void setMovePointRight(int movePointRight) {
        this.movePointRight = movePointRight;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public CharSequence getPostFix() {
        return postFix;
    }

    public void setPostFix(CharSequence postFix) {
        this.postFix = postFix;
    }

    public boolean isStripTrailingZeros() {
        return stripTrailingZeros;
    }

    public void setStripTrailingZeros(boolean stripTrailingZeros) {
        this.stripTrailingZeros = stripTrailingZeros;
    }

    public CharSequence getNullValue() {
        return nullValue;
    }

    public void setNullValue(CharSequence nullValue) {
        this.nullValue = nullValue;
    }

    public boolean isNoEscape() {
        return noEscape;
    }

    public void setNoEscape(boolean noEscape) {
        this.noEscape = noEscape;
    }

    public boolean isCurrencyCodePostfix() {
        return currencyCodePostfix;
    }

    public void setCurrencyCodePostfix(boolean currencyCodePostfix) {
        this.currencyCodePostfix = currencyCodePostfix;
    }

    public boolean isCurrencySymbolPostfix() {
        return currencySymbolPostfix;
    }

    public void setCurrencySymbolPostfix(boolean currencySymbolPostfix) {
        this.currencySymbolPostfix = currencySymbolPostfix;
    }

    public CharSequence getGroupingSeparator() {
        return groupingSeparator;
    }

    public void setGroupingSeparator(CharSequence groupingSeparator) {
        this.groupingSeparator = groupingSeparator;
    }

    public String format(Money money) {
        if (money == null) {
            if (getNullValue() != null) {
                return getNullValue().toString();
            } else {
                return null;
            }
        }

        Currency currency = money.getCurrency();
        StringBuilder builder = new StringBuilder(20);
        if (isCurrencyCodePrefix()) {
            builder.append(currency.getCode());
        }

        if (isCurrencySymbolPrefix()) {
            builder.append(getCurrencySymbol(currency));
        }

        if (getPrefix() != null) {
            builder.append(getPrefix());
        }

        BigDecimal amount = money.getAmount();

        if (getMovePointLeft() > 0) {
            amount = amount.movePointLeft(getMovePointLeft());
        }

        if (getMovePointRight() > 0) {
            amount = amount.movePointRight(getMovePointRight());
        }

        if (getScale() > 0) {
            amount = amount.setScale(getScale(), RoundingMode.HALF_UP);
        }

        if (isStripTrailingZeros()) {
            amount = amount.stripTrailingZeros();
        }

        if (groupingSeparator != null) {
            builder.append(group(amount));
        } else {
            builder.append(amount.toPlainString());
        }

        if (getPostFix() != null) {
            builder.append(getPostFix());
        }

        if (isCurrencyCodePostfix()) {
            builder.append(currency.getCode());
        }

        if (isCurrencySymbolPostfix()) {
            builder.append(getCurrencySymbol(currency));
        }
        return builder.toString();
    }

    private String getCurrencySymbol(Currency currency) {
        if (isNoEscape()) {
            return currency.getSymbol();
        }

        return currency.getUnescapedSymbol();
    }

    private String group(BigDecimal amount) {

        String number = amount.toPlainString();
        boolean negative = false;

        if (number.startsWith("-")) {
            negative = true;
            number = number.substring(1);
        }

        try {
            // Gets rid of pence or not required to group
            if (number.length() < 3) {
                return number;
            }

            String[] numbers = StringUtils.split(number, '.');

            number = numbers[0];
            int len = number.length();
            StringBuilder builder = new StringBuilder(12);
            for (int i = 0; i < len; i++) {
                builder.append(number.charAt(i));
                if (i != (len - 1) && (len - i - 1) % 3 == 0) {
                    builder.append(getGroupingSeparator());
                }
            }

            if (numbers.length == 2) {
                builder.append(".");
                builder.append(numbers[1]);
            }
            if (negative) {
                return "-" + builder.toString();
            }

            return builder.toString();
        } catch (RuntimeException ex) {
            // Return something, but the above shouldn't happen
            return amount.toPlainString();
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public static class Builder {

        private final MoneyFormatter format;

        public Builder() {
            format = new MoneyFormatter();
        }

        public Builder setCurrencyPrefix() {
            format.setCurrencyCodePrefix(true);
            return this;
        }

        public Builder setSymbolPrefix() {
            format.setCurrencySymbolPrefix(true);
            return this;
        }

        public Builder setPrefix(CharSequence prefix) {
            format.setPrefix(prefix);
            return this;
        }

        public Builder setMovePointLeft(int pointLeft) {
            format.setMovePointLeft(pointLeft);
            return this;
        }

        public Builder setMovePointRight(int pointRight) {
            format.setMovePointRight(pointRight);
            return this;
        }

        public Builder setScale(int scale) {
            format.setScale(scale);
            return this;
        }

        public Builder setPostfix(CharSequence postfix) {
            format.setPostFix(postfix);
            return this;
        }

        public Builder stripTrailingZeros() {
            format.setStripTrailingZeros(true);
            return this;
        }

        public Builder setNullValue(CharSequence value) {
            format.setNullValue(value);
            return this;
        }

        public Builder setCurrencyPostfix() {
            format.setCurrencyCodePostfix(true);
            return this;
        }

        public Builder setSymbolPostfix() {
            format.setCurrencySymbolPostfix(true);
            return this;
        }

        public Builder setNoEscape() {
            format.setNoEscape(true);
            return this;
        }

        public Builder setGroupingSeparator(CharSequence separator) {
            format.setGroupingSeparator(separator);
            return this;
        }

        public MoneyFormatter build() {
            return format;
        }

    }
}
