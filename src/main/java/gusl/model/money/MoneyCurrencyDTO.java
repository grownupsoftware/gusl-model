package gusl.model.money;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.annotations.DocField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class MoneyCurrencyDTO {

    @DocField(description = "Currency Code")
    @UiField(type = FieldType.text, label = "Code", displayInTable = true, filterInTable = true)
    private String code;

    @DocField(description = "Currency Symbol")
    @UiField(type = FieldType.symbol, label = "Symbol", displayInTable = true)
    private String symbol;

    @DocField(description = "Number of decimal places")
    @UiField(type = FieldType.number, label = "Order")
    private Integer decimalPlaces;

    @UiField(type = FieldType.number, label = "Rounding")
    private Integer displayPlaces;

}
