/* Copyright lottomart */
package gusl.model.money;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.core.tostring.ToString;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class MoneyDO implements Comparable {

    @JsonProperty("c")
    private String currencyCode;

    @JsonProperty("v")
    private long value;

    public void setValue(Long value) {
        this.value = value;
    }

    public MoneyDO multiply(Integer multiplier) {
        return new MoneyDO(currencyCode, value * multiplier);
    }

    public MoneyDO divide(Integer divider) {
        return new MoneyDO(currencyCode, value / divider);
    }

    public MoneyDO add(Long pence) {
        return new MoneyDO(currencyCode, value + pence);
    }

    public MoneyDO negate() {
        return new MoneyDO(currencyCode, -value);
    }

    public MoneyDO makeNegative() {
        if (value > 0) {
            return negate();
        }
        return this;
    }

    public MoneyDO abs() {
        if (value > 0) {
            return new MoneyDO(currencyCode, value);
        } else {
            return negate();
        }
    }

    public MoneyDO add(MoneyDO moneyDo) {
        if (moneyDo == null) {
            return new MoneyDO(currencyCode, value);
        } else {
            return new MoneyDO(currencyCode, value + moneyDo.getValue());
        }
    }

    public MoneyDO minus(MoneyDO moneyDO) {
        return new MoneyDO(currencyCode, value - moneyDO.getValue());
    }

    public MoneyDO newMoney() {
        return new MoneyDO(currencyCode, 0L);
    }

    @JsonIgnore
    public boolean isZero() {
        return value == 0;
    }

    @JsonIgnore
    public boolean isLessThan(MoneyDO amount) {
        return value < amount.getValue();
    }

    @JsonIgnore
    public boolean isGreaterThan(MoneyDO amount) {
        return value > amount.getValue();
    }

    @JsonIgnore
    public boolean isLessThanOrEqual(MoneyDO amount) {
        return value <= amount.getValue();
    }

    @JsonIgnore
    public boolean isEqual(MoneyDO amount) {
        return value == amount.getValue();
    }

    @JsonIgnore
    public boolean isNegative() {
        return value < 0;
    }

    @JsonIgnore
    public boolean isPositive() {
        return value > 0;
    }

    @Override
    public int compareTo(Object o) {
        if (o != null) {
            if (o instanceof MoneyDO) {
                return Long.compare(getValue(), ((MoneyDO) o).getValue());
            }
        }

        return 0;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
