package gusl.model.money;

import gusl.model.currency.Currency;
import lombok.CustomLog;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Optional;

/**
 * @author dhudson
 */
@CustomLog
public class AmountUtils {

    public static final MathContext DEFAULT_CONTEXT = new MathContext(16, RoundingMode.HALF_UP);
    private static final int DEFAULT_SCALE = 4;

    private AmountUtils() {
    }

    /**
     * Get the default {@link MathContext} used for calculations.
     *
     * @return
     */
    public static MathContext getDefaultContext() {
        return DEFAULT_CONTEXT;
    }

    public static int getDefaultScale() {
        return DEFAULT_SCALE;
    }

    /**
     * Scale the amount using the default scale value and rounding mode.
     *
     * @param amount
     * @return
     */
    public static BigDecimal scale(BigDecimal amount) {
        return scale(amount, getDefaultScale());
    }

    /**
     * Scale the amount using the scale value and default rounding mode.
     *
     * @param amount
     * @param scale
     * @return
     */
    public static BigDecimal scale(BigDecimal amount, int scale) {
        return amount.setScale(scale, DEFAULT_CONTEXT.getRoundingMode());
    }

    /**
     * Convert amount to GBP if required.
     *
     * @param amount
     * @param exchangeRate
     * @return convert amount, or same amount if exchange rate = 1
     */
    public static BigDecimal convertToBaseCurrency(BigDecimal amount, BigDecimal exchangeRate) {
        if (!isBaseCurrency(exchangeRate)) {
            amount = amount.divide(exchangeRate, getDefaultContext());
        }
        return scale(amount);
    }

    public static double convertToBaseCurrency(double amount, Currency currency) {
        return convertToBaseCurrency(asBigDecimal(amount), currency.getExchangeRate()).doubleValue();
    }

    /**
     * Convert amount from GBP if required.
     *
     * @param amount
     * @param exchangeRate
     * @return convert amount, or same amount if exchange rate = 1
     */
    public static BigDecimal convertFromBaseCurrency(BigDecimal amount, BigDecimal exchangeRate) {
        if (!isBaseCurrency(exchangeRate)) {
            amount = amount.multiply(exchangeRate, getDefaultContext());
        }
        return scale(amount);
    }

    public static double convertFromBaseCurrency(double amount, Currency currency) {
        return convertFromBaseCurrency(asBigDecimal(amount), currency.getExchangeRate()).doubleValue();
    }

    /**
     * Check to see if the exchange rate is 1.
     *
     * @param exchangeRate to check
     * @return true if the exchange rate is 1
     */
    public static boolean isBaseCurrency(BigDecimal exchangeRate) {
        return (exchangeRate.compareTo(BigDecimal.ONE) == 0);
    }

    public static boolean isBaseCurrency(Currency currency) {
        return isBaseCurrency(currency.getExchangeRate());
    }

    /**
     * Convert double to big decimal
     *
     * @param aDouble
     * @return a new BigDecimal
     */
    public static BigDecimal asBigDecimal(double aDouble) {
        return new BigDecimal(Double.toString(aDouble));
    }

    public static Long getValueOrZero(MoneyDTO moneyDTO) {
        return Optional.ofNullable(moneyDTO)
                .map(MoneyDTO::getValue)
                .orElse(0L);
    }

    public static Long getValueOrZero(MoneyDO moneyDTO) {
        return Optional.ofNullable(moneyDTO)
                .map(MoneyDO::getValue)
                .orElse(0L);
    }

    public static long getValueOrOne(Integer value) {
        return Optional.ofNullable(value)
                .map(Integer::intValue)
                .orElse(1);
    }

}
