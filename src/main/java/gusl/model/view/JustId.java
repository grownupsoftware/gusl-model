package gusl.model.view;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JustId {
    private Long id;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
