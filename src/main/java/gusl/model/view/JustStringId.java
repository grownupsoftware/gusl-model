package gusl.model.view;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JustStringId {

    private String id;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
