package gusl.model.schedule;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 30/09/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class TriggerDTO {

    private String jobName;
}
