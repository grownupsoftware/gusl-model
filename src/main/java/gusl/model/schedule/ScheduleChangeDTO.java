package gusl.model.schedule;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 05/11/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class ScheduleChangeDTO {

    private String id;
    private int minutes;
    private String cronString;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
