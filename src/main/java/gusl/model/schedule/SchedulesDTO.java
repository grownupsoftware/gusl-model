package gusl.model.schedule;

import gusl.core.tostring.ToString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dhudson
 */
public class SchedulesDTO {

    private static final Comparator<ScheduleDTO> NAME_SORT = (a, b) -> a.getJobName().compareTo(b.getJobName());
    private List<ScheduleDTO> jobs;

    public SchedulesDTO() {
    }

    public List<ScheduleDTO> getJobs() {
        return jobs;
    }

    public void setJobs(List<ScheduleDTO> jobs) {
        this.jobs = jobs;
    }

    public void add(ScheduleDTO sched) {
        if (jobs == null) {
            jobs = new ArrayList<>();
        }
        jobs.add(sched);
    }

    public void sort() {
        if (jobs == null) {
            return;
        }

        Collections.sort(jobs, NAME_SORT);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
