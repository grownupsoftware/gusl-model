package gusl.model.schedule;

import gusl.core.tostring.ToString;
import lombok.*;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ScheduleDTO {

    private String id;
    private String jobName;
    private String nextRun;
    private int schedule;
    private String cronString;
    private String timeZone;
    private String lastExecuted;
    private boolean canTrigger;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
