package gusl.model.bigquery;

/**
 * @author dhudson on 25/03/2020
 */
public enum BQType {
    STRING,
    OBJECT,
    MAP,
    INTEGER,
    LONG,
    FLOAT,
    BOOLEAN,
    DATE,
    TIMESTAMP,
    JSON,
    LIST_SIMPLE,
    LIST_COMPLEX
}
