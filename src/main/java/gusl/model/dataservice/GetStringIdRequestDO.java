/* Copyright lottomart */
package gusl.model.dataservice;

import gusl.core.tostring.ToString;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class GetStringIdRequestDO {

    private String username;
    private String id;
    private String secondId;

    public static GetStringIdRequestDO of(String username, String id) {
        return GetStringIdRequestDO.builder()
                .username(username)
                .id(id)
                .build();
    }

    public static GetStringIdRequestDO of(String username, String id, String secondId) {
        return GetStringIdRequestDO.builder()
                .username(username)
                .id(id)
                .secondId(secondId)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
