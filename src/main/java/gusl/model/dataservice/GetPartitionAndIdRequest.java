package gusl.model.dataservice;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetPartitionAndIdRequest {
    private String username;
    private Long partitionId;
    private Long id;

    public static GetPartitionAndIdRequest of(String username, Long partitionId, Long id) {
        return GetPartitionAndIdRequest.builder()
                .username(username)
                .partitionId(partitionId)
                .id(id)
                .build();
    }
}
