package gusl.model.dataservice.dtos;

import gusl.core.tostring.ToString;
import gusl.validators.GUSLNotEmpty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AuditableDTO<DO> {

    @GUSLNotEmpty
    private DO contents;

    @GUSLNotEmpty
    private String username;

    public static <T> AuditableDTO<T> of(String username, T contents) {
        return AuditableDTO.<T>builder()
                .username(username)
                .contents(contents)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
