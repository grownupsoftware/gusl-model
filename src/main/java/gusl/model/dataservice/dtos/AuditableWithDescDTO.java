package gusl.model.dataservice.dtos;

import gusl.core.tostring.ToString;

/**
 *
 * @author dhudson
 * @param <DO>
 */
public class AuditableWithDescDTO<DO> extends AuditableDTO<DO> {

    private String description;

    public AuditableWithDescDTO() {
    }

    public AuditableWithDescDTO(DO contents, String email, String description) {
        super(contents, email);
        this.description = description;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
