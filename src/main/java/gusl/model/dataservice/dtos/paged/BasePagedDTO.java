package gusl.model.dataservice.dtos.paged;

import java.util.List;

/**
 *
 * @author dhudson
 * @param <DO>
 */
public class BasePagedDTO<DO> {

    private List<DO> content;

    public List<DO> getContent() {
        return content;
    }

    public void setContent(List<DO> content) {
        this.content = content;
    }
}
