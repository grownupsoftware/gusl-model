/* Copyright lottomart */
package gusl.model.dataservice.dtos.paged;

import gusl.core.annotations.DocField;
import gusl.model.dataservice.GetAllRequestDO;

/**
 *
 * @author grant
 */
public abstract class AbstractPagedDTO {

    @DocField(description = "Ignore query params and get all")
    private boolean ignore;

    @DocField(description = "The page offset e.g. page 5")
    private Integer offset;

    @DocField(description = "Number of items to display")
    private Integer perPage;

    @DocField(description = "The name of the field that the data is ordered by")
    private String orderParam;

    @DocField(description = "The order direction e.g. ASC, DESC")
    private GetAllRequestDO.OrderDirection orderDirection;

    @DocField(description = "Ignore query params and get all")
    private boolean hasContent;

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public String getOrderParam() {
        return orderParam;
    }

    public void setOrderParam(String orderParam) {
        this.orderParam = orderParam;
    }

    public GetAllRequestDO.OrderDirection getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(GetAllRequestDO.OrderDirection orderDirection) {
        this.orderDirection = orderDirection;
    }

    public boolean isHasContent() {
        return hasContent;
    }

    public void setHasContent(boolean hasContent) {
        this.hasContent = hasContent;
    }

    @Override
    public String toString() {
        return "ignore=" + ignore + ", offset=" + offset + ", perPage=" + perPage + ", orderParam=" + orderParam + ", orderDirection=" + orderDirection + ", hasContent=" + hasContent;
    }

}
