package gusl.model.dataservice;

import gusl.core.annotations.DocField;

import java.util.Map;

import gusl.core.tostring.ToString;
import gusl.model.AuditableDO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Builder;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetAllRequestDO {

    public enum OrderDirection {
        ASC, DESC
    }

    private String username;

    @DocField(description = "Ignore query params and get all")
    private boolean ignore;

    @DocField(description = "The page offset e.g. page 5")
    private int offset;

    @DocField(description = "Number of items to display")
    private int perPage;

    @DocField(description = "The name of the field that the data is ordered by")
    private String orderParam;

    @DocField(description = "The order direction e.g. ASC, DESC")
    private OrderDirection orderDirection;

    @DocField(description = "Map of field names and filter (matching) values")
    private Map<String, Object> queryParams;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
