package gusl.model.dataservice;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetIdRequestDO {
    private String username;
    private String id;
    private String secondId;

    public static GetIdRequestDO of(String username, String id) {
        return GetIdRequestDO.builder()
                .username(username)
                .id(id)
                .build();
    }

    public static GetIdRequestDO of(String username, String id, String secondId) {
        return GetIdRequestDO.builder()
                .username(username)
                .id(id)
                .secondId(secondId)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
