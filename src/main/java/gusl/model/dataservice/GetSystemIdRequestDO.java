package gusl.model.dataservice;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetSystemIdRequestDO {

    private Long id;

    public static GetSystemIdRequestDO of(Long id) {
        return GetSystemIdRequestDO.builder().id(id).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
