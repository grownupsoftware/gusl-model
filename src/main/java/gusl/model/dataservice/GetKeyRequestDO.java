package gusl.model.dataservice;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetKeyRequestDO {

    private String username;
    private String id;

    public static GetKeyRequestDO of(String username, String id) {
        return GetKeyRequestDO.builder()
                .username(username)
                .id(id)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
