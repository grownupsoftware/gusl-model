package gusl.model.cohort;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CohortIdsDO {

    @Singular
    private List<String> ids;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
