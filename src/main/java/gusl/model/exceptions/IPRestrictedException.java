package gusl.model.exceptions;

/**
 *
 * @author dhudson
 */
public class IPRestrictedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String theCountry;
    private String theIp;

    public IPRestrictedException() {
    }

    public IPRestrictedException(String country, String ip) {
        theCountry = country;
        theIp = ip;
    }

    public String getCountry() {
        return theCountry;
    }

    public String getIp() {
        return theIp;
    }

}
