package gusl.model.exceptions;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.errors.SystemErrors;

public class InsufficientFundsException extends GUSLErrorException {

    private static final long serialVersionUID = 1L;

    public InsufficientFundsException() {
    }

    public InsufficientFundsException(long account, long subAccount) {
        super(SystemErrors.INSUFFICIENT_FUNDS.getError(Long.toString(account), Long.toString(subAccount)), true);
    }

    public InsufficientFundsException(String account, long subAccount) {
        super(SystemErrors.INSUFFICIENT_FUNDS.getError(account, Long.toString(subAccount)), true);
    }
}
