/* Copyright lottomart */
package gusl.model.exceptions;

import gusl.core.errors.ErrorDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author grant
 */
public class UnAuthorizedException extends RuntimeException {

    private List<ErrorDO> errors = new ArrayList<>(1);

    public UnAuthorizedException(String message, Throwable ex) {
        super(message, ex);
    }

    public UnAuthorizedException(String message) {
        super(message);
    }

    public UnAuthorizedException(ErrorDO error) {
        super(error.toString());
        this.errors.add(error);
    }

    public UnAuthorizedException(ErrorDO error, Throwable cause) {
        super(error.toString(), cause);
        this.errors.add(error);
    }

    public ErrorDO getError() {
        if (!errors.isEmpty()) {
            return errors.get(0);
        } else {
            return new ErrorDO(null, getMessage(), "internal.error");
        }
    }

}
