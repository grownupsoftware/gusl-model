package gusl.model.exceptions;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.errors.SystemErrors;

public class InsufficientDiamondsException extends GUSLErrorException {

    private static final long serialVersionUID = 1L;

    public InsufficientDiamondsException() {
    }

    public InsufficientDiamondsException(long account) {
        super(SystemErrors.INSUFFICIENT_DIAMONDS.getError(Long.toString(account)), true);
    }
}
