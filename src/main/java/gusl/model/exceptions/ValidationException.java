package gusl.model.exceptions;


import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorType;
import gusl.core.exceptions.GUSLErrorException;

import java.util.List;
import java.util.Locale;

public class ValidationException extends GUSLErrorException {

    private static final long serialVersionUID = 8448522490426410112L;
    private ErrorType errorType;

    public ValidationException(String field, String message, String messageKey, String[] params, ErrorType errorType) {
        super(new ErrorDO(field, message, messageKey, params));
        this.errorType = errorType;
    }

    public ValidationException() {
    }

    public ValidationException(ErrorDO error) {
        super(error);
    }

    public ValidationException(List<ErrorDO> errors) {
        super(errors);
    }

    public ValidationException(Locale locale, ErrorDO error) {
        super(locale, error);
    }

    public ValidationException(Locale locale, List<ErrorDO> errors) {
        super(locale, errors);
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ValidationException [errors=");
        builder.append(getErrors());
        builder.append("]");
        return builder.toString();
    }
}
