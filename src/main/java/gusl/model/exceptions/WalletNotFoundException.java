package gusl.model.exceptions;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.errors.SystemErrors;

public class WalletNotFoundException extends GUSLErrorException {

    private static final long serialVersionUID = 1L;

    public WalletNotFoundException() {
    }

    public WalletNotFoundException(long account, long subAccount) {
        addError(SystemErrors.WALLET_NOT_FOUND.getError(new String[]{Long.toString(account), Long.toString(subAccount)}));
    }
}
