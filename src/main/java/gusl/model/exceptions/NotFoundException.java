package gusl.model.exceptions;


import gusl.core.errors.ErrorDO;

import java.util.List;
import java.util.Locale;

public class NotFoundException extends ValidationException {

    private static final long serialVersionUID = 7183729751478690514L;

    /**
     * Create a {@link NotFoundException} containing the given error with
     * default locale.
     *
     * @param error
     */
    public NotFoundException(ErrorDO error) {
        super(error);
    }

    /**
     * Create a {@link NotFoundException} containing the given errors with
     * default locale.
     *
     * @param errors
     */
    public NotFoundException(List<ErrorDO> errors) {
        super(errors);
    }

    /**
     * Constructor.
     *
     * @param locale
     * @param error
     */
    public NotFoundException(Locale locale, ErrorDO error) {
        super(locale, error);
    }

    /**
     * Constructor.
     *
     * @param locale
     * @param errors
     */
    public NotFoundException(Locale locale, List<ErrorDO> errors) {
        super(locale, errors);
    }
}
