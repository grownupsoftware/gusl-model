/* Copyright lottomart */
package gusl.model.exceptions;


import gusl.core.errors.ErrorDO;

import java.util.ArrayList;
import java.util.List;

/**
 * a runtime exception - player validation
 * @author grant
 */
public class AccessDeniedException extends RuntimeException {

    private List<ErrorDO> errors = new ArrayList<>(1);

    public AccessDeniedException(String message, Throwable ex) {
        super(message, ex);
    }

//    public AccessDeniedException(String message) {
//        super(message);
//    }

    public AccessDeniedException(ErrorDO error) {
        super(error.toString());
        this.errors.add(error);
    }

    public AccessDeniedException(ErrorDO error, Throwable cause) {
        super(error.toString(), cause);
        this.errors.add(error);
    }

    public ErrorDO getError() {
        if (!errors.isEmpty()) {
            return errors.get(0);
        } else {
            return new ErrorDO(null, getMessage(), "internal.error");
        }
    }

}
