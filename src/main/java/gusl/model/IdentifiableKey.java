package gusl.model;

public interface IdentifiableKey<Key> {

    Key getKey();

}
