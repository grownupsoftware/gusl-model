package gusl.model.decimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import static java.util.Objects.isNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class DecimalDO implements Comparable<DecimalDO> {

    @JsonProperty("v")
    private long value;

    @JsonProperty("p")
    private int decimalPlaces;

    /**
     * value will be ajsuted for decimal places - i.e. Math.pow
     * @param value
     * @param decimalPlaces
     * @return
     */
    public static DecimalDO of(long value, int decimalPlaces) {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(Double.valueOf(value * Math.pow(10, decimalPlaces)).longValue())
                .build();
    }

    /**
     * long value is taken as raw - no Math.pow
     * @param value
     * @param decimalPlaces
     * @return
     */
    public static DecimalDO from(long value, int decimalPlaces) {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(value)
                .build();
    }

    public static DecimalDO from(Double value, int decimalPlaces) {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(Double.valueOf(value * Math.pow(10, decimalPlaces)).longValue())
                .build();
    }

    public static BigDecimal asBigDecimal(DecimalDO line) {
        return isNull(line) ? null : BigDecimal.valueOf(line.asDouble());
    }

    public int asInt() {
        return Double.valueOf(asDouble()).intValue();
    }

    public static DecimalDO of(BigDecimal line) {
        return isNull(line) ? null : from(line.doubleValue(), 2);
    }

    public DecimalDO clone() {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(value)
                .build();
    }

    public boolean isGreater(DecimalDO other) {
        return value * Math.pow(10, decimalPlaces) > other.getValue() * Math.pow(10, other.getDecimalPlaces());
    }

    public boolean isLess(DecimalDO other) {
        return value * Math.pow(10, decimalPlaces) < other.getValue() * Math.pow(10, other.getDecimalPlaces());
    }

    public double asDouble() {
        return value / Math.pow(10, decimalPlaces);
    }

    public long asLong() {
        return BigDecimal.valueOf(asDouble()).setScale(0, RoundingMode.HALF_UP).longValue();
    }

    @Override
    public int compareTo(DecimalDO other) {
        if (isNull(other)) {
            return +1;
        }
        return Double.valueOf(value * Math.pow(10, decimalPlaces)).compareTo(Double.valueOf(other.getValue() * Math.pow(10, other.getDecimalPlaces())));
    }

    public String toPlainString() {
        return toPlainString(false, false);
    }

    public String toPlainString(boolean withSign) {
        return toPlainString(withSign, false);
    }

    public String toPlainString(boolean withSign, boolean trimZero) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(decimalPlaces);
        df.setMinimumFractionDigits(1); // <-- s/be 0 but find outcomes do not match
        df.setGroupingUsed(false);
        String format = df.format(asDouble());
        if (withSign) {
            if (value >= 0) {
                format = "+" + format;
            }
        }
        if (trimZero) {
            // sbe replaced with a regex
            if (format.endsWith(".000")) {
                return format.replace(".000", "");
            } else if (format.endsWith(".00")) {
                return format.replace(".00", "");
            } else if (format.endsWith(".0")) {
                return format.replace(".0", "");
            }
        }

        return format;
    }

    @Override
    public String toString() {
        return "DecimalDO { value: " + value + " decimalPlaces: " + decimalPlaces + " formatted: " + toPlainString() + "}";
    }

    public DecimalDO flipped() {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(value * -1)
                .build();
    }

    public DecimalDO abs() {
        return DecimalDO.builder()
                .decimalPlaces(decimalPlaces)
                .value(Math.abs(value))
                .build();
    }

    public boolean isSame(DecimalDO line) {
        return this.compareTo(line) == 0;
    }

    public static boolean areSame(DecimalDO a, DecimalDO b) {
        if (isNull(a) && isNull(b)) {
            return true;
        } else if (isNull(a)) {
            return false;
        } else if (isNull(b)) {
            return false;
        }

        return a.toPlainString().equals(b.toPlainString());
    }

    public void add(long val) {
        this.value += val;
    }

    public void adjust(long val) {
        this.value += val;
    }

    public void subtract(long val) {
        this.value -= val;
    }

    @JsonIgnore
    public boolean isPositive() {
        return value >= 0;
    }

}
