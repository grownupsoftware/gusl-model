package gusl.model.decimal;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TotalDO {

    private DecimalDO value;

    private String prefix;

    private String suffix;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
