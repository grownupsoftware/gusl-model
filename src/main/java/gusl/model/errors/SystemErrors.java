package gusl.model.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

import static java.util.Objects.nonNull;

/**
 * @author grant
 */
public enum SystemErrors {

    // System wide api errors
    NODE_ROUTE_NODE("LMSE01 Router node header missing", "route.node.header.missing"),
    NO_ROUTE_TO_NODE("LMSE02 No route to node {0} {1}", "no.route.to.node"),
    IO_EXCEPTION("LMSE03 IO Exception {0}", "io.exception"),
    REQUEST_FAILED("LMSE04 Proxy request failed", "proxy.request.failed"),
    REQUEST_CANCELLED("LMSE05 Proxy request cancelled", "proxy.request.cancelled"),
    INSERT_DAO_ERROR("LMSE06 insert dao {0} error: {1}", "dao.error"),
    COUNT_DAO_ERROR("LMSE06 count dao {0} error: {1}", "dao.error"),
    UPDATE_DAO_ERROR("LMSE07 update dao {0} error: {1}", "dao.error"),
    DELETE_DAO_ERROR("LMSE08 insert dao {0} error: {1}", "dao.error"),
    DAO_ERROR("LMSE09 internal dao error: {0}", "dao.error"),
    DAO_GET_ALL_ERROR("LMSE10 get all dao Table: {0} error: {1} ", "dao.error"),
    NO_LINE_AVAILABLE("id", "LMSE11 No available game line found: game instance {1}", "no.game.line.available"),
    ID_NOT_FOUND("id", "LMSE12 Id not found: {1} Table: {0}", "id.not.found"),
    ID_2_NOT_FOUND("id", "LMSE13 Id not found: {1} {2} Table: {0}", "id.not.found"),
    ID_3_NOT_FOUND("id", "LMSE14 Id not found: {1} {2} {3} Table: {0}", "id.not.found"),
    TERMS_NOT_FOUND("LMSE15 Failed to find terms and conditions", "terms.not.found"),
    ACCESS_DENIED("LMSE16 Access Denied", "access.denied"),
    FAILED_TO_FIND_CREDENTIALS("LMSE17 Failed to get player or session from dataservice", "failed.to.find.credentials"),
    PLAYER_DENIED("LMSE18 Player access is denied", "player.access.denied"),
    INCORRECT_PLAYER_STATUS_FOR_OPERATION("LMSE19 Player status does not allow that operation", "incorrect.player.status.for.operation"),
    SESSION_NOT_VALID("LMSE20 Session no longer valid", "session.no.longer.valid"),
    CONSTRAINT_VIOLATION("LMSE21 Constraint violation error: Resource: {0} Method: {1} Field: {2} Violation: {3}", "constraint.violation"),
    BAD_JSON_INPUT_ERROR_KEY("LMSE22 Bad Json", "resource.error.bad_json"),
    ERR_SERVER_ERROR("LMSE23 Internal error: {0}", "internal.error"),
    NOT_ALLOWED("LMSE24 Not allowed", "not.allowed"),
    NOT_SUPPORTED("LMSE25 Unsupported media type", "not.supported"),
    NOT_FOUND("LMSE26 Not found", "not.found"),
    RESOURCE_NOT_FOUND("LMSE27 Resource (endpoint) does not exist {0}", "resource.not.found"),
    ERR_CANNOT_COMPLETE("LMSE28 Your request could not be completed", "server.error"),
    ERR_TIMEOUT("LMSE29 Request timed out", "server.timeout.error"),
    ERR_VALUE_IS_NULL("LMSE30 value", "the value is null", "value.isnull"),
    ERR_NAME_IS_NULL("name", "LMSE31 the name is null", "name.isnull"),
    ERR_TYPE_IS_NULL("type", "LMSE32 the type is null", "type.isnull"),
    ERR_VALUE_IS_NOT_DIFFERENT("value", "LMSE33 the value is not different", "value.notdifferent"),
    ENCODING_ERROR("LMSE34 Request must be un encoded or use gzip", "server.error.request.content_encoding"),
    UNAUTHORIZED("LMSE35 Not authorised", "server.error.not.authorised"),
    INVALID_TOKEN("LMSE36 Invalid token", "server.error.invalid.token"),
    SESSION_TOKEN_EXPIRED("LMSE37 Session Token Expired", "session.token.expired"),
    DESERIALISE_ERROR("LMSE28 Error reading and deserializing request content", "server.error.request.derserializing"),
    CLASS_NOT_FOUND("LMSE39 Class of type {0} does not exist", "server.error.class.notfound"),
    MISSING_X_BODY("LMSE40 Invalid request. The request does not contain a X-Body-Type header", "server.error.request.no.xbody"),
    HTTP_SERVICE_NOT_CONFIGURED("LMSE41 HTTP Service has not been configured", "server.error.http.service.notconfigured"),
    INVALID_COUNTRY_CODE("LMSE42 Invalid Country Code", "invalid.country.code"),
    INVALID_CURRENCY_CODE("LMSE43 Invalid Currency Code", "invalid.currency.code"),
    COUNTRY_NOT_ALLOWED("LMSE44 Country not allowed", "country.not.allowed"),
    INVALID_POST_CODE("LMSE45 Invalid post code", "gusl.validation.Postcode"),
    CURRENCY_NOT_ALLOWED("LMSE46 Currency not allowed", "currency.not.allowed"),
    TOO_MANY_REGISTRATIONS("LMSE47 Too many registrations from one IP address", "too.many.registrations"),
    INVALID_TITLE("LMSE48 Invalid Title", "invalid.title"),
    INVALID_GENDER("LMSE49 Invalid Gender", "invalid.gender"),
    MISSING_AUTH_HEADER("LMSE50 Missing Auth Header", "missing.auth.header"),
    ELASTIC_ERROR("LMSE51 Elastic error: {0}", "elastic.error"),
    PAYMENT_FAILED("LMSE52 Payment failed, Reason {0}, Player {1}, Card {2}", "payment.failed"),
    REQUIRED_PASSWORD_EXPIRY("LMSE53 Required password expiry", "required.password.expiry"),
    AUTH_REST_FUTURE_ERROR("LMSE54 Interrupted Rest Auth future: {0}", "interrupted.future.for.auth.error"),
    REST_RESUME_WITH_EXCEPTION("LMSE55 REST resume async response with exception: {0}", "resume.rest.async.with.exception"),
    HTTP_RESUME_WITH_EXCEPTION("LMSE56 HTTP resume async response with exception: {0}", "resume.http.async.with.exception"),
    UNEXPECTED_ERROR("LMSE57 Unexpected exception: {0}", "unexpected.exception"),
    LM_SERVER_ERROR("LMSE58 Lm exception: {0}", "gusl.exception"),
    SERIALISATION_ERROR("LMSE59 Serialisation exception: {0}", "serialisation.exception"),
    NON_200_HTTP_RESPONSE("LMSE60 Non 200 response exception: {0}", "non.200.http.response"),
    NO_COUNTRY_PRIZE_TABLE("LMSE61 No country prize table", "no.country.prize.table"),
    NO_CURRENCY_PRIZE_TABLE("LMSE62 No currency prize table", "no.currency.prize.table"),
    LOTTOLAND_MISSING_ODDS_LIST("LMSE63 Lottoland odds list is empty", "lottoland.empty.odds.list"),
    LOTTOLAND_MISSING_RANKS_LIST("LMSE64 Lottoland ranks list is empty", "lottoland.empty.ranks.list"),
    LOTTOLAND_MISSING_RESULT_DATA("LMSE65 Lottoland missing result data", "lottoland.missing.result.data"),
    LOTTOLAND_TIER_MISMATCH("LMSE65 Lottoland tier mismatch", "lottoland.tier.mismatch"),
    INSUFFICIENT_FUNDS("LMSE66 Insufficient Funds for player {}, subAccount {}", "insufficient.funds"),
    INSUFFICIENT_DIAMONDS("LMSE78 Insufficient Diamonds for Player {}", "insufficient.diamonds"),
    WALLET_NOT_FOUND("LMSE67 Wallet not found player {}, subAccount {}", "wallet.not.found"),
    HTTP_TIMEOUT("LMSE68 HTTP timeout", "http.timeout"),
    HTTP_FUTURE_ERROR("LMSE69 Error waiting for future", "error.waiting.fo.future"),
    ES_WRITE_ERROR("LMSE70 failed to write to elastic search [{}]", "elastic.search.write.error"),
    ERR_RELEASE_TIMEOUT("LMSE71 Request timed out for node release info", "server.timeout.error"),
    ERR_REST_AUTH_TIMEOUT("LMSE72 Request timed out for rest auth", "server.timeout.error"),
    ERR_REST_RESPONSE_TIMEOUT("LMSE73 Request timed out for rest auth", "server.timeout.error"),
    ERR_ASYNC_RESPONSE_TIMEOUT("LMSE74 Request async timed out timeout [{0}] seconds", "server.timeout.error"),
    INVALID_CACHE_DATA("LMSE75 Invalid cache data", "invalid.cache.data"),
    INVALID_GAME("LMSE53 Invalid Game: {0}", "invalid.game"),
    INSTANCE_NOT_FOUND("LMSE77 Instance not found: {0}", "instance.not.found.in.draw"),
    CANNOT_READ("LMSE78 Cannot read from disk: {0}", "cannot.not.read.from.disk"),
    GENERAL_IO_ERROR("LMSE79 General file IO exception: {0}", "general.io.errro"),
    CANNOT_WRITE("LMSE80 Cannot write to disk: {0}", "cannot.not.write.to.disk"),
    INVALID_DOB("LMSE81 Invalid DoB", "invalid.dob"),
    INVALID_DRAW_ORDER_TYPE("LMSE82 Invalid draw order type {0}", "invalid.draw.order.type"),
    PLAYER_VERIFY("LMSE82 Player needs to be verified", "player.verification"),
    NULL_POINTER("LMSE83 Null pointer", "null.pointer"),
    SERVER_THROWABLE("LMSE84 Throwable {0}", "internal.throwable"),
    INVALID_LINK_PARAMS("LMSE85 Invalid link param settings", "invalid.link.params"),
    API_RESPONSE_NULL("LMSE86 Null response from {0} URL:{1}", "api.response.null"),
    API_ERROR("LMSE87 IError {0} from {1} for url:{2}", "api.error"),
    SILVERPOP_MUID_NOT_FOUND("LMSE88 Could not find contact with MUID={0}", "silverpop.muid.notFound"),
    PLAYER_NOT_ALLOWED_CASINO("LMSE89 Player {0} not allowed to play Casino", "player.casino.blocked"),
    NO_FREE_SPINS_LEFT("LMSE90 No Free spins left", "no.free.spins.left"),
    FREE_SPINS_EXPIRED("LMSE91 Free spins expired", "free.spins.expired"),
    NO_GAME_PROVIDER("LMSE92 No game provider for {0}", "no.game.provider"),
    INVALID_VALUE("LMSE93 Invalid value, field {0} {1}", "invalid.value"),
    NO_FREE_SPIN_VIEW("LMSE94 Can't get Free spin view for ext ref {0}, Player {1}", "invalid.freespin.reference"),
    LOG_CONTROL_ERROR("LMSE95 Log Control Error {0}", "log.error.control"),
    INVALID_CIDR("LMSE96 Invalid CIDR {0}", "invalid.cidr"),
    INVALID_BUNDLE_PARAMS("LMSE97 Invalid bundle param settings", "invalid.bundle.params"),
    ROUTER_CLIENT_SEND_FAILED("LMSE98 Failed to send node message {0}, {1}", "failed.node.message"),
    CANNOT_PUSH_MESSAGE(null, "LMSE99 Can't push notification...", "cannot.push.message"),
    CASSANDRA_BATCH_ERROR(null, "LMSE100 Can not write to cassandra Batch after {} retries", "cassandra.batch.write"),
    FAILED_TO_GET_GETTER_METHOD(null,"CASE12 Failed to get getter method {0}", "failed.to.get.getter.method"),
    FAILED_TO_CREATE_PREDICATE(null,"CASE13 Failed to create predicate {0}", "failed.to.create.predicate");


    private String field;
    private final String message;
    private final String messageKey;

    SystemErrors(String field, String message, String messageKey) {
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
    }

    SystemErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        if (field != null) {
            return new ErrorDO(field, message, messageKey);
        } else {
            return new ErrorDO(message, messageKey);
        }

    }

    public ErrorDO getError(Long id) {
        if (field != null) {
            if (id != null) {
                return new ErrorDO(field, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (id != null) {
                return new ErrorDO(null, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public ErrorDO getError(String... params) {
        if (nonNull(field)) {
            if (params != null) {
                return new ErrorDO(field, message, messageKey, params);
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (params != null) {
                return new ErrorDO(null, message, messageKey, params);
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
