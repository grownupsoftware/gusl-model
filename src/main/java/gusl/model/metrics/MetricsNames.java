package gusl.model.metrics;

public interface MetricsNames {

    String ROUTER_PUBLISH_REQUESTS = "router.publish.request";
    String ROUTER_CACHE_UPDATE_EVENTS = "router.cache.update.events";
    String ROUTER_CACHE_NOT_SUBSCRIBED_EVENTS = "router.cache.not.subscribed.events";
    String ROUTER_CACHE_UPDATE_SENT = "router.cache.update.sent";

}
