package gusl.model.metrics;

import com.codahale.metrics.Meter;

public class MeterDTO {
    private Long count;
    private double oneMinuteRate;
    private double fiveMinuteRate;
    private double fifteenMinuteRate;
    private double meanRate;

    public MeterDTO() {
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public static MeterDTO create(Meter meter) {
        MeterDTO meterDTO = new MeterDTO();
        meterDTO.setCount(meter.getCount());
        meterDTO.setOneMinuteRate(meter.getOneMinuteRate());
        meterDTO.setFiveMinuteRate(meter.getFiveMinuteRate());
        meterDTO.setFifteenMinuteRate(meter.getFifteenMinuteRate());
        meterDTO.setMeanRate(meter.getMeanRate());
        return meterDTO;
    }

    public void setOneMinuteRate(double oneMinuteRate) {
        this.oneMinuteRate = oneMinuteRate;
    }

    public double getOneMinuteRate() {
        return oneMinuteRate;
    }

    public void setFifteenMinuteRate(double fifteenMinuteRate) {
        this.fifteenMinuteRate = fifteenMinuteRate;
    }

    public double getFifteenMinuteRate() {
        return fifteenMinuteRate;
    }

    public void setMeanRate(double meanRate) {
        this.meanRate = meanRate;
    }

    public double getMeanRate() {
        return meanRate;
    }

    public double getFiveMinuteRate() {
        return fiveMinuteRate;
    }

    public void setFiveMinuteRate(double fiveMinuteRate) {
        this.fiveMinuteRate = fiveMinuteRate;
    }
}
