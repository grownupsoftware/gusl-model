package gusl.model.dao;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.exceptions.GUSLErrorException;
import gusl.query.QueryParams;
import gusl.query.UpdateParams;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * @author dhudson
 * @since 10/02/2022
 */
public interface DAOInterface<T> {

    List<T> getAll() throws GUSLErrorException;

    PagedDAOResponse<T> getAllPaged(QueryParams request) throws GUSLErrorException;

    List<T> get(QueryParams params) throws GUSLErrorException;

    Optional<T> getOne(QueryParams params) throws GUSLErrorException;

    Optional<T> findRecord(T entity) throws GUSLErrorException;

    <T> T findRecord(String id) throws GUSLErrorException;

    <VIEW> List<VIEW> getView(Class<VIEW> clazz, QueryParams params) throws GUSLErrorException;

    T insert(T entity) throws GUSLErrorException;

    List<T> insertBatch(List<T> list) throws GUSLErrorException;

    T update(T entity) throws GUSLErrorException;

    T update(T entity, UpdateParams params) throws GUSLErrorException;

    void updateBatch(List<T> list) throws GUSLErrorException;

    void delete(T entity) throws GUSLErrorException;

    void deleteBatch(List<T> list) throws GUSLErrorException;

    int deleteBatch(QueryParams params) throws GUSLErrorException;

    int loadResource(String resource) throws GUSLErrorException;

    int loadFile(File file) throws GUSLErrorException;
}
