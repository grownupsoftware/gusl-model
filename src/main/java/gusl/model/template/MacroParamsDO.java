package gusl.model.template;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToStringCount;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MacroParamsDO {

    @ToStringCount
    @Singular
    @UiField(type = FieldType.nested_table, canAdd = "true")
    private List<MacroParamDO> params;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
