package gusl.model.template;

public enum TemplateStatus {

    ACTIVE("ACTIVE"),
    IN_ACTIVE("IN_ACTIVE");

    private final String name;

    TemplateStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean canLookup() {
        return this == ACTIVE;
    }

    public boolean isActive() {
        return this == ACTIVE;
    }
}
