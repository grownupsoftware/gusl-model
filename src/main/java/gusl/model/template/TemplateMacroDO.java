package gusl.model.template;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.model.Identifiable;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class TemplateMacroDO implements TemplateMacro, Identifiable<String>, WithConfirmation {

    @DocField(description = "DB evolutions version")
    public static final int VERSION = 2;

    @DocField(description = "Unique id")
    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String id;

    @DocField(description = "Cohort Id")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cohortId;

    @DocField(description = "Code or description of template")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String code;

    @ToStringIgnore
    @DocField(description = "HTML template")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String template;

    @DocField(description = "")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String currentGroup;

    @DocField(description = "Additional static macro params")
    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private MacroParamsDO params;

    @DocField(description = "Status")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private TemplateStatus status;

    @DocField(description = "Date created")
    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    private ZonedDateTime dateCreated;

    @DocField(description = "Date last updated")
    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    private ZonedDateTime dateUpdated;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
