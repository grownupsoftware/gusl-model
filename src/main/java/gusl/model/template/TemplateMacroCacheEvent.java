package gusl.model.template;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class TemplateMacroCacheEvent extends AbstractCacheActionEvent<TemplateMacroDO> {

    public TemplateMacroCacheEvent() {
    }

    public TemplateMacroCacheEvent(CacheAction action, TemplateMacroDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.APP;
    }
}
