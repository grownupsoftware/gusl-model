package gusl.model.template;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TemplateMacrosDO {

    @Singular
    private List<TemplateMacroDO> templates;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
