package gusl.model.template;

import gusl.model.Identifiable;

public interface TemplateMacro extends Identifiable<String> {


    String getId();

    String getTemplate();

    TemplateStatus getStatus();

    String getCode();

    MacroParamsDO getParams();

}
