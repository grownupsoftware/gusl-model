package gusl.model.template;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class MacroParamDO {
    @UiField(type = FieldType.text)
    private String param;

    @UiField(type = FieldType.text, label = "Default Value")
    private String value;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
