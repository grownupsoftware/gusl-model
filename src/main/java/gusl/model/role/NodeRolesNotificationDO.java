package gusl.model.role;

import gusl.core.tostring.ToString;
import gusl.model.Identifiable;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NodeRolesNotificationDO implements Identifiable<String> {

    private String nodeType;

    List<RoleGrouping> roleGroups;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getId() {
        return nodeType;
    }

    @Override
    public void setId(String nodeType) {
        this.nodeType = nodeType;
    }
}
