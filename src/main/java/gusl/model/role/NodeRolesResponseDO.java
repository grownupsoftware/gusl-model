package gusl.model.role;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NodeRolesResponseDO {

    private List<NodeRolesNotificationDO> nodeRoles;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
