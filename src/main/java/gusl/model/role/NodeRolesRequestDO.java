package gusl.model.role;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NodeRolesRequestDO {
    private String nodeType;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
