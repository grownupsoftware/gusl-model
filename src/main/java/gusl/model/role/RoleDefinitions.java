package gusl.model.role;

public interface RoleDefinitions {
    RoleGrouping getRoles();
}
