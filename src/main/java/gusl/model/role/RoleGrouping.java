package gusl.model.role;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RoleGrouping {
    private String name;
    private String[] roles;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
