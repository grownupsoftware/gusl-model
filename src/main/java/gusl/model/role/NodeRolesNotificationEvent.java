package gusl.model.role;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NodeRolesNotificationEvent {

    private NodeRolesNotificationDO content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
