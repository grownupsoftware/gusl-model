package gusl.model.fix;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

/**
 * @author dhudson
 * @since 19/07/2023
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class FixMessagesDO {

    private List<FixMessageDO> messages;

    public String toString() {
        return ToString.toString(this);
    }
}
