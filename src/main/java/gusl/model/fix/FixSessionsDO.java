package gusl.model.fix;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

/**
 * @author dhudson
 * @since 14/07/2023
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class FixSessionsDO {

    private List<FixSessionDO> sessions;

    public String toString() {
        return ToString.toString(this);
    }
}
