package gusl.model.fix;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

/**
 * @author dhudson
 * @since 19/07/2023
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FixMessageDO {

    public static final int VERSION = 2;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true)
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    private String sessionQualifier;

    @PostgresField(type = PostgresFieldType.INTEGER)
    private Integer msgSeqNum;

    @PostgresField(type = PostgresFieldType.TEXT)
    private String messageType;

    @PostgresField(type = PostgresFieldType.TEXT)
    private String message;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    private ZonedDateTime dateCreated;

    public String toString() {
        return ToString.toString(this);
    }
}
