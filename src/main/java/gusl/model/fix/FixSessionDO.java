package gusl.model.fix;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * @author dhudson
 * @since 14/07/2023
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class FixSessionDO {

    public static final int VERSION = 1;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true)
    private String sessionQualifier;
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private LocalDateTime creationTime;
    @PostgresField(type = PostgresFieldType.TIMESTAMP)
    private LocalDateTime lastHeartBeat;
    @PostgresField(type = PostgresFieldType.INTEGER)
    private Integer incomingSeqNum;
    @PostgresField(type = PostgresFieldType.INTEGER)
    private Integer outgoingSeqNum;
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    private ZonedDateTime dateUpdated;

    public String toString() {
        return ToString.toString(this);
    }
}
