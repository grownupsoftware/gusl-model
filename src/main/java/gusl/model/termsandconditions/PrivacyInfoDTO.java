package gusl.model.termsandconditions;

import gusl.core.annotations.DocField;

public class PrivacyInfoDTO {

    @DocField(description = "The T&C's version - a combination of major.minor")
    private String versionNumber;

    @DocField(description = "The actual T&Cs in html format")
    private String content;

}
