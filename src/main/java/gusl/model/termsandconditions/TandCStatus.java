package gusl.model.termsandconditions;

public enum TandCStatus {
    ACTIVE,
    IN_ACTIVE;
}
