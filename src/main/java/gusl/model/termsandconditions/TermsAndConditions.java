package gusl.model.termsandconditions;

import gusl.model.Identifiable;

public interface TermsAndConditions extends Identifiable<String> {

    String getId();

    String getCohortId();

    Integer getMinor();

    Integer getMajor();

    Boolean getSystemDefault();

    String getTermsAndConditionsId();

    String getPrivacyPolicyId();

    String getRulesId();

    TandCStatus getStatus();

    String getFundsPolicyId();

    String getResponsiblePolicyId();

    String getCookiePolicyId();

    String getTermsAndConditionsLabel();

    String getPrivacyPolicyLabel();

    String getFundsPolicyLabel();

    String getRulesLabel();

    String getResponsiblePolicyLabel();

    String getCookiePolicyLabel();

    String getFooterHeader();

    String getFooterText();

    String getFooterButton();

}
