package gusl.model.termsandconditions;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class AllTermsAndConditionsDO {

    @ToStringCount
    private List<TermsAndConditionsDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
