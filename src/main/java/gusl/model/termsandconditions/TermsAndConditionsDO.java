/* Copyright lottomart */
package gusl.model.termsandconditions;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TermsAndConditionsDO implements TermsAndConditions {

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 4;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cohortId;

    @PostgresField(type = PostgresFieldType.INTEGER)
    @ElasticField(type = ESType.KEYWORD)
    private Integer major;

    @PostgresField(type = PostgresFieldType.INTEGER)
    @ElasticField(type = ESType.KEYWORD)
    private Integer minor;

    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean systemDefault;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String termsAndConditionsId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String termsAndConditionsLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String privacyPolicyId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String privacyPolicyLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String fundsPolicyId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String fundsPolicyLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String rulesId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String rulesLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String responsiblePolicyId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String responsiblePolicyLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cookiePolicyId;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cookiePolicyLabel;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String footerHeader;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String footerText;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String footerButton;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private TandCStatus status;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    private ZonedDateTime dateUpdated;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
