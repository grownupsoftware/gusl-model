package gusl.model.termsandconditions;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@DocClass(description = "Container of terms and conditions and version")
public class TermsInfoDTO {

    @DocField(description = "Version")
    private String version;

    @DocField(description = "Privacy policy")
    private String privacy;

    @DocField(description = "Terms and conditions")
    private String terms;

    @DocField(description = "Funds protection policy")
    private String funds;

    @DocField(description = "Associated rules")
    private String rules;

    @DocField(description = "Responsible Policy")
    private String responsiblePolicy;

    @DocField(description = "Cookie Policy")
    private String cookiePolicy;

    private String footerHeader;

    private String footerText;

    private String footerButton;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
