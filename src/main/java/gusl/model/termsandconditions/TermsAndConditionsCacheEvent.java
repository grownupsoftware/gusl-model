/* Copyright lottomart */
package gusl.model.termsandconditions;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class TermsAndConditionsCacheEvent extends AbstractCacheActionEvent<TermsAndConditionsDO> {

    public TermsAndConditionsCacheEvent() {
    }

    public TermsAndConditionsCacheEvent(CacheAction action, TermsAndConditionsDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
