package gusl.model;

import java.util.Date;

public class DOUtils {
    private DOUtils() {
    }

    public static void audit(Auditable obj) {
        Date now = new Date();
        obj.setDateCreated(now);
        obj.setDateUpdated(now);
    }

    public static void touch(Auditable obj) {
        obj.setDateUpdated(new Date());
    }
}
