package gusl.model;

import java.util.Date;

/**
 * @author dhudson on 03/08/2020
 */
public interface Auditable {

    Date getDateCreated();

    void setDateCreated(Date dateCreated);

    Date getDateUpdated();

    void setDateUpdated(Date dateUpdated);

    void touch();

    void created();

    void created(Date now);
}
