/* Copyright lottomart */
package gusl.model.googlesso;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class GoogleLoginRequestDO {

    @DocField(description = "Unique Google ID for user")
    private String googleId;

    @DocField(description = "Expiry time of the google token")
    private Date expiresAt;

    @DocField(description = "Name of Google user")
    private String name;

    @DocField(description = "Current Google generated token")
    private String token;

    @DocField(description = "Email of Google User")
    private String email;

    @DocField(description = "Avatar of Google User")
    private String avatar;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
