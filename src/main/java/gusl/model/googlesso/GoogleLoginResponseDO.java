/* Copyright lottomart */
package gusl.model.googlesso;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@DocClass(description = "Admin Login Response")
public class GoogleLoginResponseDO {

    @DocField(description = "The Session Token")
    private String sessionToken;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
