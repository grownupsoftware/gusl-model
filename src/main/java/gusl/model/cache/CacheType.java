package gusl.model.cache;

/**
 *
 * @author dhudson
 */
public enum CacheType {

    /**
     * Changes in frequently, broadcast to all.
     */
    STATIC,
    /**
     * Only send to subscribed consumers.
     */
    SUBSCRIPTION

}
