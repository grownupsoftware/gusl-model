package gusl.model.cache;

import gusl.core.tostring.ToString;

/**
 * These events get fired when a cache needs to take some action.
 *
 * @author dhudson
 * @param <DO> The DO in question.
 */
public abstract class AbstractCacheActionEvent<DO> {

    private CacheAction cacheAction;
    private DO dataObject;

    public AbstractCacheActionEvent() {
    }

    public AbstractCacheActionEvent(CacheAction action, DO data) {
        cacheAction = action;
        dataObject = data;
    }

    public CacheAction getCacheAction() {
        return cacheAction;
    }

    public void setCacheAction(CacheAction cacheAction) {
        this.cacheAction = cacheAction;
    }

    public DO getDataObject() {
        return dataObject;
    }

    public void setDataObject(DO dataObject) {
        this.dataObject = dataObject;
    }

    public CacheType getCacheType() {
        return CacheType.SUBSCRIPTION;
    }

    public abstract CacheGroup getCacheGroup();

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
