package gusl.model.cache;

/**
 * Cache Action.
 *
 * @author dhudson
 */
public enum CacheAction {
    // Populate and clear are local events only and the data will be null
    POPULATE,
    CLEAR,
    // These will be distributed from remote nodes
    ADD,
    UPDATE,
    REMOVE,
    DELETE,
    // Delta messaging
    DELTA,
    // Dump
    DUMP,
    CACHE_SIZE
}
