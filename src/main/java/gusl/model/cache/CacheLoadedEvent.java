/* Copyright lottomart */
package gusl.model.cache;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author grant
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CacheLoadedEvent {
    private boolean success;

}
