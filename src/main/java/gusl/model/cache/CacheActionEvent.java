package gusl.model.cache;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author grantwallace
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CacheActionEvent {

    private CacheAction cacheAction;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
