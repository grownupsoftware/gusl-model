package gusl.model.cache;

/**
 * @author dhudson
 */
public enum CacheGroup {

    APP,
    PROMO,
    PLAYER,
    SYSTEM,
    CART,
    LOTTO,
    GAMES,
    SPORT,
    LEDGER

}
