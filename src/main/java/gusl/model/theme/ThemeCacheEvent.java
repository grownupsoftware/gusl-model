package gusl.model.theme;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class ThemeCacheEvent extends AbstractCacheActionEvent<ThemeDO> {

    public ThemeCacheEvent() {
    }

    public ThemeCacheEvent(CacheAction action, ThemeDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
    
