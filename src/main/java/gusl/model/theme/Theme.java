package gusl.model.theme;

import gusl.model.Identifiable;
import gusl.model.theme.model.ThemeDefinitionDO;

import java.time.ZonedDateTime;

public interface Theme extends Identifiable<String> {

    String getId();

    String getName();

    ThemeDefinitionDO getDefinition();

    ZonedDateTime getDateCreated();

    ZonedDateTime getDateUpdated();

    ThemeStatus getStatus();

}
