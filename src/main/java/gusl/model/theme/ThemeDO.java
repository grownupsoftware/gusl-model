package gusl.model.theme;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.model.theme.model.ThemeDefinitionDO;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class ThemeDO implements Theme{

    @DocField(description = "Database version - used by Evolutions migrate tool")
    public static final int VERSION = 1;

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    @DocField(description = "Unique id, 1-to-1 with cohort so cohortId = id")
    private String id;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Name of theme")
    private String name;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    @DocField(description = "Theme definition")
    private ThemeDefinitionDO definition;

    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    @DocField(description = "Date created")
    private ZonedDateTime dateCreated;

    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    @DocField(description = "Date last modified")
    private ZonedDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    @DocField(description = "Theme status")
    private ThemeStatus status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
