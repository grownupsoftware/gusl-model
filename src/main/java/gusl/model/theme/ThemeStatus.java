package gusl.model.theme;

public enum ThemeStatus {
    ACTIVE,
    IN_ACTIVE
}
