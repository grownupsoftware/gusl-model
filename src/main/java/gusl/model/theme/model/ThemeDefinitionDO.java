package gusl.model.theme.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.tostring.ToString;
import gusl.model.theme.model.parts.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ThemeDefinitionDO {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(ThemeDefinitionDO.class);

    public static final String THEME_COLLECTION = "theme_collection";

    public static final String COLOURS = "colours";
    public static final String COLOUR = "colour";
    public static final String CHART_COLOURS = "chart_colours";
    public static final String PIXEL = "pixel";

    public static final String PADDING = "padding";

    public static final String BORDER = "border";

    public static final String FONT_SIZE = "font_size";
    public static final String FONT_FAMILY = "font_family";

    public static final String FONT_WEIGHT = "font_weight";

    public static final String SIZE_INTEGER = "size_integer";

    @UiField(type = FieldType.text)
    private String imageSuffix;

    @UiField(type = THEME_COLLECTION)
    private ThemeColorsDO colors;

    @UiField(type = THEME_COLLECTION)
    private ThemeBodyDO body;

    @UiField(type = THEME_COLLECTION)
    private ThemeSplashDO splash;

    @UiField(type = THEME_COLLECTION)
    private ThemeSpinnerDO spinner;

    @UiField(type = THEME_COLLECTION)
    private ThemeModalDO modal;

    @UiField(type = THEME_COLLECTION)
    private ThemeLoginDO login;

    @UiField(type = THEME_COLLECTION)
    private ThemeFilterControlsDO filterControls;

    @UiField(type = THEME_COLLECTION)
    private ThemeTableDO table;

    @UiField(type = THEME_COLLECTION)
    private ThemePanelDO panel;

    @UiField(type = THEME_COLLECTION)
    private ThemeFormDO form;

    @UiField(type = THEME_COLLECTION)
    private ThemeActionDO action;

    @UiField(type = THEME_COLLECTION)
    private ThemeLookupDO lookup;

    @UiField(type = THEME_COLLECTION)
    private ThemeMoneyDO money;

    @UiField(type = THEME_COLLECTION)
    private ThemeNavigationDO navigation;

    @UiField(type = THEME_COLLECTION)
    private List<ThemeChartDO> chart;

    @UiField(type = THEME_COLLECTION)
    private ThemePaddingDO padding;

    @UiField(type = THEME_COLLECTION)
    private ThemeMarginDO margin;

    @UiField(type = THEME_COLLECTION)
    private ThemeBorderRadiusDO borderRadius;

    @UiField(type = THEME_COLLECTION)
    private ThemeGridDO grid;

    @UiField(type = PIXEL)
    private String maxContentWidth;

    @UiField(type = PIXEL)
    private String maxPageWidth;

    @UiField(type = THEME_COLLECTION)
    private ThemeMediaQueryBreakPointsDO mediaQueryBreakPoints;

    @UiField(type = THEME_COLLECTION)
    private ThemeTopographyDO typography;

    @UiField(type = THEME_COLLECTION)
    private ThemeContentPaddingDO contentPadding;

    @UiField(type = THEME_COLLECTION)
    private ThemeFontSizeDO fontSizes;

    @UiField(type = THEME_COLLECTION)
    private ThemeFontWeightsDO fontWeights;

    @UiField(type = THEME_COLLECTION)
    private ThemeMarginMapDO marginMap;

    @UiField(type = THEME_COLLECTION)
    private ThemeHeaderPanelDO headerPanel;

    public ThemeDefinitionDO() {
    }

    public ThemeDefinitionDO(String jsonString) {
        try {
            final ThemeDefinitionDO definition = ObjectMapperFactory.getDefaultObjectMapper().readValue(jsonString, ThemeDefinitionDO.class);
            colors = definition.colors;
            body = definition.body;
            splash = definition.splash;
            spinner = definition.spinner;
            modal = definition.modal;
            login = definition.login;
            table = definition.table;
            panel = definition.panel;
            form = definition.form;
            action = definition.action;
            lookup = definition.lookup;
            money = definition.money;
            navigation = definition.navigation;
            filterControls = definition.filterControls;
            chart = definition.chart;
            padding = definition.padding;
            margin = definition.margin;
            borderRadius = definition.borderRadius;
            grid = definition.grid;
            maxContentWidth = definition.maxContentWidth;
            maxPageWidth = definition.maxPageWidth;
            mediaQueryBreakPoints = definition.mediaQueryBreakPoints;
            typography = definition.typography;
            contentPadding = definition.contentPadding;
            fontSizes = definition.fontSizes;
            fontWeights = definition.fontWeights;
            marginMap = definition.marginMap;
            headerPanel = definition.headerPanel;
            imageSuffix = definition.imageSuffix;
        } catch (JsonProcessingException e) {
            logger.error("Failed to parse theme string as Json", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
