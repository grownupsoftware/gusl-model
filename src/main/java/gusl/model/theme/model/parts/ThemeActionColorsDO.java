package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeActionColorsDO {

    @UiField(type = COLOUR)
    private String defaultColor;

    @UiField(type = COLOUR)
    private String warning;

    @UiField(type = COLOUR)
    private String save;

    @UiField(type = COLOUR)
    private String refresh;

    @UiField(type = COLOUR)
    private String newAction;

    @UiField(type = COLOUR)
    private String update;

    @UiField(type = COLOUR)
    private String edit;

    @UiField(type = COLOUR)
    private String cancel;

    @UiField(type = COLOUR)
    private String delete;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
