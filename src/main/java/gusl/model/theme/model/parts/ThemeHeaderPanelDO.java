package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeHeaderPanelDO {

    @UiField(type = COLOUR)
    private String fgColor;

    @UiField(type = COLOUR)
    private String bgColor;

    @UiField(type = BORDER)
    private String border;

    @UiField(type = PIXEL)
    private String borderRadius;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
