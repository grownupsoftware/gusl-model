package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.THEME_COLLECTION;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeSideMenuHeaderDO {

    @UiField(type = COLOUR)
    private String bgColor;

    @UiField(type = COLOUR)
    private String color;

    @UiField(type = THEME_COLLECTION)
    private ThemeAvatarDO avatar;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
