package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.FONT_SIZE;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeSubPanelDO {
    @UiField(type = COLOUR)
    private String titleColor;

    @UiField(type = COLOUR)
    private String titleBgColor;

    @UiField(type = FONT_SIZE)
    private String titleFontSize;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
