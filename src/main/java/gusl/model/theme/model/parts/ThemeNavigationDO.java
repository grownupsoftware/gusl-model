package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

import static gusl.model.theme.model.ThemeDefinitionDO.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeNavigationDO {
    @UiField(type = COLOUR)
    private String topNavbarColor;

    @UiField(type = COLOUR)
    private String topNavbarBgColor;

    @UiField(type = COLOURS)
    private List<String> gradient;

    @UiField(type = COLOUR)
    private String buttonColor;

    @UiField(type = COLOUR)
    private String buttonBgColor;

    @UiField(type = COLOUR)
    private String positiveColor;

    @UiField(type = COLOUR)
    private String hoverButtonColor;

    @UiField(type = COLOUR)
    private String hoverButtonBgColor;

    @UiField(type = COLOUR)
    private String hamburgerColor;

    @UiField(type = COLOUR)
    private String borderColor;

    @UiField(type = THEME_COLLECTION)
    private ThemeDropdownMenuDO dropdownMenu;

    @UiField(type = THEME_COLLECTION)
    private ThemeSideMenuDO sideMenu;

    @UiField(type = CHART_COLOURS)
    private List<ThemeChartColorDO> chart;


    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
