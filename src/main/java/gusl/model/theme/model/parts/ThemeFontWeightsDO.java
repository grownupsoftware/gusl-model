package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.FONT_WEIGHT;
import static gusl.model.theme.model.ThemeDefinitionDO.SIZE_INTEGER;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeFontWeightsDO {

    @UiField(type = FONT_WEIGHT)
    private String bold;

    @UiField(type = SIZE_INTEGER)
    private Integer medium;

    @UiField(type = SIZE_INTEGER)
    private Integer regular;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
