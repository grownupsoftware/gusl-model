package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeTableDO {

    @UiField(type = COLOUR)
    private String panelBgColor;

    @UiField(type = COLOUR)
    private String titleColor;

    @UiField(type = COLOUR)
    private String titleBgColor;

    @UiField(type = PADDING)
    private String titlePadding;

    @UiField(type = FONT_SIZE)
    private String titleFontSize;

    @UiField(type = COLOUR)
    private String headerBgColor;

    @UiField(type = COLOUR)
    private String headerColor;

    @UiField(type = COLOUR)
    private String columnColor;

    @UiField(type = BORDER)
    private String gridBorder;

    @UiField(type = COLOUR)
    private String selectedColor;

    @UiField(type = FONT_SIZE)
    private String headerFontSize;

    @UiField(type = FONT_SIZE)
    private String columnFontSize;

    @UiField(type = COLOUR)
    private String menuButtonColor;

    @UiField(type = COLOUR)
    private String menuButtonBgColor;

    @UiField(type = COLOUR)
    private String menuButtonHoverColor;

    @UiField(type = COLOUR)
    private String menuButtonHoverBgColor;

    @UiField(type = COLOUR)
    private String rowHoverBgColor;

    @UiField(type = THEME_COLLECTION)
    private ThemeHeaderDO summaryHeader;

    @UiField(type = THEME_COLLECTION)
    private ThemeFooterDO summaryFooter;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
