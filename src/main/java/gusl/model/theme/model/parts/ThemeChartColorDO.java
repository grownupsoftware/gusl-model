package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.COLOURS;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeChartColorDO {

    @UiField(type = COLOUR)
    private String borderColor;

    @UiField(type = COLOURS)
    private List<String> backgroundColor;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
