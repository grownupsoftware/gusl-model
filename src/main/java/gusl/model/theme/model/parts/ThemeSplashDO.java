package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeSplashDO {

    @UiField(type = COLOUR)
    private String background;

    @UiField(type = COLOUR)
    private String color;

    @UiField(type = SIZE_INTEGER)
    private Integer size;

    @UiField(type = PIXEL)
    private String left;

    @UiField(type = PIXEL)
    private String top;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
