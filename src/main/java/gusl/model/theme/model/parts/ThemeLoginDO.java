package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeLoginDO {

    @UiField(type = COLOUR)
    private String fontColor;

    @UiField(type = COLOUR)
    private String linkColor;

    @UiField(type = COLOUR)
    private String buttonBgColor;

    @UiField(type = COLOUR)
    private String buttonFgColor;

    @UiField(type = COLOUR)
    private String outerBackground;

    @UiField(type = COLOUR)
    private String innerBackground;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
