package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeSideMenuFooterDO {

    @UiField(type = COLOUR)
    private String bgColor;

    @UiField(type = COLOUR)
    private String color;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
