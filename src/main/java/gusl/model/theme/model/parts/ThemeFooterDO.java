package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeFooterDO {

    @UiField(type = COLOUR)
    private String bgColor;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
