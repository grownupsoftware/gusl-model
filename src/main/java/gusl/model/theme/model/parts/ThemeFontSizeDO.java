package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.PIXEL;
import static gusl.model.theme.model.ThemeDefinitionDO.THEME_COLLECTION;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeFontSizeDO {

    @UiField(type = THEME_COLLECTION)
    private ThemeFontSizeHeadingsDO headings;

    @UiField(type = THEME_COLLECTION)
    private ThemeFontSizeBodyDO body;

    @UiField(type = PIXEL)
    private String tooltip;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
