package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.PIXEL;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemePaddingDO {

    @UiField(type = PIXEL)
    private String xs;

    @UiField(type = PIXEL)
    private String s;

    @UiField(type = PIXEL)
    private String m;

    @UiField(type = PIXEL)
    private String l;

    @UiField(type = PIXEL)
    private String xl;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
