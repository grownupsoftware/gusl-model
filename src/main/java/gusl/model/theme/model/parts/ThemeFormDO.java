package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.BORDER;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeFormDO {

    @UiField(type = BORDER)
    private String fieldBorder;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
