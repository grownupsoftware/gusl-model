package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.SIZE_INTEGER;
import static gusl.model.theme.model.ThemeDefinitionDO.THEME_COLLECTION;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeGridDO {

    @UiField(type = SIZE_INTEGER)
    private Integer baseline;

    @UiField(type = THEME_COLLECTION)
    private ThemeGapDO gap;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
