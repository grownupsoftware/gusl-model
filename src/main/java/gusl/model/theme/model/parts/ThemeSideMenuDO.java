package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.THEME_COLLECTION;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeSideMenuDO {

    @UiField(type = COLOUR)
    private String bgColor;

    @UiField(type = COLOUR)
    private String color;

    @UiField(type = COLOUR)
    private String hoverBgColor;

    @UiField(type = COLOUR)
    private String hoverColor;

    @UiField(type = COLOUR)
    private String rightBorderColor;

    @UiField(type = THEME_COLLECTION)
    private ThemeSideMenuFooterDO footer;

    @UiField(type = THEME_COLLECTION)
    private ThemeSideMenuHeaderDO header;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
