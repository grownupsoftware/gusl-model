package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.FONT_FAMILY;
import static gusl.model.theme.model.ThemeDefinitionDO.FONT_SIZE;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeTopographyDO {

    @UiField(type = FONT_FAMILY)
    private String fontFamilyRegular;

    @UiField(type = FONT_FAMILY)
    private String fontFamilySemiBold;

    @UiField(type = FONT_SIZE)
    private String baseFontSize;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
