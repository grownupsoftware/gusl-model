package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.FONT_SIZE;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeLookupDO {

    @UiField(type = COLOUR)
    private String headerColor;

    @UiField(type = COLOUR)
    private String columnColor;

    @UiField(type = FONT_SIZE)
    private String headerFontSize;

    @UiField(type = FONT_SIZE)
    private String columnFontSize;

    @UiField(type = COLOUR)
    private String hoverColor;

    @UiField(type = COLOUR)
    private String hoverBgColor;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
