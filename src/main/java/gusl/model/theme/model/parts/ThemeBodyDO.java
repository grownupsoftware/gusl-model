package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeBodyDO {
    @UiField(type = COLOUR)
    private String background;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
