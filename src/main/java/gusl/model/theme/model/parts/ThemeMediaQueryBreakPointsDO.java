package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.SIZE_INTEGER;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeMediaQueryBreakPointsDO {
    @UiField(type = SIZE_INTEGER)
    private Integer xs;

    @UiField(type = SIZE_INTEGER)
    private Integer sm;

    @UiField(type = SIZE_INTEGER)
    private Integer md;

    @UiField(type = SIZE_INTEGER)
    private Integer lg;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
