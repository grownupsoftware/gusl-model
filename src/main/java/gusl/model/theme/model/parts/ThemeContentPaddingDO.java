package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.PIXEL;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeContentPaddingDO {

    @UiField(type = PIXEL)
    private String desktop;

    @UiField(type = PIXEL)
    private String mobile;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
