package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import static gusl.model.theme.model.ThemeDefinitionDO.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemePanelDO {

    @UiField(type = COLOUR)
    private String panelBgColor;

    @UiField(type = COLOUR)
    private String titleColor;

    @UiField(type = COLOUR)
    private String titleBgColor;

    @UiField(type = FONT_SIZE)
    private String titleFontSize;

    @UiField(type = THEME_COLLECTION)
    private ThemeSubPanelDO subPanel;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
