package gusl.model.theme.model.parts;

import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

import static gusl.model.theme.model.ThemeDefinitionDO.COLOUR;
import static gusl.model.theme.model.ThemeDefinitionDO.COLOURS;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThemeColorsDO {
    @UiField(type = COLOURS)
    private List<String> primary;

    @UiField(type = COLOURS)
    private List<String> secondary;

    @UiField(type = COLOURS)
    private List<String> background;

    @UiField(type = COLOURS)
    private List<String> dark;

    @UiField(type = COLOURS)
    private List<String> bespoke;

    @UiField(type = COLOUR)
    private String light;

    @UiField(type = COLOUR)
    private String error;

    @UiField(type = COLOUR)
    private String warning;

    @UiField(type = COLOUR)
    private String notification;

    @UiField(type = COLOUR)
    private String accentColor;

    @UiField(type = COLOUR)
    private String accentColorLight;

    @UiField(type = COLOUR)
    private String accentColorMuted;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
