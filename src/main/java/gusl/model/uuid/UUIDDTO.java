package gusl.model.uuid;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UUIDDTO {

    @ToStringMask
    private String uuid;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
