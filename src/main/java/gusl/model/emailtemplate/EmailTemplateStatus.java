package gusl.model.emailtemplate;

public enum EmailTemplateStatus {
    ACTIVE,
    IN_ACTIVE
}
