package gusl.model.emailtemplate;

import gusl.model.Identifiable;

import java.time.ZonedDateTime;

public interface EmailTemplate extends Identifiable<String> {

    String getId();

    String getTemplateMacroId();

    String getSubject();

    TemplateType getType();

    EmailTemplateStatus getStatus();

    ZonedDateTime getDateCreated();

    ZonedDateTime getDateUpdated();

}
