package gusl.model.emailtemplate;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class EmailTemplateCacheEvent extends AbstractCacheActionEvent<EmailTemplateDO> {

    public EmailTemplateCacheEvent() {
    }

    public EmailTemplateCacheEvent(CacheAction action, EmailTemplateDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.APP;
    }
}
