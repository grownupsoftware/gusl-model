package gusl.model.emailtemplate;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.elastic.model.WithConfirmation;
import gusl.model.Identifiable;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmailTemplateDO implements EmailTemplate, Identifiable<String>, WithConfirmation {

    @DocField(description = "DB evolutions version")
    public static final int VERSION = 4;

    @DocField(description = "Cohort Id")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String cohortId;

    @DocField(description = "Unique id")
    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true, populate = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String id;

    @DocField(description = "Unique Id of template macro")
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private String templateMacroId;

    @DocField(description = "Email Subject")
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private String subject;

    @DocField(description = "Email Template type")
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private TemplateType type;

    @DocField(description = "Status")
    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private EmailTemplateStatus status;

    @DocField(description = "Date created")
    @ElasticField(type = ESType.DATE, dateCreated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, populate = true)
    private ZonedDateTime dateCreated;

    @DocField(description = "Date last updated")
    @ElasticField(type = ESType.DATE, dateUpdated = true, populate = true)
    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, populate = true)
    private ZonedDateTime dateUpdated;

    @DocField(description = "From (internal addr) Email")
    @PostgresField(type = PostgresFieldType.TEXT) // ideally , notNull = true - next release
    @ElasticField(type = ESType.KEYWORD)
    private String fromEmail;

    @DocField(description = "From (internal addr) Email")
    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    public CCEmailsDO ccEmails;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
