package gusl.model.emailtemplate;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CCEmailBO {

    @UiField(type = FieldType.text, sortable = false)
    private String email;

    @UiField(type = FieldType.text, sortable = false)
    private String test1;

    @UiField(type = FieldType.text, sortable = false)
    private String test2;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
