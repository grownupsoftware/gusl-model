package gusl.model.emailtemplate;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmailTemplatesDO {

    @Singular
    private List<EmailTemplateDO> emailtemplates;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
