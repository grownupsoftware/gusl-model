package gusl.model.emailtemplate;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CCEmailsDO {

    @Singular
    private List<String> emails;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
