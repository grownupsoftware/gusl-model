package gusl.model.comment;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentsDO {

    @ToStringCount
    @Singular
    private List<CommentDO> comments;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
