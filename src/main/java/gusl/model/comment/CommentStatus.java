package gusl.model.comment;

public enum CommentStatus {
    ACTIVE,
    DELETED,
    EDIT_REMOVE
}
