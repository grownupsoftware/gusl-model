package gusl.model.comment;

import gusl.core.tostring.ToString;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentDO {

    private String id;

    private String parentId;

    private String username;

    private String comment;

    private CommentStatus status;

    private ZonedDateTime dateCreated;

    private ZonedDateTime dateRemoved;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
