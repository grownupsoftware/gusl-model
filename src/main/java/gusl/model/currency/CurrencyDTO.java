package gusl.model.currency;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class CurrencyDTO {

    private String code;

    private String name;

    private String symbol;

    private Integer decimalPlaces;

    private BigDecimal exchangeRate;

    private Date lastUpdateTime;

    private Boolean hideInRegistration;

    private Boolean systemBaseCurrency;

    private String unescapedSymbol;

    private Boolean isCrypto;

    private Date dateCreated;

    private Date dateUpdated;

    private Integer displayPlaces;

}
