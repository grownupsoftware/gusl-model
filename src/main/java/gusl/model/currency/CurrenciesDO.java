package gusl.model.currency;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

import gusl.core.tostring.ToString;
import gusl.model.AuditableDO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class CurrenciesDO {

    @ToStringCount
    @Singular
    private List<CurrencyDO> currencies;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
