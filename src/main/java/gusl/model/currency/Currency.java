package gusl.model.currency;

import gusl.model.Identifiable;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface Currency extends Identifiable<String> {

    String getName();

    Integer getDecimalPlaces();

    Integer getDisplayPlaces();

    BigDecimal getExchangeRate();

    Boolean getHideInRegistration();

    Boolean getSystemBaseCurrency();

    String getUnescapedSymbol();

    Boolean getIsCrypto();

    String getSymbol();

    String getCode();

    LocalDateTime getDateCreated();

    LocalDateTime getDateUpdated();
}
