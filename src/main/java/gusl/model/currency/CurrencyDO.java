package gusl.model.currency;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CurrencyDO implements Currency {

    private String code;

    private String name;

    private String symbol;

    private Integer decimalPlaces;

    private BigDecimal exchangeRate;

    private Boolean hideInRegistration;

    private Boolean systemBaseCurrency;

    private String unescapedSymbol;

    private Boolean isCrypto;

    private LocalDateTime dateCreated;

    private LocalDateTime dateUpdated;

    private Integer displayPlaces;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getId() {
        return code;
    }

    @Override
    public void setId(String code) {
        this.code = code;
    }
}
