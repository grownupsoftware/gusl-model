package gusl.model.currency;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class CurrencyCacheEvent extends AbstractCacheActionEvent<CurrencyDO> {

    public CurrencyCacheEvent() {
    }

    public CurrencyCacheEvent(CacheAction action, CurrencyDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
