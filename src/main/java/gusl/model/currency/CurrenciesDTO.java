package gusl.model.currency;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class CurrenciesDTO {
    @ToStringCount
    private List<CurrencyDTO> currencies;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
