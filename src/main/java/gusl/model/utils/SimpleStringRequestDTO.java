package gusl.model.utils;

import lombok.*;

/**
 * @author dhudson
 * 27/10/2020:10:44
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimpleStringRequestDTO {

    private String id;

    @Override
    public String toString() {
        return id;
    }
}
