package gusl.model.odds;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.annotations.form.EnumFilter;
import gusl.core.tostring.ToString;
import gusl.model.decimal.DecimalDO;
import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OddsDO implements EnumFilter {
    private OddsType type;
    private BigDecimal value;

    @JsonIgnore
    public Odds asOdds() {
        return new Odds(type, value, true);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public static OddsDO of(OddsType type, BigDecimal value) {
        return OddsDO.builder().type(type).value(value).build();
    }

    public String toPlainString() {
        return DecimalDO.of(value).toPlainString(true, true);
    }

    @JsonIgnore
    // used in BO form generation
    public static String[] getFilteredForBackOffice() {
        return OddsType.getOddsTypes();
    }
}
