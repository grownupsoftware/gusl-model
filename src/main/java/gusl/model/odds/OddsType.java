package gusl.model.odds;

import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.nonNull;

public enum OddsType {
    US("US"),
    D("D"),
    P("P"),
    HK("HK"),
    M("M"),
    I("I");

    private final String name;

    OddsType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String[] getOddsTypes() {
        return safeStream(OddsType.values()).map(type -> type.getName()).collect(Collectors.toList()).toArray(String[]::new);
    }

    public static OddsType get(String name) {
        final OddsType oddsType = safeStream(OddsType.values()).filter(type -> type.getName().equals(name)).findFirst().orElse(null);

        if (nonNull(oddsType)) {
            return oddsType;
        }

        return safeStream(OddsType.values()).filter(type -> type.name().equals(name)).findFirst().orElse(null);
    }
}
