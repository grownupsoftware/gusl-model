package gusl.model.odds;

import gusl.core.tostring.ToString;
import gusl.model.money.MoneyDO;
import lombok.*;

@Builder
@NoArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor(staticName = "of")
public class RiskGainDO {

    @NonNull
    private OddsDO odds;

    @NonNull
    private MoneyDO risk;

    @NonNull
    private MoneyDO gain;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
