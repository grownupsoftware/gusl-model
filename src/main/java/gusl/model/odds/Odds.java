package gusl.model.odds;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * <b>Title</b> Odds
 * </p>
 * <p>
 * <b>Description</b> Encapsulation of an odds value and associated type.<br/>
 * This class automatically normalises the odds by applying the odds ladder.
 * </p>
 *
 * @author David Mills - 3 Sep 2015
 * @version 1.0
 */
public class Odds implements Comparable<Odds>, Serializable {

    private static final long serialVersionUID = -7494946108413354289L;

    protected static final List<OddsLadderRule> LADDER_RULES = new ArrayList<>();

    static {
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(100), BigDecimal.valueOf(200), BigDecimal.valueOf(1)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(200), BigDecimal.valueOf(224), BigDecimal.valueOf(2)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(224), BigDecimal.valueOf(226), BigDecimal.valueOf(1)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(226), BigDecimal.valueOf(274), BigDecimal.valueOf(2)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(274), BigDecimal.valueOf(276), BigDecimal.valueOf(1)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(276), BigDecimal.valueOf(300), BigDecimal.valueOf(2)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(300), BigDecimal.valueOf(400), BigDecimal.valueOf(5)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(400), BigDecimal.valueOf(660), BigDecimal.valueOf(10)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(660), BigDecimal.valueOf(740), BigDecimal.valueOf(20)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(740), BigDecimal.valueOf(760), BigDecimal.valueOf(10)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(760), BigDecimal.valueOf(840), BigDecimal.valueOf(20)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(840), BigDecimal.valueOf(860), BigDecimal.valueOf(10)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(860), BigDecimal.valueOf(940), BigDecimal.valueOf(20)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(940), BigDecimal.valueOf(960), BigDecimal.valueOf(10)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(960), BigDecimal.valueOf(1000), BigDecimal.valueOf(20)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(1000), BigDecimal.valueOf(2000), BigDecimal.valueOf(50)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(2000), BigDecimal.valueOf(3000), BigDecimal.valueOf(100)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(3000), BigDecimal.valueOf(4000), BigDecimal.valueOf(200)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(4000), BigDecimal.valueOf(7000), BigDecimal.valueOf(500)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(7000), BigDecimal.valueOf(20000), BigDecimal.valueOf(1000)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(20000), BigDecimal.valueOf(50000), BigDecimal.valueOf(2000)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(50000), BigDecimal.valueOf(95000), BigDecimal.valueOf(5000)));
        LADDER_RULES.add(new OddsLadderRule(BigDecimal.valueOf(95000), BigDecimal.valueOf(99999), BigDecimal.valueOf(4999)));
    }

    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
    private static final BigDecimal MINUS_ONE_HUNDRED = BigDecimal.valueOf(-100);

    private final OddsType type;
    private final BigDecimal value;
    private final Integer usOddsValue;

    /**
     * Needed for Jackson unmarshalling
     */
    @SuppressWarnings("unused")
    private Odds() {
        type = null;
        value = null;
        usOddsValue = null;
    }

    /**
     * Constructor.
     *
     * @param type
     * @param value   The value in the desired type.
     * @param roundUp
     */
    public Odds(OddsType type, BigDecimal value, boolean roundUp) {
        this(type, value, roundUp, true);
    }

    /**
     * Constructor.
     *
     * @param type
     * @param value           The value in the desired type.
     * @param roundUp
     * @param applyOddsLadder
     */
    public Odds(OddsType type, BigDecimal value, boolean roundUp, boolean applyOddsLadder) {
        this.type = type;
        BigDecimal usOdds = OddsConverter.convertToUsOdds(value, type, roundUp);
        if (applyOddsLadder) {
            usOdds = applyLadderToUsOdds(usOdds, roundUp);
        } else {
            //Rounding mode modified to half in order to get better adjustments when bypassing odds ladder
            usOdds = usOdds.setScale(0, roundUp ? RoundingMode.HALF_UP : RoundingMode.HALF_DOWN);
        }
        this.value = OddsConverter.convertFromUsOdds(usOdds, type, roundUp);
        this.usOddsValue = usOdds.intValue();
    }

    /**
     * Constructor.
     *
     * @param type
     * @param usOddsValue The value in us odds, to be converted to the desired type.
     * @param roundUp
     */
    public Odds(OddsType type, Integer usOddsValue, boolean roundUp) {
        this(type, usOddsValue, roundUp, true);
    }

    /**
     * Constructor.
     *
     * @param type
     * @param usOddsValue     The value in us odds, to be converted to the desired type.
     * @param roundUp
     * @param applyOddsLadder
     */
    public Odds(OddsType type, Integer usOddsValue, boolean roundUp, boolean applyOddsLadder) {
        this.type = type;
        BigDecimal usOdds = new BigDecimal(usOddsValue);
        if (applyOddsLadder) {
            usOdds = applyLadderToUsOdds(usOdds, roundUp);
        }
        this.value = OddsConverter.convertFromUsOdds(usOdds, type, roundUp);
        this.usOddsValue = usOdds.intValue();
    }

    /**
     * V = absolute initial odds value, F = from, I = increment, O = offset,
     * V1 = rounded odds value
     * <p>
     * O = (V - F) % I
     * Round Up: V1 = V + I - O
     * Round Down: V1 = V - O
     *
     * @param odds
     * @param roundUp
     * @return
     */
    private BigDecimal applyLadderToUsOdds(BigDecimal odds, boolean roundUp) {
        BigDecimal v = odds;
        if ((v.compareTo(MINUS_ONE_HUNDRED) >= 0) && (v.compareTo(ONE_HUNDRED) <= 0)) {
            return ONE_HUNDRED;
        }

        boolean negative = v.compareTo(BigDecimal.ZERO) < 0;
        if (negative) {
            v = v.negate();
        }

        // Get the ladder rule
        OddsLadderRule rule = null;
        for (OddsLadderRule r : LADDER_RULES) {
            if ((v.compareTo(r.getFrom()) >= 0) && (v.compareTo(r.getTo()) <= 0)) { // This rule applies
                rule = r;
                break;
            }
        }

        BigDecimal v1;
        if (rule == null) { // take the last ladder rule and set to the max value
            rule = LADDER_RULES.get(LADDER_RULES.size() - 1);
            v1 = rule.getTo();
            return (negative ? v1.negate() : v1);
        }

        //BigDecimal t = rule.getTo();
        BigDecimal f = rule.getFrom();
        BigDecimal inc = rule.getIncrement();
        BigDecimal o = v.subtract(f).remainder(inc);
        if (o.compareTo(BigDecimal.ZERO) == 0) { // v is already on the ladder
            v1 = v;
        } else if ((!negative) && (roundUp)) { // positive odds round up
            v1 = v.add(inc).subtract(o);
        } else if ((!negative) && (!roundUp)) { // positive odds round down
            v1 = v.subtract(o);
        } else if ((negative) && (roundUp)) { // negative odds round up
            v1 = v.subtract(o);
        } else { // if ((negative) && (!roundUp)) // negative odds round down
            v1 = v.add(inc).subtract(o);
        }

        v1 = (negative ? v1.negate() : v1);

        // convert -100 to +100
        if (v1.compareTo(MINUS_ONE_HUNDRED) == 0) {
            return ONE_HUNDRED;
        }
        return v1;
    }

    /**
     * Returns type.
     *
     * @return the type
     */
    public OddsType getType() {
        return type;
    }

    /**
     * Returns value.
     *
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Returns usOddsValue.
     *
     * @return the usOddsValue
     */
    public Integer getUsOddsValue() {
        return usOddsValue;
    }

    /**
     * @see Comparable#compareTo(Object)
     */
    @Override
    public int compareTo(Odds o) {
        return usOddsValue.compareTo(o.getUsOddsValue());
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Odds [type=");
        builder.append(type);
        builder.append(", value=");
        builder.append(value);
        builder.append(", usOddsValue=");
        builder.append(usOddsValue);
        builder.append("]");
        return builder.toString();
    }

    /**
     * <p>
     * <b>Title</b> OddsLadderRule
     * </p>
     * <p>
     * <b>Description</b> A Ladder Rule is a section of the odds ladder which defines the increment to
     * use between two values.
     * </p>
     * <p>
     * <b>Company</b> Xanadu Consultancy Ltd
     * </p>
     * <p>
     * <b>Copyright</b> Copyright (c) 2014
     * </p>
     *
     * @author David Mills
     * @version 1.0
     */
    protected static class OddsLadderRule implements Serializable {

        private static final long serialVersionUID = -463885547989032908L;

        private final BigDecimal from;
        private final BigDecimal to;
        private final BigDecimal increment;

        /**
         * @param from
         * @param to
         * @param increment
         */
        OddsLadderRule(BigDecimal from, BigDecimal to, BigDecimal increment) {
            this.from = from;
            this.to = to;
            this.increment = increment;
        }

        /**
         * @return
         */
        public BigDecimal getFrom() {
            return from;
        }

        /**
         * @return
         */
        public BigDecimal getTo() {
            return to;
        }

        /**
         * @return
         */
        public BigDecimal getIncrement() {
            return increment;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("OddsLadderRule [from=");
            builder.append(from);
            builder.append(", to=");
            builder.append(to);
            builder.append(", increment=");
            builder.append(increment);
            builder.append("]");
            return builder.toString();
        }
    }
}
