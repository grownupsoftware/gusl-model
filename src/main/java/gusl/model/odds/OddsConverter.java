package gusl.model.odds;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.decimal.DecimalDO;
import gusl.model.money.MoneyDO;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import static java.util.Objects.isNull;

public class OddsConverter {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(OddsConverter.class);

    private static final Integer VER = 1;

    private static final BigDecimal TWO = BigDecimal.valueOf(2);
    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
    private static final BigDecimal MINUS_ONE_HUNDRED = BigDecimal.valueOf(-100);
    private static final BigDecimal MINUS_ONE = BigDecimal.valueOf(-1);
    private static final BigDecimal FIFTY = BigDecimal.valueOf(50);
    private static final BigDecimal TEN_THOUSAND = BigDecimal.valueOf(10000);

    public static final MathContext ODDS_CONTEXT_ROUND_UP = new MathContext(16, RoundingMode.CEILING);
    public static final MathContext ODDS_CONTEXT_ROUND_DOWN = new MathContext(16, RoundingMode.FLOOR);
    private static final Integer ONE_HUNDRED_INT = 100;
    private static final Integer MINUS_ONE_HUNDRED_INT = -100;
    private static final BigDecimal HALF = BigDecimal.valueOf(0.5D);
    public static final BigDecimal ONE = BigDecimal.valueOf(1);

    public static final MathContext DEFAULT_CONTEXT = new MathContext(16, RoundingMode.HALF_UP);
    private static final int DEFAULT_SCALE = 16;

    private static final int FORMAT_SCALE = 2;

    private static final int US_FORMAT_SCALE = 0;

    private static final int DEFAULT_US_SCALE = 0;

    public static final MathContext DEFAULT_CONTEXT_TO_LONG = new MathContext(16, RoundingMode.HALF_UP);

    private static final int SCALE_UP_SCALE = 0;

    public static final MathContext RISK_GAIN_TO_ODDS = new MathContext(2, RoundingMode.HALF_UP);

    private OddsConverter() {
    }

    public static MathContext getDefaultContext() {
        return DEFAULT_CONTEXT;
    }

    public static int getDefaultScale() {
        return DEFAULT_SCALE;
    }

    public static int getFormatScale() {
        return FORMAT_SCALE;
    }

    public static int getUsFormatScale() {
        return US_FORMAT_SCALE;
    }

    public static int getDefaultUsScale() {
        return DEFAULT_US_SCALE;
    }

    public static int getRoundUpScale() {
        return SCALE_UP_SCALE;
    }

    public static MoneyDO calculateWinAmount(OddsDO odds, MoneyDO stake) {
        return stake.add(calculateGain(odds, stake));
    }

    public static MoneyDO calculateGain(OddsDO odds, MoneyDO risk) {
        OddsDO decimalOdds = odds.getType() == OddsType.D ? odds : convertFromAnyToDecimal(odds);
        final BigDecimal result = BigDecimal
                .valueOf(risk.getValue())
                .multiply(decimalOdds.getValue().subtract(BigDecimal.ONE), DEFAULT_CONTEXT_TO_LONG); // getDefaultContext());
        return new MoneyDO(risk.getCurrencyCode(), result.longValue());
//        if (decimalOdds.getValue().signum() == -1) {
//            // -ve decimal odds e.g. -1.92, more likely we flipped a bet
//
//        } else {
//            final BigDecimal result = BigDecimal
//                    .valueOf(risk.getValue())
//                    .multiply(decimalOdds.getValue().subtract(BigDecimal.ONE), DEFAULT_CONTEXT_TO_LONG); // getDefaultContext());
//            return new MoneyDO(risk.getCurrencyCode(), result.longValue());
//        }
    }

    public static OddsDO flipOdds(OddsDO odds) {
        return OddsDO.builder()
                .type(odds.getType())
                .value(isNull(odds.getValue()) ? BigDecimal.ZERO : odds.getValue().multiply(MINUS_ONE, DEFAULT_CONTEXT_TO_LONG))
                .build();
    }

//    tryOne(odds, risk, true, true);
//    tryOne(odds, risk, true, false);
//    tryOne(odds, risk, false, true);
//    tryOne(odds, risk, false, false);
//    private static void tryOne(OddsDO odds, MoneyDO risk, boolean roundUp, boolean roundDown) {
//        final BigDecimal value = convertFromUsOdds(convertToUsOdds(odds.getValue(), odds.getType(), roundUp), OddsType.D, roundDown);
//        BigDecimal result = BigDecimal
//                .valueOf(risk.getValue())
//                .multiply(value.subtract(BigDecimal.ONE), DEFAULT_CONTEXT_TO_LONG); // getDefaultContext());
//        logger.info("tryOne(16): {} {} {} {}", value, value.toPlainString(), result, result.toPlainString());
//
//        result = BigDecimal
//                .valueOf(risk.getValue())
//                .multiply(value.subtract(BigDecimal.ONE), new MathContext(4, RoundingMode.HALF_UP));
//        logger.info("tryOne(4): {} {} {} {}", value, value.toPlainString(), result, result.toPlainString());
//        result = BigDecimal
//                .valueOf(risk.getValue())
//                .multiply(value.subtract(BigDecimal.ONE), new MathContext(6, RoundingMode.HALF_UP));
//        logger.info("tryOne(6): {} {} {} {}", value, value.toPlainString(), result, result.toPlainString());
//        result = BigDecimal
//                .valueOf(risk.getValue())
//                .multiply(value.subtract(BigDecimal.ONE), new MathContext(8, RoundingMode.HALF_UP));
//        logger.info("tryOne(8): {} {} {} {}", value, value.toPlainString(), result, result.toPlainString());
//        result = BigDecimal
//                .valueOf(risk.getValue())
//                .multiply(value.subtract(BigDecimal.ONE), new MathContext(10, RoundingMode.HALF_UP));
//        logger.info("tryOne(10): {} {} {} {}", value, value.toPlainString(), result, result.toPlainString());
//    }

    public static MoneyDO calculateRisk(OddsDO odds, MoneyDO risk) {
        OddsDO decimalOdds = convertFromAnyToDecimal(odds);
        if (risk.getValue() == 0) {
            return new MoneyDO(risk.getCurrencyCode(), 0);
        }
        final BigDecimal result = BigDecimal.valueOf(risk.getValue())
                .divide(decimalOdds.getValue().subtract(BigDecimal.ONE), getDefaultContext())
                .setScale(getRoundUpScale(), RoundingMode.HALF_UP);

        logger.debug("decimalOdds: {} risk: {} subtract: {} result:{}",
                decimalOdds,
                risk.getValue(),
                decimalOdds.getValue().subtract(BigDecimal.ONE),
                result);

        return new MoneyDO(risk.getCurrencyCode(), result.longValue());
    }

    public static OddsDO convertToUs(OddsDO odds, boolean roundUp) {
        return OddsDO.builder().type(OddsType.US).value(OddsConverter.convertToUsOdds(odds.getValue(), odds.getType(), roundUp)).build();
    }

    public static OddsDO convertToUs(BigDecimal odds, OddsType fromOddsType, boolean roundUp) {
        return OddsDO.builder().type(OddsType.US).value(OddsConverter.convertToUsOdds(odds, fromOddsType, roundUp)).build();
    }

    /**
     * Convert the odds which are in the specified to US odds.
     *
     * @param odds
     * @param fromOddsType
     * @param roundUp
     * @return
     */
    public static BigDecimal convertToUsOdds(BigDecimal odds, OddsType fromOddsType, boolean roundUp) {
        MathContext context = getOddsContext(roundUp);
        BigDecimal usOdds;
        switch (fromOddsType) {
            case US:
                usOdds = odds;
                break;
            case D:
                usOdds = convertFromDecimalOddsToUsOdds(odds, context);
                break;
            case P:
                context = getOddsContext(!roundUp); // probability odds work in reverse - lower is better
                usOdds = convertFromProbabilityOddsToUsOdds(odds, context);
                break;
            case HK:
                usOdds = convertFromHongKongOddsToUsOdds(odds, context);
                break;
            case M:
                usOdds = convertFromMalayOddsToUsOdds(odds, context);
                break;
            case I:
                usOdds = convertFromIndonesianOddsToUsOdds(odds, context);
                break;
            default:
                usOdds = odds;
                break;
        }
        return scaleUsOdds(usOdds, roundUp);
    }

    /**
     * Convert from decimal to US odds.
     *
     * @param decimalOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromDecimalOddsToUsOdds(BigDecimal decimalOdds, MathContext context) {
        // ret = (ret >= 2) ? 100 * (ret-1) : -100 /(ret-1);
        BigDecimal usOdds;
        int compare = decimalOdds.compareTo(TWO);
        if (compare >= 0) {
            usOdds = decimalOdds.subtract(BigDecimal.ONE);
            usOdds = usOdds.multiply(ONE_HUNDRED, context);
        } else {
            usOdds = decimalOdds.subtract(BigDecimal.ONE);
            usOdds = MINUS_ONE_HUNDRED.divide(usOdds, context);
        }
//        logger.temp("US= up => {} {}", usOdds.toPlainString(), usOdds.setScale(4, getOddsContext(true).getRoundingMode()));
//        logger.temp("US=down=> {} {}", usOdds.toPlainString(), usOdds.setScale(4, getOddsContext(false).getRoundingMode()));
//        logger.temp("US= now=> {} {}", usOdds.toPlainString(), usOdds.setScale(getFormatScale(), RoundingMode.HALF_EVEN));

        return usOdds.setScale(getDefaultUsScale(), RoundingMode.HALF_EVEN);

        // return scaleUsOdds(usOdds, false);
    }

    /**
     * Convert from probability to US odds.
     *
     * @param probabilityOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromProbabilityOddsToUsOdds(BigDecimal probabilityOdds, MathContext context) {
        // ret = (ret <= 50) ? 100 * ((100-ret) / ret) : (ret*100) / (ret-100);
        BigDecimal usOdds;
        final int compare = probabilityOdds.compareTo(FIFTY);
        if (compare <= 0) {
            final BigDecimal a = ONE_HUNDRED.subtract(probabilityOdds);
            usOdds = a.divide(probabilityOdds, context);
            usOdds = ONE_HUNDRED.multiply(usOdds, context);
        } else {
            final BigDecimal a = probabilityOdds.multiply(ONE_HUNDRED);
            final BigDecimal b = probabilityOdds.subtract(ONE_HUNDRED);
            usOdds = a.divide(b, context);
        }
        return usOdds;
    }

    /**
     * Convert from Hong Kong to US odds.
     *
     * @param hkOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromHongKongOddsToUsOdds(BigDecimal hkOdds, MathContext context) {
        // ret = (ret >= 1)? 100 * (ret) : -100 /(ret);
        BigDecimal usOdds;
        final int compare = hkOdds.compareTo(BigDecimal.ONE);
        if (compare >= 0) {
            usOdds = hkOdds.multiply(ONE_HUNDRED, context);
        } else {
            usOdds = MINUS_ONE_HUNDRED.divide(hkOdds, context);
        }
        return usOdds;
    }

    /**
     * Convert from Malay to US odds.
     *
     * @param malayOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromMalayOddsToUsOdds(BigDecimal malayOdds, MathContext context) {
        // ret = -100/ret;
        BigDecimal usOdds = MINUS_ONE_HUNDRED.divide(malayOdds, context);
        return usOdds;
    }

    /**
     * Convert from Indonesian to US odds.
     *
     * @param indoOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromIndonesianOddsToUsOdds(BigDecimal indoOdds, MathContext context) {
        // ret = ret*100;
        BigDecimal usOdds = ONE_HUNDRED.multiply(indoOdds, context);
        return usOdds;
    }

    public static OddsDO convertFromAnyToDecimal(OddsDO odds) {

//        logger.temp("a=>{}", convertToUsOdds(odds.getValue(), odds.getType(), true).toPlainString());
//        logger.temp("b=>{}", convertFromUsOdds(
//                convertToUsOdds(odds.getValue(), odds.getType(), true),
//                OddsType.D, false).toPlainString());
        return OddsDO.builder()
                .type(OddsType.D)
                .value(convertFromUsOdds(
                        convertToUsOdds(odds.getValue(), odds.getType(), true),
                        OddsType.D, false))
                .build();
    }

    /**
     * Convert the given US Odds to the specified {}.
     *
     * @param usOdds     The US odds value to convert.
     * @param toOddsType The type to convert to.
     * @param roundUp
     * @return The converted odds value.
     */
    public static BigDecimal convertFromUsOdds(BigDecimal usOdds, OddsType toOddsType, boolean roundUp) {
        boolean finalRoundUp = roundUp;
        MathContext context = getOddsContext(roundUp);

//        if ((usOdds.compareTo(ONE_HUNDRED) < 0) && (usOdds.compareTo(MINUS_ONE_HUNDRED) > 0)) {
//            usOdds = ONE_HUNDRED;
//        }

        BigDecimal theOdds;
        switch (toOddsType) {
            case US:
                theOdds = usOdds;
                break;
            case D:
                theOdds = convertFromUsOddsToDecimalOdds(usOdds, context);
                break;
            case P:
                finalRoundUp = !roundUp; // probability odds work in reverse - lower is better
                context = getOddsContext(!roundUp);
                theOdds = convertFromUsOddsToProbabilityOdds(usOdds, context);
                break;
            case HK:
                theOdds = convertFromUsOddsToHongKongOdds(usOdds, context);
                break;
            case M:
                theOdds = convertFromUsOddsToMalayOdds(usOdds, context);
                break;
            case I:
                theOdds = convertFromUsOddsToIndonesianOdds(usOdds, context);
                break;
            default:
                theOdds = usOdds;
                break;
        }
        return scaleOdds(theOdds, finalRoundUp);
    }

    /**
     * Convert from US to decimal odds.
     *
     * @param usOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromUsOddsToDecimalOdds(BigDecimal usOdds, MathContext context) {
        // ret = (value >= 100) ? ((value/100) + 1) : ((-100/value) + 1);
        BigDecimal converted;
        final int compare = usOdds.compareTo(ONE_HUNDRED);
        if (compare >= 0) {
            converted = usOdds.divide(ONE_HUNDRED, context);
            converted = converted.add(BigDecimal.ONE);
        } else {
            converted = MINUS_ONE_HUNDRED.divide(usOdds, context);
            converted = converted.add(BigDecimal.ONE);
        }
        return converted;
    }

    /**
     * Convert from US to probability odds.
     *
     * @param usOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromUsOddsToProbabilityOdds(BigDecimal usOdds, MathContext context) {
        // ret = (value >= 100) ? (10000 / (value + 100)) : (100 * value) / (value-100);
        BigDecimal converted;
        final int compare = usOdds.compareTo(ONE_HUNDRED);
        if (compare >= 0) {
            converted = usOdds.add(ONE_HUNDRED);
            converted = TEN_THOUSAND.divide(converted, context);
        } else {
            final BigDecimal a = usOdds.multiply(ONE_HUNDRED, context);
            final BigDecimal b = usOdds.subtract(ONE_HUNDRED);
            converted = a.divide(b, context);
        }
        return converted;
    }

    /**
     * Convert from US to Hong Kong odds.
     *
     * @param usOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromUsOddsToHongKongOdds(BigDecimal usOdds, MathContext context) {
        // ret = (value >= 100) ? (value/100) : (-100/value);
        BigDecimal converted;
        final int compare = usOdds.compareTo(ONE_HUNDRED);
        if (compare >= 0) {
            converted = usOdds.divide(ONE_HUNDRED, context);
        } else {
            converted = MINUS_ONE_HUNDRED.divide(usOdds, context);
        }
        return converted;
    }

    /**
     * Convert from US to Malaysian odds.
     *
     * @param usOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromUsOddsToMalayOdds(BigDecimal usOdds, MathContext context) {
        if (usOdds.compareTo(ONE_HUNDRED) == 0) {
            return BigDecimal.ONE;
        }
        // ret = (-100/value) ;
        BigDecimal converted = MINUS_ONE_HUNDRED.divide(usOdds, context);
        return converted;
    }

    /**
     * Convert from US to Indonesian odds.
     *
     * @param usOdds
     * @param context
     * @return
     */
    private static BigDecimal convertFromUsOddsToIndonesianOdds(BigDecimal usOdds, MathContext context) {
        // ret = (value/100) ;
        BigDecimal converted = usOdds.divide(ONE_HUNDRED, context);
        return converted;
    }

    public static boolean validRange(OddsDO odds) {
        return validRange(odds.getType(), odds.getValue());
    }

    public static boolean validRange(OddsType oddsType, BigDecimal odds) {
        return true;
//        if (isNull(odds) || isNull(oddsType)) {
//            return false;
//        }
//        switch (oddsType) {
//            case US:
//                if (odds.stripTrailingZeros().scale() > 0) {
//                    return false;
//                }
//                if (odds.longValue() >= 100 || odds.longValue() <= -100) {
//                    return true;
//                }
//                return false;
//            case D:
//            case P:
//            case HK:
//            case M:
//            case I:
//            default:
//                return true;
//        }
    }

    public static MathContext getOddsContext(boolean roundUp) {
        return roundUp ? ODDS_CONTEXT_ROUND_UP : ODDS_CONTEXT_ROUND_DOWN;
    }

    public static BigDecimal scaleOdds(BigDecimal odds, boolean roundUp) {
        MathContext context = getOddsContext(roundUp);
        return odds.setScale(getDefaultScale(), context.getRoundingMode());
    }

    public static OddsDO formatForDisplay(OddsDO odds) {
        if (isNull(odds) || isNull(odds.getType()) || isNull(odds.getValue())) {
            return odds;
        }
//        logger.temp("= up => {} {}", odds.getValue().toPlainString(), odds.getValue().setScale(4, getOddsContext(true).getRoundingMode()));
//        logger.temp("=down=> {} {}", odds.getValue().toPlainString(), odds.getValue().setScale(4, getOddsContext(false).getRoundingMode()));
//        logger.temp("= now=> {} {}", odds.getValue().toPlainString(), odds.getValue().setScale(getFormatScale(), RoundingMode.HALF_EVEN));
//        logger.temp("= US now=> {} {}", odds.getValue().toPlainString(), odds.getValue().setScale(getUsFormatScale(), RoundingMode.HALF_EVEN));

        MathContext context = getOddsContext(true);
        switch (odds.getType()) {
            case D:
                return OddsDO.builder()
                        .type(odds.getType())
                        .value(odds.getValue().setScale(getFormatScale(), RoundingMode.HALF_EVEN))
                        .build();
            case US:
                return OddsDO.builder()
                        .type(odds.getType())
                        .value(odds.getValue().setScale(getUsFormatScale(), RoundingMode.HALF_EVEN))
                        .build();
        }
        return odds;
    }

    public static BigDecimal scaleUsOdds(BigDecimal odds, boolean roundUp) {
        MathContext context = getOddsContext(roundUp);
        return odds.setScale(getDefaultUsScale(), context.getRoundingMode());
    }

    public static long calcPercentage(double percentage, long value) {
        return BigDecimal.valueOf(value).multiply(BigDecimal.valueOf(percentage), DEFAULT_CONTEXT_TO_LONG).divide(BigDecimal.valueOf(100.0d), DEFAULT_CONTEXT_TO_LONG).longValue();
    }

    public static RiskGainDO calculateDecimalOdds(MoneyDO risk, MoneyDO gain) {
        double decimalOdds = 0d;
        if (gain.getValue() != 0) {
            decimalOdds = (((gain.getValue() * 1.0d) - (risk.getValue() * 1.0d)) / (risk.getValue() * 1.0d)) + 2.0d;
        }
        return RiskGainDO.builder()
                .odds(OddsDO.builder()
                        .type(OddsType.D)
                        .value(BigDecimal.valueOf(decimalOdds)
                                .setScale(RISK_GAIN_TO_ODDS.getPrecision(), RISK_GAIN_TO_ODDS.getRoundingMode()))
                        .build())
                .gain(gain)
                .risk(risk)
                .build();

    }

    public static DecimalDO calculateUsOdds(MoneyDO risk, MoneyDO gain) {
        double odds = -1d;
        boolean divByGain = false;
        try {
            if (isNull(gain) || isNull(risk)) {
                return DecimalDO.of(0, 2);
            }
            boolean negativeOdds = Math.abs(gain.getValue()) < Math.abs(risk.getValue())
                    || gain.getValue() < 0;

            divByGain = Math.abs(gain.getValue()) < Math.abs(risk.getValue());

            if ((divByGain && gain.getValue() == 0) || (!divByGain && risk.getValue() == 0)) {
                return DecimalDO.of(0, 2);
            }

            odds = ((Math.abs(Math.abs(gain.getValue()) - Math.abs(risk.getValue())) * 1.0d) /
                    ((divByGain ? Math.abs(gain.getValue()) : Math.abs(risk.getValue()))
                            * 1.0d) + 1.0d) * 100.0d;

            final BigDecimal bigDecimal = BigDecimal.valueOf(odds)
                    .setScale(RISK_GAIN_TO_ODDS.getPrecision(), RISK_GAIN_TO_ODDS.getRoundingMode());

            logger.debug("-ve: {} R: {} G: {} O: {}", negativeOdds, risk.getValue(), gain.getValue(), bigDecimal.doubleValue());
            return DecimalDO.from(negativeOdds ? bigDecimal.doubleValue() * -1 : bigDecimal.doubleValue(), 2);
        } catch (Throwable t) {
            logger.error("Failed to calc odds odds: {} {} {} {}", odds, divByGain, gain.getValue(), risk.getValue());
            throw t;
        }
    }
}
