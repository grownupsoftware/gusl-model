package gusl.telegram;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTelegramIdRequestDO {

    private String id;
    private String telegramId;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
