package gusl.postgres.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dhudson
 * @since 25/08/2022
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PostgresView {
    String sql();
    String count();
    String name() default "";
    int version() default 0;
    boolean isDbView() default false;
}
