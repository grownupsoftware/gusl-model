package gusl.postgres.model;

import gusl.query.QueryOrder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dhudson
 * @since 14/04/2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface PostgresIndex {

    PostgresIndexType type() default PostgresIndexType.BTREE;

    QueryOrder order() default QueryOrder.ASC;

    boolean unique() default false;
}
