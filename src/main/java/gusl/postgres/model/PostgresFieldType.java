package gusl.postgres.model;

/**
 * @author dhudson
 * @since 14/04/2021
 * https://www.javatpoint.com/postgresql-bigint
 */
public enum PostgresFieldType {
    TEXT,
    INTEGER,
    BIGINT,
    NUMERIC,
    BOOLEAN,
    TIMESTAMP,
    DATE,
    TIME,
    JSONB,
    CIDR,
    INET,
    MACADDR,
    POINT,
    CHAR_ARRAY,
    LIST,
    ZONED_TIMESTAMP
}
