package gusl.postgres.model;

import gusl.core.security.ObfuscateMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dhudson
 * @since 14/04/2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface PostgresField {

    PostgresFieldType type();

    ObfuscateMethod securityMethod() default ObfuscateMethod.NONE;

    boolean notNull() default false;

    boolean primaryKey() default false;

    boolean dateCreated() default false;

    boolean dateUpdated() default false;

    boolean populate() default false;

    String name() default "";

    String defaultValue() default "";
}
