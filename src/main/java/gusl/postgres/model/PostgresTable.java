package gusl.postgres.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dhudson
 * @since 14/04/2021
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PostgresTable {

    String table();

    int version();

    String schema() default "public";

    Class<?> type();

    String staticContent() default "";
}
