package gusl.postgres.model;

/**
 * @author dhudson
 * @since 19/04/2021
 */
public enum PostgresIndexType {
    BTREE,
    HASH,
    GIST,
    SPGIST,
    GIN,
    BRIN
}
