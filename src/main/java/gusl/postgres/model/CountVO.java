package gusl.postgres.model;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CountVO {

    @PostgresField(type = PostgresFieldType.BIGINT)
    private Long count;

    public String toString() {
        return ToString.toString(this);
    }
}
