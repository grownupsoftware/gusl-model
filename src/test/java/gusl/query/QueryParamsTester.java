package gusl.query;


import lombok.CustomLog;
import org.junit.Test;

@CustomLog
public class QueryParamsTester {

    @Test
    public void queryParamBuilder() {
        QueryParams params = QueryParams.builder().firstAndLast(true).build();
        logger.info("{}", params);
    }

}
