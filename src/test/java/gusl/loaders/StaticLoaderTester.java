package gusl.loaders;

import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.junit.Test;

import java.io.IOException;

/**
 * @author dhudson
 * @since 09/10/2022
 */
@CustomLog
public class StaticLoaderTester {

    @Test
    public void testInclude() throws IOException, GUSLErrorException {
        logger.info("{}", StaticLoader.loadResource("template_macro.json", Macro.class));
    }
}
