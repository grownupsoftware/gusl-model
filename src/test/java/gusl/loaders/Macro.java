package gusl.loaders;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gusl.core.json.Base64Deserializer;
import gusl.core.json.Base64Serializer;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringSummary;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 * @since 09/10/2022
 */
@Getter
@Setter
public class Macro {
    private String id;
    private String code;
    @ToStringSummary
    @JsonDeserialize(using = Base64Deserializer.class)
    @JsonSerialize(using = Base64Serializer.class)
    private String template;
    private String status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
