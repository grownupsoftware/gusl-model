package gusl.model.odds

import spock.lang.Specification

class OddsConverterTest extends Specification {

    def "Odds converter US -> D"(double us, Double decimal) {
        given:
        final OddsDO decimalOdds = OddsDO.builder().type(OddsType.D).value(BigDecimal.valueOf(decimal)).build()
        final OddsDO usOdds = OddsDO.builder().type(OddsType.US).value(BigDecimal.valueOf(us)).build()
print(OddsConverter.convertFromAnyToDecimal(usOdds))
        OddsDO result = OddsConverter.formatForDisplay(OddsConverter.convertFromAnyToDecimal(usOdds))

        expect: "correct type"
        result.type == OddsType.D

        and: "correct value"
        result.value == decimalOdds.value

        where:
        us     | decimal
        -95    | 2.05
//        -10000 | 1.01
//        -500   | 1.2
//        -450   | 1.22
//        -400   | 1.25
//        -350   | 1.29
//        -333.3 | 1.3
//        -300   | 1.33
//        -275   | 1.36
//        -250   | 1.4
//        -225   | 1.44
//        -200   | 1.5
//        -187.5 | 1.53
//        -175   | 1.57
//        -162.5 | 1.62
//        -150   | 1.67
//        -137.5 | 1.73
//        -125   | 1.8
//        -120   | 1.83
//        -110   | 1.91
//        100    | 2
//        105    | 2.05
//        110    | 2.1
//        115    | 2.15
//        120    | 2.2
//        125    | 2.25
//        137.5  | 2.38
//        140    | 2.4
//        150    | 2.5
//        160    | 2.6
//        162.5  | 2.63
//        175    | 2.75
//        180    | 2.8
//        187.5  | 2.88
//        200    | 3
//        220    | 3.2
//        225    | 3.25
//        240    | 3.4
//        250    | 3.5
//        260    | 3.6
//        275    | 3.75
//        300    | 4
//        320    | 4.2
//        333.3  | 4.34
//        350    | 4.5
//        400    | 5
//        450    | 5.5
//        500    | 6
//        550    | 6.5
//        600    | 7
//        650    | 7.5
//        700    | 8
//        750    | 8.5
//        800    | 9
//        900    | 10
//        1000   | 11
//        1100   | 12
//        1200   | 13
//        1300   | 14
//        1400   | 15
//        1500   | 16
//        1600   | 17
//        1800   | 19
//        2000   | 21
//        2500   | 26
//        3300   | 34
//        5000   | 51
//        6600   | 67
//        10000  | 101
//        100000 | 1001
    }

//    def "Odds converter D -> US"(Double decimal, double us) {
//        given:
//        final OddsDO decimalOdds = OddsDO.builder().type(OddsType.D).value(BigDecimal.valueOf(decimal)).build()
//        final OddsDO usOdds = OddsDO.builder().type(OddsType.US).value(BigDecimal.valueOf(us)).build()
//
//        OddsDO result = OddsConverter.formatForDisplay(OddsConverter.convertToUs(decimalOdds, true))
//
//        expect: "correct type"
//        result.type == OddsType.US
//
//        and: "correct value"
//        result.value == usOdds.value
//
//        where:
//        decimal | us
//        1.9     | -111
//        1.01    | -10000
//        1.2     | -500
////        1.22    | -450
//        1.22    | -455
//        1.25    | -400
////        1.29    | -350
//        1.29    | -345
//        1.3     | -333
////        1.33    | -300
//        1.33    | -303
////        1.36    | -275
//        1.36    | -278
//        1.4     | -250
////        1.44    | -225
//        1.44    | -227
//        1.5     | -200
////        1.53    | -188
//        1.53    | -189
//        1.57    | -175
//        1.62    | -161
//        1.67    | -149
//        1.73    | -137
//        1.8     | -125
//        1.83    | -120
//        1.91    | -110
//        2       | 100
//        2.05    | 105
//        2.1     | 110
//        2.15    | 115
//        2.2     | 120
//        2.25    | 125
////        2.38    | 137.5
//        2.38    | 138
//        2.4     | 140
//        2.5     | 150
//        2.6     | 160
//        2.62    | 162
//        2.63    | 163
//        2.75    | 175
//        2.8     | 180
//        2.88    | 188
//        3       | 200
//        3.2     | 220
//        3.25    | 225
//        3.4     | 240
//        3.5     | 250
//        3.6     | 260
//        3.75    | 275
//        4       | 300
//        4.2     | 320
//        4.33    | 333
//        4.5     | 350
//        5       | 400
//        5.5     | 450
//        6       | 500
//        6.5     | 550
//        7       | 600
//        7.5     | 650
//        8       | 700
//        8.5     | 750
//        9       | 800
//        10      | 900
//        11      | 1000
//        12      | 1100
//        13      | 1200
//        14      | 1300
//        15      | 1400
//        16      | 1500
//        17      | 1600
//        19      | 1800
//        21      | 2000
//        26      | 2500
//        34      | 3300
//        51      | 5000
//        67      | 6600
//        101     | 10000
//        1001    | 100000
//
//    }
//
//    def "Odds converter US -> US "(double us, double expected) {
//        given:
//        final OddsDO expectedOdds = OddsDO.builder().type(OddsType.US).value(BigDecimal.valueOf(expected)).build()
//        final OddsDO usOdds = OddsDO.builder().type(OddsType.US).value(BigDecimal.valueOf(us)).build()
//
//        final OddsDO decimalOdds = OddsConverter.convertFromAnyToDecimal(usOdds);
//        print(decimalOdds)
//
//        OddsDO result = OddsConverter.formatForDisplay(OddsConverter.convertToUs(decimalOdds, true))
//
//        expect: "correct type"
//        result.type == OddsType.US
//
//        and: "correct value"
//        result.value == expected.value
//
//        where:
//        us  | expected
//        -95 | -95
//    }

}
